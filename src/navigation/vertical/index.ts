// ** Icon imports
import Login from 'mdi-material-ui/Login'
import Table from 'mdi-material-ui/Table'
import CubeOutline from 'mdi-material-ui/CubeOutline'
import HomeOutline from 'mdi-material-ui/HomeOutline'
import FormatLetterCase from 'mdi-material-ui/FormatLetterCase'
import AccountCogOutline from 'mdi-material-ui/AccountCogOutline'
import CreditCardOutline from 'mdi-material-ui/CreditCardOutline'
import AccountPlusOutline from 'mdi-material-ui/AccountPlusOutline'
import AlertCircleOutline from 'mdi-material-ui/AlertCircleOutline'
import GoogleCirclesExtended from 'mdi-material-ui/GoogleCirclesExtended'
import { Package, CartArrowUp, CartArrowDown } from 'mdi-material-ui'

// ** Type import
import { VerticalNavItemsType } from 'src/@core/layouts/types'

const navigation = (roleName: string): VerticalNavItemsType | any => {
  if (roleName)
    return [
      {
        title: 'Dashboard',
        icon: HomeOutline,
        path: '/dashboard',
        rolesAllowed: ['Superadmin']
      },
      {
        title: 'Mitra',
        icon: AccountCogOutline,
        path: '/partners',
        rolesAllowed: ['Superadmin']
      },
      {
        title: 'Penjualan',
        icon: CartArrowUp,
        path: '/invoices',
        rolesAllowed: ['Owner', 'Superadmin', 'Admin', 'Reseller']
      },
      {
        title: 'Pembelian',
        icon: CartArrowDown,
        path: '/',
        rolesAllowed: ['Superadmin']
      },
      {
        title: 'Produk',
        icon: Package,
        path: '/products',
        rolesAllowed: ['Superadmin']
      },
      {
        title: 'Admin User',
        icon: FormatLetterCase,
        path: '/users',
        rolesAllowed: ['Superadmin']
      }
    ].filter(nav => nav.rolesAllowed.length === 0 || nav.rolesAllowed.includes(roleName))
}

export default navigation
