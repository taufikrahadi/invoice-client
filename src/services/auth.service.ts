import { SupabaseClient } from '@supabase/supabase-js'

export class AuthService {
  constructor(
    private readonly supabase: SupabaseClient<any, 'public', any>,
    private readonly startLoading: () => void,
    private readonly stopLoading: () => void
  ) {}

  protected emailSuffix = '@invoice-app.com'
  public userProfile = null

  public async login(username: string, password: string) {
    try {
      this.startLoading()
      const { data, error } = await this.supabase.auth.signInWithPassword({
        email: username + this.emailSuffix,
        password: password
      })
      if (error) throw error

      const { user, session } = data
    } catch (error) {
      throw error
    } finally {
      this.stopLoading()
    }
  }

  public async getProfile(uid: string) {
    try {
      const { data: profile } = await this.supabase.from('profiles').select('*, companies(*)').eq('uid', uid).single()

      return
    } catch (error) {}
  }
}
