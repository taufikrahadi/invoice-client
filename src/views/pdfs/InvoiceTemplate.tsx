import { useEffect, useCallback, useState } from 'react'
import Head from 'next/head'
import { Grid, Box, Typography, TableFooter } from '@mui/material'
import { styled } from '@mui/material/styles'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell, { tableCellClasses } from '@mui/material/TableCell'
import TableRow from '@mui/material/TableRow'
import dayjs from 'dayjs'
import * as localeId from 'dayjs/locale/id'

dayjs.locale('id')

interface Props {
  invoice?: any
  invoice_merges?: any
  company?: any
  padding?: any
  margin?: any
  stampTop?: string
  stampLeft?: string
}

const stamp = (invoice: any) => {
  if (invoice?.invoice_status_id === 3) {
    if (invoice?.remaining_amount <= 0)
      return `https://oyhmqxylcaqizqcroxkt.supabase.co/storage/v1/object/public/assets/lunas.png`
    else return `https://oyhmqxylcaqizqcroxkt.supabase.co/storage/v1/object/public/assets/selesai.png`
  }

  return null
}

const formatDate = (invoiceDate: string) => {
  const dayNames = {
    Sunday: 'Minggu',
    Monday: 'Senin',
    Tuesday: 'Selasa',
    Wednesday: 'Rabu',
    Thursday: 'Kamis',
    Friday: 'Jumat',
    Saturday: 'Sabtu'
  }

  const day = (dayNames as any)[dayjs(invoiceDate).format('dddd') as any] as any

  return `${day}, ${dayjs(invoiceDate).format('DD-MM-YYYY')}`
}

const turnIntoCurrency = (val: number) => {
  return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(val)
}

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head} `]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: '2vw'
  },
  [`&.${tableCellClasses.body} `]: {
    fontSize: '2vw'
  }
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
    fontSize: 10
  },

  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
    fontSize: 10
  }
}))

const InvoiceTemplate = (props: Props) => {
  const { company } = props
  const [mainPadding, setMainPadding] = useState<string>('0px 10px 25px')
  const [mainMargin, setMainMargin] = useState<string>('-30px 15px 0px')
  const [stampTop, setStampTop] = useState('40%')
  const [stampLeft, setStampLeft] = useState('15vw')
  const formatdate = (date: any) => dayjs(date).format('DD/MM/YYYY')

  useEffect(() => {
    if (props.margin) setMainMargin(props.margin)
    if (props.padding) setMainPadding(props.padding)
    if (props.stampTop) setStampTop(props.stampTop)
    if (props.stampLeft) setStampTop(props.stampLeft)
  }, [props])

  return (
    <>
      <style>
        {`
  @page {
    margin: 0;
  }
          header {
    text - align: center;
    padding: 10px;
  }

            /* Define main content */
            main {
    padding: 20px;
  }

            /* Define footer */
            footer {
    position: fixed;
    bottom: 0;
    width: 100 %;
    text - align: center;
    padding: 10px;
  }
  /* Responsive styles */
  @media screen and(max - width: 768px) {
              body {
      margin: 0;
    }

              main {
      padding: 0px;
    }
  }
  `}
      </style>
      <body>
        <div
          style={{
            width: '100%',
            overflowX: 'hidden',
            position: 'relative',
            zIndex: 0
          }}
        >
          <img
            src='https://oyhmqxylcaqizqcroxkt.supabase.co/storage/v1/object/public/assets/wave-vector.png'
            style={{
              height: '100px',
              width: '100vw'
            }}
          />
        </div>
        <div
          id='invoice-pdf'
          style={{
            padding: mainPadding,
            margin: mainMargin,
            zIndex: 99,
            fontSize: '3vw'
          }}
        >
          <Head>
            <title>Nota #{props?.invoice?.invoice_number}</title>
          </Head>

          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'start',
              width: '100%',
              marginBottom: '35px',
              gap: 4
            }}
          >
            <img
              style={{
                width: '30vw'
              }}
              src={company?.logo}
            />

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: 1,
                fontSize: '2vw',
                width: '60vw'
              }}
            >
              {company?.stores.map((store: any) => (
                <Typography
                  sx={{
                    fontSize: '2vw'
                  }}
                >
                  <strong>{store?.name}</strong>: {store?.address}
                </Typography>
              ))}

              <Typography
                sx={{
                  fontSize: '2vw'
                }}
              >
                <strong>Office</strong>: {company?.address}
              </Typography>

              <Box
                sx={{
                  marginTop: '10px'
                }}
              >
                <Typography
                  sx={{
                    fontSize: '2vw'
                  }}
                >
                  <strong>Telp/WA</strong>: {company?.phone}
                </Typography>

                <Typography
                  sx={{
                    fontSize: '2vw'
                  }}
                >
                  <strong>Email</strong>: {company?.email}
                </Typography>
              </Box>
            </Box>
          </Box>

          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'start',
              fontSize: '2vw',
              marginTop: '5em'
            }}
            id='invoice-receiver'
          >
            <table style={{ borderLeft: '3px solid #915FDD', paddingLeft: '25px' }}>
              <tr>
                <td style={{ fontWeight: 'bolder', textAlign: 'left' }}>Kepada Yth.</td>
                <td>: {props.invoice?.profiles?.fullname}</td>
              </tr>

              <tr>
                <td style={{ fontWeight: 'bolder', textAlign: 'left' }}>Telp/WA</td>
                <td>: {props.invoice?.profiles?.phone ?? '-'}</td>
              </tr>

              <tr>
                <td style={{ fontWeight: 'bolder', textAlign: 'left' }}>Alamat</td>
                <td>: {props.invoice?.profiles?.address ?? '-'}</td>
              </tr>
            </table>

            <table>
              <tr>
                <td style={{ fontWeight: 'bolder', textAlign: 'left' }}>No Nota :</td>
                <td style={{ textAlign: 'right' }}>#{props.invoice?.invoice_number}</td>
              </tr>

              <tr>
                <td style={{ fontWeight: 'bolder', textAlign: 'left' }}>Tanggal :</td>
                <td style={{ textAlign: 'right' }}>{formatDate(props.invoice?.invoice_date)}</td>
              </tr>
            </table>
          </div>

          <Grid
            container
            spacing={6}
            sx={{
              marginY: '30px'
            }}
          >
            <Grid item xs={12}>
              {stamp(props.invoice) !== null ? (
                <Box
                  sx={{
                    top: stampTop,
                    left: stampLeft,
                    rotate: '15deg',
                    position: 'absolute',
                    height: '15vh'
                  }}
                >
                  <img
                    style={{
                      position: 'absolute',
                      height: '10vh',
                      opacity: 0.5,
                      zIndex: 9999999
                    }}
                    src={stamp(props.invoice) as any}
                  />
                </Box>
              ) : (
                <></>
              )}
              <Table sx={{ width: '100%' }}>
                <TableBody>
                  <StyledTableRow>
                    <StyledTableCell sx={{ background: '#313131', color: '#fff' }} align='left'>
                      <Typography
                        sx={{
                          fontSize: '2vw',
                          color: '#fff'
                        }}
                      >
                        Deskripsi
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell sx={{ background: '#313131', width: '10px' }} align='right'>
                      <Typography
                        sx={{
                          fontSize: '2vw',
                          color: '#fff'
                        }}
                      >
                        Kuantitas
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell sx={{ background: '#313131', color: '#fff' }} align='right'>
                      <Typography
                        sx={{
                          fontSize: '2vw',
                          color: '#fff'
                        }}
                      >
                        Harga
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell sx={{ background: '#313131', color: '#fff' }} align='right'>
                      <Typography
                        sx={{
                          fontSize: '2vw',
                          color: '#fff'
                        }}
                      >
                        Jumlah
                      </Typography>
                    </StyledTableCell>
                  </StyledTableRow>
                  <StyledTableRow>
                    <StyledTableCell component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        Pengambilan Barang
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                  </StyledTableRow>
                  {props?.invoice?.invoice_details.map((invoiceDetail: any) => (
                    <StyledTableRow key={invoiceDetail?.id}>
                      <StyledTableCell component='th' scope='row'>
                        {invoiceDetail?.products?.name}
                      </StyledTableCell>
                      <StyledTableCell align='right'>{invoiceDetail?.qty}</StyledTableCell>
                      <StyledTableCell align='right'>
                        {turnIntoCurrency(invoiceDetail.total_price / invoiceDetail.qty)}
                      </StyledTableCell>

                      <StyledTableCell align='right'>{turnIntoCurrency(invoiceDetail?.total_price)}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                  <StyledTableRow>
                    <StyledTableCell align='right'></StyledTableCell>

                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        Subtotal
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        {turnIntoCurrency(props?.invoice?.subtotal_product)}
                      </Typography>
                    </StyledTableCell>
                  </StyledTableRow>

                  <StyledTableRow>
                    <StyledTableCell component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        Penggabungan Nota
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                  </StyledTableRow>
                  {props?.invoice_merges.map((invoiceDetail: any) => (
                    <StyledTableRow key={invoiceDetail?.id}>
                      <StyledTableCell component='th' scope='row'>
                        {invoiceDetail?.invoices?.invoice_date} - Nota No #{invoiceDetail?.invoices?.invoice_number}
                      </StyledTableCell>
                      <StyledTableCell align='right'></StyledTableCell>
                      <StyledTableCell align='right'></StyledTableCell>
                      <StyledTableCell align='right'>
                        {turnIntoCurrency(invoiceDetail?.remaining_amount)}
                      </StyledTableCell>
                    </StyledTableRow>
                  ))}
                  <StyledTableRow>
                    <StyledTableCell align='right'></StyledTableCell>

                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        Subtotal
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        {turnIntoCurrency(props?.invoice?.subtotal_merge)}
                      </Typography>
                    </StyledTableCell>
                  </StyledTableRow>

                  <StyledTableRow>
                    <StyledTableCell component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        Retur Produk
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                  </StyledTableRow>
                  {props?.invoice?.invoice_returns.map((invoiceDetail: any) => (
                    <StyledTableRow key={invoiceDetail?.id}>
                      <StyledTableCell component='th' scope='row'>
                        {invoiceDetail?.products?.name}
                      </StyledTableCell>
                      <StyledTableCell align='right' sx={{ width: '20px' }}>
                        {invoiceDetail?.qty}
                      </StyledTableCell>
                      <StyledTableCell align='right'>
                        {turnIntoCurrency(invoiceDetail.total_price / invoiceDetail.qty)}
                      </StyledTableCell>
                      <StyledTableCell align='right'>{turnIntoCurrency(invoiceDetail?.total_price)}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                  <StyledTableRow>
                    <StyledTableCell align='right'></StyledTableCell>

                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        Subtotal
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        {turnIntoCurrency(props?.invoice?.subtotal_return)}
                      </Typography>
                    </StyledTableCell>
                  </StyledTableRow>

                  <StyledTableRow>
                    <StyledTableCell component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        Pembayaran
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right'></StyledTableCell>
                  </StyledTableRow>
                  {props?.invoice?.invoice_payments.map((invoiceDetail: any) => (
                    <StyledTableRow key={invoiceDetail?.id}>
                      <StyledTableCell component='th' scope='row'>
                        {formatdate(invoiceDetail?.paid_at)} -{invoiceDetail?.payment_method}{' '}
                        {invoiceDetail?.bank_account_name ? `- ${invoiceDetail?.bank_account_name} ` : ''}
                      </StyledTableCell>
                      <StyledTableCell align='right'></StyledTableCell>
                      <StyledTableCell align='right'></StyledTableCell>
                      <StyledTableCell align='right'>{turnIntoCurrency(invoiceDetail?.amount)}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                  <StyledTableRow>
                    <StyledTableCell align='right'></StyledTableCell>

                    <StyledTableCell align='right'></StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        Subtotal
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row'>
                      <Typography variant='body2' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                        {turnIntoCurrency(props?.invoice?.paid_amount)}
                      </Typography>
                    </StyledTableCell>
                  </StyledTableRow>

                  <StyledTableRow>
                    <StyledTableCell align='right' sx={{ background: '#ebe12a' }}></StyledTableCell>

                    <StyledTableCell align='right' sx={{ background: '#ebe12a' }}></StyledTableCell>
                    <StyledTableCell align='right' sx={{ background: '#ebe12a' }} component='th' scope='row'>
                      <Typography variant='body1' sx={{ fontWeight: 'bold', fontSize: '2vw' }} color='#000'>
                        TOTAL
                      </Typography>
                    </StyledTableCell>
                    <StyledTableCell align='right' component='th' scope='row' sx={{ background: '#ebe12a' }}>
                      <Typography variant='body1' sx={{ fontWeight: 'bold', fontSize: '2vw' }} color='#000'>
                        {turnIntoCurrency(props?.invoice?.remaining_amount)}
                      </Typography>
                    </StyledTableCell>
                  </StyledTableRow>
                </TableBody>
              </Table>
            </Grid>
          </Grid>

          <Box
            sx={{
              marginTop: '100px',
              marginBottom: '30px',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'start',
              width: '100%',
              gap: 2
            }}
          >
            <Box>
              <Typography variant='body1' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                PERHATIAN!
              </Typography>
              <Typography variant='body1' sx={{ fontWeight: 'bold', fontSize: '2vw' }}>
                Barang yang sudah dibeli tidak dapat ditukar/dikembalikan.
              </Typography>
            </Box>

            <Box>
              <table>
                <thead>
                  {company?.bank_accounts.map((bankAccount: any) => (
                    <tr key={bankAccount.bank}>
                      <td width={100}>
                        <Typography sx={{ fontWeight: 'bold', fontSize: '2vw' }}>{bankAccount.bank}</Typography>
                      </td>
                      <td>
                        <Typography variant='body1' sx={{ fontSize: '2vw' }}>
                          : {'\t'}
                          {bankAccount.accountNo} - A/N {bankAccount.pic}
                        </Typography>
                      </td>
                    </tr>
                  ))}
                </thead>
              </table>
            </Box>
          </Box>
        </div>
      </body>
    </>
  )
}

export default InvoiceTemplate
