import { Dialog, DialogTitle } from '@mui/material'

interface Props {
  show: boolean
  onClose: any
  formComponent: any
  title: string
}

const FormModal = (props: Props) => {
  const { show, onClose, formComponent, title } = props

  return (
    <>
      <Dialog open={show} onClose={onClose} sx={{ marginBottom: '20px' }}>
        <DialogTitle>{title}</DialogTitle>
        {formComponent}
      </Dialog>
    </>
  )
}

export default FormModal
