import { Table, TableRow, TableBody, TableHead, TableCell, TableContainer, IconButton } from '@mui/material'
import ConfirmDialog from 'src/views/ConfirmDialog'
import { Pencil, TrashCan } from 'mdi-material-ui'
import { useState } from 'react'
import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'

const ListUserTable = (props: {
  users: any[]
  onDelete: (id: string | number | any) => any
  onEdit: (id: string | number | any, user: any) => any
}) => {
  const { users, onDelete, onEdit } = props
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false)
  const [selectedId, setSelectedId] = useState<any>(null)
  const profile = useProfile()

  return (
    <>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>NO</TableCell>
              <TableCell>Nama Lengkap</TableCell>
              <TableCell>No HP</TableCell>
              <TableCell>Tipe Admin</TableCell>
              <TableCell>Aksi</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user: any, index: number) => (
              <TableRow key={user?.id}>
                <TableCell>{index + 1}</TableCell>
                <TableCell>{user?.fullname}</TableCell>
                <TableCell>{user?.phone}</TableCell>
                <TableCell>{user?.roles?.name}</TableCell>
                <TableCell>
                  {profile?.id !== user?.id ? (
                    <>
                      <IconButton
                        color='warning'
                        onClick={() => {
                          setSelectedId(user?.id)
                          onEdit(user?.id, user)
                        }}
                      >
                        <Pencil />
                      </IconButton>
                      <IconButton
                        color='error'
                        onClick={() => {
                          setSelectedId(user?.id)
                          setShowDeleteConfirm(true)
                        }}
                      >
                        <TrashCan />
                      </IconButton>
                    </>
                  ) : (
                    <> </>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <ConfirmDialog
        show={showDeleteConfirm}
        setShow={setShowDeleteConfirm}
        title={`Anda yakin ingin menghapus user ini?`}
        message='Hapus User Admin'
        action={() => {
          onDelete(selectedId)
          setShowDeleteConfirm(false)
        }}
      />
    </>
  )
}

export default ListUserTable
