// ** React Imports
import { ReactElement, useCallback, useEffect, useState } from 'react'
import { useRouter } from 'next/router'

// ** MUI Imports
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Avatar from '@mui/material/Avatar'
import CardHeader from '@mui/material/CardHeader'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'

// ** Icons Imports
import TrendingUp from 'mdi-material-ui/TrendingUp'
import CurrencyUsd from 'mdi-material-ui/CurrencyUsd'
import DotsVertical from 'mdi-material-ui/DotsVertical'
import CellphoneLink from 'mdi-material-ui/CellphoneLink'
import AccountOutline from 'mdi-material-ui/AccountOutline'

// ** Types
import { ThemeColor } from 'src/@core/layouts/types'

import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'
import { useSupabaseClient } from '@supabase/auth-helpers-react'

interface DataType {
  stats: string
  title: string
  color: ThemeColor
  icon: ReactElement
}

const salesData: DataType[] = [
  {
    stats: '23',
    title: 'Total Nota Jalan',
    color: 'primary',
    icon: <TrendingUp sx={{ fontSize: '1.75rem' }} />
  },
  {
    stats: '11',
    title: 'Total Nota Mati',
    color: 'success',
    icon: <AccountOutline sx={{ fontSize: '1.75rem' }} />
  },
  {
    stats: 'Rp. 145.000.000',
    color: 'warning',
    title: 'Sisa Piutang Nota Jalan',
    icon: <CellphoneLink sx={{ fontSize: '1.75rem' }} />
  },
  {
    stats: 'Rp. 133.000.000',
    color: 'info',
    title: 'Sisa Piutang Nota Mati',
    icon: <CurrencyUsd sx={{ fontSize: '1.75rem' }} />
  }
]

const renderStats = (props?: { profileId?: string }) => {
  const profile = useProfile()
  const supabase = useSupabaseClient()
  const [notaMati, setNotaMati] = useState(0)
  const [notaJalan, setNotaJalan] = useState(0)
  const [totalNotaJalan, setTotalNotaJalan] = useState(0)
  const [totalNotaMati, setTotalNotaMati] = useState(0)
  const [total, setTotal] = useState(0)

  const fetchData = useCallback(async () => {
    if (props?.profileId) {
      const { data: notaJalanD, count: notaJalanCount } = await supabase
        .from('invoices')
        .select('id, remaining_amount, deleted_at', { count: 'exact' })
        .eq('invoice_status_id', 1)
        .is('deleted_at', null)
        .eq('profile_id', props?.profileId)
      const { data: notaMatiD, count: notaMatiCount } = await supabase
        .from('invoices')
        .select('id, remaining_amount, deleted_at', { count: 'exact' })
        .is('deleted_at', null)
        .eq('invoice_status_id', 2)
        .eq('profile_id', props?.profileId)

      const [totalNotaJalan, totalNotaMati] = [sum41(notaJalanD as any), sum41(notaMatiD as any)]
      const totalPiutang = totalNotaJalan ?? 0 + totalNotaMati ?? 0

      setState([
        {
          stats: String(notaJalanCount),
          title: 'Total Nota Jalan',
          color: 'primary',
          icon: <TrendingUp sx={{ fontSize: '1.75rem' }} />
        },
        {
          stats: String(notaMatiCount),
          title: 'Total Nota Mati',
          color: 'success',
          icon: <AccountOutline sx={{ fontSize: '1.75rem' }} />
        },
        {
          stats: new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(totalNotaJalan),
          color: 'warning',
          title: 'Sisa Piutang Nota Jalan',
          icon: <CellphoneLink sx={{ fontSize: '1.75rem' }} />
        },
        {
          stats: new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(totalNotaMati),
          color: 'info',
          title: 'Sisa Piutang Nota Mati',
          icon: <CurrencyUsd sx={{ fontSize: '1.75rem' }} />
        }
      ])

      return
    }

    const { data: notaJalanD, count: notaJalanCount } = await supabase
      .from('invoices')
      .select('id, remaining_amount, deleted_at', { count: 'exact' })
      .eq('invoice_status_id', 1)
    const { data: notaMatiD, count: notaMatiCount } = await supabase
      .from('invoices')
      .select('id, remaining_amount, deleted_at', { count: 'exact' })
      .eq('invoice_status_id', 2)

    const [totalNotaJalan, totalNotaMati] = [sum41(notaJalanD as any), sum41(notaMatiD as any)]
    const totalPiutang = totalNotaJalan ?? 0 + totalNotaMati ?? 0

    setState([
      {
        stats: String(notaJalanCount),
        title: 'Total Nota Jalan',
        color: 'primary',
        icon: <TrendingUp sx={{ fontSize: '1.75rem' }} />
      },
      {
        stats: String(notaMatiCount),
        title: 'Total Nota Mati',
        color: 'success',
        icon: <AccountOutline sx={{ fontSize: '1.75rem' }} />
      },
      {
        stats: new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(totalNotaJalan),
        color: 'warning',
        title: 'Sisa Piutang Nota Jalan',
        icon: <CellphoneLink sx={{ fontSize: '1.75rem' }} />
      },
      {
        stats: new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(totalNotaMati),
        color: 'info',
        title: 'Sisa Piutang Nota Mati',
        icon: <CurrencyUsd sx={{ fontSize: '1.75rem' }} />
      }
    ])
  }, [])

  const sum41 = (val: any[]) => {
    let total = 0
    for (const v of val) {
      total += v.remaining_amount
    }

    return total
  }

  useEffect(() => {
    fetchData()
  }, [])

  const [state, setState] = useState<DataType[]>([])

  return state.map((item: DataType, index: number) => (
    <Grid item xs={12} sm={3} key={index}>
      <Box key={index} sx={{ display: 'flex', alignItems: 'center' }}>
        <Avatar
          variant='rounded'
          sx={{
            mr: 3,
            width: 44,
            height: 44,
            boxShadow: 3,
            color: 'common.white',
            backgroundColor: `${item.color}.main`
          }}
        >
          {item.icon}
        </Avatar>
        <Box sx={{ display: 'flex', flexDirection: 'column' }}>
          <Typography variant='caption'>{item.title}</Typography>
          <Typography variant='h6'>{item.stats}</Typography>
        </Box>
      </Box>
    </Grid>
  ))
}

const StatisticsCard = (props?: { profileId?: string }) => {
  const [total, setTotal] = useState(0)
  const supabase = useSupabaseClient()

  const fetchData = useCallback(async () => {
    let notaJalanD = []
    if (props?.profileId) {
      notaJalanD = (
        await supabase
          .from('invoices')
          .select('id, remaining_amount', { count: 'exact' })
          .in('invoice_status_id', [1, 2])
          .is('deleted_at', null)
          .eq('profile_id', props?.profileId)
      ).data as any
    } else {
      notaJalanD = (
        await supabase
          .from('invoices')
          .select('id, remaining_amount', { count: 'exact' })
          .is('deleted_at', null)
          .in('invoice_status_id', [1, 2])
      ).data as any
    }

    const total = sum41(notaJalanD as any)

    setTotal(total)
  }, [])

  const sum41 = (val: any[]) => {
    let total = 0
    for (const v of val) {
      total += v.remaining_amount
    }

    return total
  }

  useEffect(() => {
    fetchData()
  }, [])

  const router = useRouter()

  useEffect(() => {
    const { refresh } = router.query

    if (refresh) fetchData()
  }, [router.query])

  return (
    <Card>
      <CardHeader
        title='Total Piutang'
        subheader={
          <Typography variant='h4' component='h4'>
            {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(total)}
          </Typography>
        }
        titleTypographyProps={{
          sx: {
            mb: 2.5,
            lineHeight: '2rem !important',
            letterSpacing: '0.15px !important'
          }
        }}
      />
      <CardContent sx={{ pt: theme => `${theme.spacing(3)} !important` }}>
        <Grid container spacing={[5, 0]}>
          {renderStats(props)}
        </Grid>
      </CardContent>
    </Card>
  )
}

export default StatisticsCard
