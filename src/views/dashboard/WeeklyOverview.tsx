// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Button from '@mui/material/Button'
import { useTheme } from '@mui/material/styles'
import CardHeader from '@mui/material/CardHeader'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'

// ** Icons Imports
import DotsVertical from 'mdi-material-ui/DotsVertical'

// ** Third Party Imports
import { ApexOptions } from 'apexcharts'

// ** Custom Components Imports
import ReactApexcharts from 'src/@core/components/react-apexcharts'

const WeeklyOverview = (props: { moneyIn: number; moneyOut: number }) => {
  const { moneyIn, moneyOut } = props

  // ** Hook
  const theme = useTheme()

  const options: ApexOptions = {
    labels: ['Uang Masuk', 'Uang Keluar'],
    chart: {
      width: 600
    },
    plotOptions: {
      pie: {
        expandOnClick: true
      }
    },
    dataLabels: {
      enabled: true,
      formatter: (val: any) => `${val} %`,
      textAnchor: 'end'
    }
  }

  return (
    <Card>
      <CardHeader
        title='Laporan Keuangan'
        titleTypographyProps={{
          sx: { lineHeight: '2rem !important', letterSpacing: '0.15px !important' }
        }}
      />
      <CardContent sx={{ '& .apexcharts-xcrosshairs.apexcharts-active': { opacity: 0 } }}>
        <ReactApexcharts type='donut' options={options} series={[moneyIn, moneyOut]} />
      </CardContent>
    </Card>
  )
}

export default WeeklyOverview
