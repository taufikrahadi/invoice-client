// ** Third Party Imports
import { ApexOptions } from 'apexcharts'
import { Card, CardContent, CardHeader } from '@mui/material'

// ** Custom Components Imports
import ReactApexcharts from 'src/@core/components/react-apexcharts'

interface Props {
  categories: (string | number)[]
  dataSeries: { name: string; data: number[] }[]
  colors: string[]
  yAxisOption: any
}

const LineChartMoney = (props: Props) => {
  const { categories, dataSeries, colors, yAxisOption } = props
  const options: ApexOptions = {
    chart: {
      height: 350,
      type: 'line',
      stacked: false
    },
    dataLabels: {
      enabled: false
    },
    colors: ['#FF1654', '#247BA0'],
    series: dataSeries,
    stroke: {
      width: [4, 4]
    },
    plotOptions: {
      bar: {
        columnWidth: '20%'
      }
    },
    xaxis: {
      categories
    },
    yaxis: yAxisOption,
    tooltip: {
      shared: true,
      intersect: false,
      y: {
        formatter: val => {
          return `Rp ${val}`
        }
      }
    },
    legend: {
      horizontalAlign: 'left',
      offsetX: 40,
      formatter: val => `Rp ${val}`
    }
  }

  return (
    <>
      <Card>
        <CardHeader
          title='Uang Masuk'
          titleTypographyProps={{
            sx: { lineHeight: '2rem !important', letterSpacing: '0.15px !important' }
          }}
        />
        <CardContent sx={{ '& .apexcharts-xcrosshairs.apexcharts-active': { opacity: 0 } }}>
          <ReactApexcharts type='line' options={options} series={dataSeries} />
        </CardContent>
      </Card>
    </>
  )
}

export default LineChartMoney
