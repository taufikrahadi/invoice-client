import { Card, CardActionArea, CardContent, Grid, Box, Typography, Chip } from '@mui/material'

interface Props {
  name: string
  code: string
  money: number
  categoryOrSomething: string
  onClick: () => void
}

const DataCard = (props: Props) => {
  const { name, code, money, categoryOrSomething, onClick } = props
  
return (
    <Card>
      <CardActionArea onClick={onClick}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <CardContent>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  width: '100%'
                }}
              >
                <Typography variant='body1' sx={{ marginBottom: 3 }}>
                  {name}
                </Typography>

                <Typography variant='body2' sx={{ marginBottom: 3 }}>
                  {code}
                </Typography>
              </Box>

              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  width: '100%'
                }}
              >
                <Typography sx={{ fontWeight: 500, marginBottom: 3 }}>
                  <Box component='span' sx={{ fontWeight: 'bold' }}>
                    {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(money)}
                  </Box>
                </Typography>

                <Typography sx={{ marginBottom: 3 }}>
                  <Chip label={categoryOrSomething} color='primary' />
                </Typography>
              </Box>
            </CardContent>
          </Grid>
        </Grid>
      </CardActionArea>
    </Card>
  )
}

export default DataCard
