import { Box, IconButton, Button } from '@mui/material'
import { ArrowLeft, Pencil, Delete } from 'mdi-material-ui'
import { useRouter } from 'next/router'

interface Props {
  dontShowEdit?: boolean
  deleteOnClick?: () => void
  showDeleteButton?: boolean
  editOnClick: () => void
  children?: any
}

const DetailCardHeader = (props: Props) => {
  const { children, dontShowEdit, deleteOnClick, editOnClick, showDeleteButton } = props
  const router = useRouter()
  
return (
    <Box
      sx={{
        padding: '10px',
        display: 'flex',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between'
      }}
    >
      <IconButton onClick={() => router.back()} color='secondary'>
        <ArrowLeft />
      </IconButton>
      <Box>
        {children}
        {!dontShowEdit ? (
          <Button variant='text' color='info' onClick={editOnClick}>
            <Pencil sx={{ color: 'info', marginRight: 1.5 }} />
            Edit
          </Button>
        ) : (
          <></>
        )}

        {showDeleteButton ? (
          <Button variant='text' color='error' onClick={deleteOnClick}>
            <Delete sx={{ color: 'error', marginRight: 1.5 }} />
            Delete
          </Button>
        ) : (
          <></>
        )}
      </Box>
    </Box>
  )
}

export default DetailCardHeader
