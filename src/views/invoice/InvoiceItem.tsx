import {
  CardActions,
  Chip,
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardHeader,
  IconButton,
  Grid,
  Typography,
  Badge
} from '@mui/material'
import { LockClock } from 'mdi-material-ui'
import ConfirmDialog from 'src/views/ConfirmDialog'
import { useState, useCallback } from 'react'
import { useRouter } from 'next/router'
import { isEmpty } from 'class-validator'
import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'

export class InvoiceItemProps {
  id?: number
  status?: string
  remaining_amount?: number
  invoice_number?: string
  partner_name?: string
  partner_id?: string
  invoice_date?: string
  code?: string
  read_at?: string
  updateStatusAction?: any
}

const mapStatusColor = (status: string) => {
  switch (status) {
    case 'Nota Jalan':
      return 'success'
    case 'Nota Mati':
      return 'secondary'
    default:
      return 'error'
  }
}

const InvoiceCard = (props: InvoiceItemProps) => {
  const {
    id,
    status,
    remaining_amount,
    invoice_number,
    partner_name,
    partner_id,
    invoice_date,
    read_at,
    updateStatusAction
  } = props
  const router = useRouter()
  const profile = useProfile()

  const isOwnedInvoice = useCallback(() => {
    if (profile?.id === partner_id) {
      if (isEmpty(read_at)) return true
    }

    return false
  }, [profile, props, partner_id])

  return (
    <Card sx={{ width: '100%' }}>
      <CardActionArea onClick={() => router.push(`/invoices/${partner_id}/${id}/detail`)}>
        {isOwnedInvoice() ? (
          <CardHeader
            sx={{
              paddingRight: '35px'
            }}
            action={<Badge color='primary' badgeContent='BARU' />}
          />
        ) : (
          <></>
        )}
        <CardContent>
          <Box
            sx={{
              display: 'flex',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginBottom: '10px'
            }}
          >
            <Typography variant='body1'>{invoice_date}</Typography>

            <Typography variant='h6'></Typography>
          </Box>
          <Box
            sx={{
              display: 'flex',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginBottom: '10px'
            }}
          >
            <Typography variant='body1' sx={{ fontWeight: 'bold' }}>
              {partner_name}
            </Typography>

            <Typography variant='h6'># {invoice_number}</Typography>
          </Box>

          <Box
            sx={{
              display: 'flex',
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginBottom: '10px'
            }}
          >
            <Typography sx={{ fontWeight: 'bold' }}>
              {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(remaining_amount ?? 0)}
            </Typography>

            <Typography variant='body1'>
              <Chip label={status} color={mapStatusColor(status as any)} />
            </Typography>
          </Box>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

const InvoiceItem = (props: InvoiceItemProps) => {
  const { id, code, updateStatusAction } = props
  const [showConfirm, setShowConfirm] = useState<boolean>(false)

  return (
    <>
      <ConfirmDialog
        show={showConfirm}
        setShow={setShowConfirm}
        title={`Ubah nota '${code}' menjadi Nota Mati?`}
        message='Ubah status nota'
        action={() => {
          updateStatusAction(id).then(() => setShowConfirm(false))
        }}
      />

      <InvoiceCard {...props} updateStatusAction={() => setShowConfirm(true)} />
    </>
  )
}

export default InvoiceItem
