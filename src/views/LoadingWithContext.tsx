import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { Backdrop, CircularProgress, Typography } from '@mui/material'

const Loading = () => {
  const { loading, loadingText } = useLoadingUsingContext()

  return (
    <Backdrop
      sx={{
        color: '#fff',
        zIndex: theme => theme.zIndex.drawer + 9999999
      }}
      open={loading}
    >
      <CircularProgress color='inherit' />

      <Typography>{loadingText}</Typography>
    </Backdrop>
  )
}

export default Loading
