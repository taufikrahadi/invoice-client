import { Alert, AlertColor, Snackbar } from '@mui/material'

export interface ToastProps {
  show: boolean
  message: string
  severity: AlertColor
  onClose?(): void
}

const Toast = (props: ToastProps) => (
  <Snackbar
    open={props.show}
    autoHideDuration={6000}
    onClose={props.onClose}
    anchorOrigin={{
      vertical: 'top',
      horizontal: 'right'
    }}
  >
    <Alert severity={props.severity} sx={{ width: '100%' }}>
      {props.message}
    </Alert>
  </Snackbar>
)

export default Toast
