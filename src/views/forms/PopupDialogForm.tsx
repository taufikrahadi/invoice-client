import { Dialog } from '@mui/material'

const PopUpDialogForm = (props: { show: boolean; onClose: () => void; children: any }) => {
  return (
    <>
      <Dialog open={props.show} onClose={props.onClose}>
        {props.children}
      </Dialog>
    </>
  )
}

export default PopUpDialogForm
