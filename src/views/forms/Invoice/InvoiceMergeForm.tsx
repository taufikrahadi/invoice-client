import { InvoiceMerge } from 'src/pages/invoices/new'
import PopUpDialogForm from '../PopupDialogForm'
import { DialogContent, DialogActions, Button, Autocomplete, TextField, Grid } from '@mui/material'

const InvoiceMergeForm = (props: {
  onClose: () => void
  show: boolean
  form: InvoiceMerge
  setForm: any
  invoices: any[]
  handleSubmit: (e: any) => void
  errors?: any[]
}) => {
  return (
    <>
      <PopUpDialogForm show={props.show} onClose={props.onClose}>
        <form onSubmit={props.handleSubmit}>
          <DialogContent>
            <Grid container spacing={6}>
              <Grid item xs={12}>
                <Autocomplete
                  options={props.invoices}
                  value={props.form.from_invoice}
                  getOptionLabel={(option: any) => {
                    return option.invoice_number ? option.invoice_number : props.form.from_invoice.invoice_number
                  }}
                  onChange={(_, newValue: any) => {
                    console.log(newValue.id)
                    props.setForm({
                      ...props.form,
                      from_invoice_id: newValue.id,
                      remaining_amount: newValue.remaining_amount,
                      from_invoice: newValue
                    })
                  }}
                  renderInput={params => (
                    <TextField
                      {...params}
                      label='Pilih Nota'
                      variant='filled'

                      // error={findIsError('product_id', errors as any)}
                      // helperText={findErrorProperty('product_id', errors as any)?.errors.join(', ')}
                    />
                  )}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label='Sisa Hutang'
                  fullWidth
                  variant='filled'
                  disabled
                  value={new Intl.NumberFormat('id-ID', { style: 'decimal' }).format(
                    props.form.from_invoice?.remaining_amount ?? 0
                  )}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  multiline
                  rows={4}
                  variant='filled'
                  fullWidth
                  label='Catatan'
                  disabled={!props.form.from_invoice_id}
                  value={props.form.description}
                  onChange={(e: any) => {
                    props.setForm({
                      ...props.form,
                      description: e.target.value
                    })
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>

          <DialogActions>
            <Button variant='text' color='secondary' onClick={props.onClose}>
              Batalkan
            </Button>
            <Button variant='contained' color='primary' onClick={props.handleSubmit}>
              Tambahkan
            </Button>
          </DialogActions>
        </form>
      </PopUpDialogForm>
    </>
  )
}

export default InvoiceMergeForm
