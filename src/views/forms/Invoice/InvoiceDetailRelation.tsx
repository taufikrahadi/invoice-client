import { Typography, Box, Card, CardHeader, CardContent } from '@mui/material'
import { isNotEmpty } from 'class-validator'

interface Props {
  data: any[]
  titleText: string
  additional?: (row: any) => any
}

const InvoiceDetailRelation = (props: Props) => {
  const { titleText, data, additional } = props
  
return (
    <>
      <Typography variant='subtitle1' sx={{ marginBottom: '5px' }}>
        {titleText}: {data.length ?? 0}
      </Typography>
      {data && data.length > 0 ? (
        data.map((invoiceDetail: any) => (
          <Card key={invoiceDetail.id} sx={{ marginY: '10px' }}>
            <CardHeader subheader={`${invoiceDetail?.products.name} (${invoiceDetail?.products.sku})`} />
            <CardContent>
              {additional ? additional(invoiceDetail) : <></>}
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <Typography variant='subtitle1'>Kuantitas</Typography>
                <Typography variant='body1' sx={{ textAlign: 'right' }}>
                  {invoiceDetail?.qty}
                </Typography>
              </Box>

              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <Typography variant='subtitle1'>Harga Jual Satuan</Typography>
                <Typography variant='body1' sx={{ textAlign: 'right' }}>
                  {new Intl.NumberFormat('en-US', { style: 'decimal' }).format(
                    invoiceDetail?.total_price / invoiceDetail?.qty
                  )}
                </Typography>
              </Box>

              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <Typography variant='subtitle1'>Total Harga</Typography>
                <Typography variant='body1' sx={{ textAlign: 'right' }}>
                  {new Intl.NumberFormat('en-US', { style: 'decimal' }).format(invoiceDetail?.total_price)}
                </Typography>
              </Box>
            </CardContent>
          </Card>
        ))
      ) : (
        <></>
      )}
    </>
  )
}

export default InvoiceDetailRelation
