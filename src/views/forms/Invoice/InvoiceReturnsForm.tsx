import { InvoiceReturn } from 'src/pages/invoices/new'
import CreateProductOnTheFly from './CreateProductOnTheFly'
import { DialogContent, Grid, Autocomplete, TextField, IconButton, DialogActions, Button } from '@mui/material'
import { Plus } from 'mdi-material-ui'
import PopUpDialogForm from '../PopupDialogForm'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useState, useEffect } from 'react'

const InvoiceReturnsForm = (props: {
  onClose: () => void
  show: boolean
  form: InvoiceReturn
  setForm: any
  products: any[]
  handleSubmit: (e: any) => void
  errors?: any[]
}) => {
  const { products: productList, form, setForm, errors, show, onClose, handleSubmit } = props
  const [showProductForm, setShowProductForm] = useState(false)
  const supabase = useSupabaseClient()
  const [products, setProducts] = useState<any>([])

  useEffect(() => {
    setProducts(productList)
  }, [props.products])

  useEffect(() => {
    ;(async () => {
      const { data, error } = await supabase
        .from('products')
        .select('id, name, sku, selling_price')
        .eq('is_exposed_to_list', true)
        .is('deleted_at', null)

      setProducts(data as any)
    })()
  }, [showProductForm])

  return (
    <>
      <PopUpDialogForm show={show} onClose={onClose}>
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <Grid container spacing={6}>
              <Grid item xs={12}>
                <Grid container spacing={1}>
                  <Grid item xs={10}>
                    <Autocomplete
                      options={products}
                      value={form.product_id}
                      getOptionLabel={(option: any) => {
                        return option.name ? option.name : form.product.name
                      }}
                      onChange={(_, newValue: any) => {
                        console.log(newValue.id)
                        setForm({
                          ...form,
                          product_id: newValue.id,
                          product: newValue,
                          total_price: newValue.selling_price * form.qty
                        })
                      }}
                      renderInput={params => (
                        <TextField
                          {...params}
                          label='Pilih Produk'
                          variant='filled'

                          // error={findIsError('product_id', errors as any)}
                          // helperText={findErrorProperty('product_id', errors as any)?.errors.join(', ')}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item>
                    <IconButton
                      onClick={() => {
                        setShowProductForm(true)
                      }}
                      color='primary'
                    >
                      <Plus fontSize='inherit' />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Harga'
                  type='number'
                  variant='filled'
                  onChange={(e: any) => {
                    const qty = form.qty
                    const productPrice = e.target.value

                    const total_price = qty * productPrice

                    setForm({
                      ...form,
                      product: {
                        ...form.product,
                        selling_price: productPrice
                      },
                      total_price
                    })
                  }}
                  value={form.product?.selling_price ?? 0}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  fullWidth
                  variant='filled'
                  label='Quantity'
                  type='number'
                  onChange={(e: any) => {
                    const qty = e.target.value
                    const productPrice = form.product?.selling_price ?? 0

                    const total_price = qty * productPrice

                    setForm({
                      ...form,
                      qty,
                      total_price
                    })
                  }}
                  defaultValue={form.qty}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField fullWidth variant='filled' label='Total Harga' type='number' value={form.total_price} />
              </Grid>
            </Grid>
          </DialogContent>

          <DialogActions>
            <Button variant='text' color='secondary' onClick={onClose}>
              Batalkan
            </Button>
            <Button variant='contained' color='primary' type='submit'>
              Tambahkan
            </Button>
          </DialogActions>
        </form>

        <CreateProductOnTheFly show={showProductForm} onClose={() => setShowProductForm(false)} />
      </PopUpDialogForm>
    </>
  )
}

export default InvoiceReturnsForm
