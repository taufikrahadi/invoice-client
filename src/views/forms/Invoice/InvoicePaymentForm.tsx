import { InvoicePayment } from 'src/pages/invoices/new'
import PopUpDialogForm from '../PopupDialogForm'
import {
  DialogContent,
  DialogActions,
  Button,
  Autocomplete,
  TextField,
  Grid,
  Select,
  MenuItem,
  FormControl,
  InputLabel
} from '@mui/material'
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import { useCallback, useEffect, useState } from 'react'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'

const InvoicePaymentForm = (props: {
  onClose: () => void
  show: boolean
  form: InvoicePayment
  setForm: any
  handleSubmit: (e: any) => void
  errors?: any[]
}) => {
  const supabase = useSupabaseClient()
  const { onClose, form, show, setForm, handleSubmit, errors } = props
  const [bankAccounts, setBankAccounts] = useState([])
  const profile = useProfile()

  const fetchBankAccount = useCallback(async () => {
    const { data } = await supabase.from('companies').select('bank_accounts').eq('id', 1).single()

    setBankAccounts(data?.bank_accounts as any)
  }, [supabase, profile, setBankAccounts])

  useEffect(() => {
    fetchBankAccount()
  }, [profile])

  return (
    <>
      <PopUpDialogForm show={props.show} onClose={props.onClose}>
        <form onSubmit={props.handleSubmit}>
          <DialogContent>
            <Grid container spacing={6}>
              <Grid item xs={6}>
                <TextField
                  label='Jumlah'
                  fullWidth
                  type='number'
                  onChange={(e: any) =>
                    setForm({
                      ...form,
                      amount: Number(e.target.value)
                    })
                  }
                  value={form.amount}
                />
              </Grid>

              <Grid item xs={6}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    label='Tanggal Dibayar'
                    value={form.paid_at}
                    onChange={(newValue: any) => {
                      setForm({
                        ...form,
                        paid_at: newValue
                      })
                    }}
                  />
                </LocalizationProvider>
              </Grid>

              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel>Metode Pembayaran</InputLabel>
                  <Select
                    value={form.payment_method}
                    onChange={(e: any) => {
                      setForm({
                        ...form,
                        payment_method: e.target.value,
                        bank_account_name: ''
                      })
                    }}
                  >
                    <MenuItem value='Transfer'>Transfer</MenuItem>
                    <MenuItem value='Cash'>Cash</MenuItem>
                  </Select>
                </FormControl>
              </Grid>

              {form.payment_method !== 'Cash' ? (
                <Grid item xs={12}>
                  <FormControl fullWidth>
                    <InputLabel>Akun Rekening</InputLabel>
                    <Select
                      value={form.bank_account_name}
                      onChange={(e: any) => {
                        setForm({
                          ...form,
                          bank_account_name: e.target.value
                        })
                      }}
                    >
                      {bankAccounts.map((bank: any) => (
                        <MenuItem value={bank.bank} key={bank.bank}>
                          {bank.bank}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
              ) : (
                <></>
              )}

              <Grid item xs={12}>
                <TextField
                  label='Catatan'
                  value={form.note}
                  multiline
                  rows={4}
                  fullWidth
                  onChange={(e: any) => {
                    setForm({
                      ...form,
                      note: e.target.value
                    })
                  }}
                />
              </Grid>
            </Grid>
          </DialogContent>

          <DialogActions>
            <Button variant='text' color='secondary' onClick={onClose}>
              Kembali
            </Button>
            <Button variant='contained' color='primary' type='submit'>
              Tambahkan
            </Button>
          </DialogActions>
        </form>
      </PopUpDialogForm>
    </>
  )
}

export default InvoicePaymentForm
