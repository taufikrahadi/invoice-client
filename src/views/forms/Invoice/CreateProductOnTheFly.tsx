import PopUpDialogForm from '../PopupDialogForm'
import NewProduct from 'src/pages/products/new'

interface Props {
  show: boolean
  onClose: () => void
}

const CreateProductOnTheFly = ({ show, onClose }: Props) => {
  return (
    <>
      <PopUpDialogForm show={show} onClose={onClose}>
        <NewProduct handleBack={onClose} />
      </PopUpDialogForm>
    </>
  )
}

export default CreateProductOnTheFly
