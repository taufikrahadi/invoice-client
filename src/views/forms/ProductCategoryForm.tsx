import { useCallback, useState } from 'react'
import { Grid, Card, CardContent, CardActions, Button, TextField, DialogContent, DialogActions } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import Toast from '../Toast'

export interface ProductCategoryFormProps {
  afterSave: () => void
}

const ProductCategoryForm = (props: ProductCategoryFormProps) => {
  const { afterSave } = props
  const supabase = useSupabaseClient()
  const [form, setForm] = useState({
    name: ''
  })
  const [isSuccess, setIsSuccess] = useState(false)

  const handleSubmit = useCallback(async (e: any, form: any) => {
    e.preventDefault()

    await supabase.from('product_categories').insert({
      name: form.name
    })
    afterSave()
    setIsSuccess(true)
  }, [])

  return (
    <>
      <Toast show={isSuccess} message={'Kategori produk sukses ditambahkan!'} severity='success' />

      <form onSubmit={(e: any) => handleSubmit(e, form)}>
        <DialogContent>
          <Grid container spacing={6}>
            <Grid item xs={12}>
              <TextField
                label='Nama Kategori'
                onChange={(e: any) => setForm({ name: e.target.value })}
                value={form.name}
                fullWidth
              />
            </Grid>
          </Grid>
        </DialogContent>

        <DialogActions>
          <Button type='submit' variant='contained' color='primary'>
            Simpan
          </Button>
        </DialogActions>
      </form>
    </>
  )
}

export default ProductCategoryForm
