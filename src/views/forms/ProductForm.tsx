import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Grid,
  TextField,
  Autocomplete,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent
} from '@mui/material'
import { Plus } from 'mdi-material-ui'
import { useCallback, useEffect, useState } from 'react'
import { useListProductCategory } from 'src/modules/product-category'
import ProductCategoryForm from './ProductCategoryForm'
import { useRouter } from 'next/router'
import { ProductFormSchema } from 'src/pages/products/new'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { useProductList } from 'src/modules/product'
import { useFormValidation } from 'src/@core/hooks/useForm'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { isEmpty, isNotEmpty } from 'class-validator'
import { findIsError, findErrorProperty } from 'src/@core/utils/validateSync'

export interface BaseFormProps<T> {
  title: string
  form: T
  save: (form: T) => Promise<void>
  handleBack?: any
}

export interface ProductFormProps {
  title: string
  form: ProductFormSchema
  save: (form: ProductFormSchema) => Promise<void>
}

const ProductForm = (props: BaseFormProps<ProductFormSchema>) => {
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const { form: formProps, save } = props
  const [showAddCategoryDialog, setShowAddCategoryDialog] = useState(false)
  const productCategories = useListProductCategory(showAddCategoryDialog)
  const router = useRouter()
  const [form, setForm] = useState<ProductFormSchema>(formProps)
  const { runValidation, errors } = useFormValidation()
  const handleChange = useHandleChange(setForm, form)

  useEffect(() => {
    setForm({
      ...formProps
    })
    stopLoading()
  }, [props])

  const handleSubmit = (e: any) => {
    e.preventDefault()
    startLoading()
    runValidation(new ProductFormSchema(form))
    console.log(errors)

    if (errors.length != 0) {
      stopLoading()
      console.log(errors)
      
return
    } else {
      save(form).then(() => {
        stopLoading()
      })
    }
  }
  
return (
    <>
      <Card>
        <CardHeader title={props.title} />

        <form onSubmit={handleSubmit}>
          <CardContent>
            <Grid container spacing={6}>
              <Grid item xs={6}>
                <TextField
                  variant='filled'
                  onChange={handleChange('name')}
                  value={form.name}
                  label='Nama Produk'
                  name='name'
                  fullWidth
                  error={findIsError('name', errors)}
                  helperText={findErrorProperty('name', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={6}>
                <TextField
                  defaultValue={form.sku}
                  onChange={handleChange('sku')}
                  value={form.sku}
                  variant='filled'
                  disabled
                  name='sku'
                  label='SKU'
                  fullWidth
                  error={findIsError('sku', errors)}
                  helperText={findErrorProperty('sku', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  variant='filled'
                  onChange={handleChange('selling_price')}
                  name='selling_price'
                  value={form.selling_price}
                  label='Harga Jual'
                  fullWidth
                  type='number'
                  error={findIsError('selling_price', errors)}
                  helperText={findErrorProperty('selling_price', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={12}>
                <Grid container spacing={1}>
                  <Grid item xs={10}>
                    <Autocomplete
                      options={productCategories}
                      value={form.product_categories}
                      getOptionLabel={(option: any) => {
                        console.log(option)
                        
return option.name
                      }}
                      onChange={(_, newValue: any) => {
                        console.log(newValue.id)
                        setForm({
                          ...form,
                          category_id: newValue.id
                        })
                      }}
                      renderInput={params => (
                        <TextField
                          {...params}
                          label='Kategori Produk'
                          variant='filled'
                          error={findIsError('category_id', errors)}
                          helperText={findErrorProperty('category_id', errors)?.errors.join(', ')}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item>
                    <IconButton onClick={() => setShowAddCategoryDialog(true)} color='primary'>
                      <Plus fontSize='inherit' />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label='Deskripsi'
                  onChange={handleChange('description')}
                  multiline
                  value={form.description}
                  rows={4}
                  variant='filled'
                  fullWidth
                  error={findIsError('description', errors)}
                  helperText={findErrorProperty('description', errors)?.errors.join(', ')}
                />
              </Grid>
            </Grid>
          </CardContent>

          <CardActions>
            <Box sx={{ display: 'flex', width: '100%', justifyContent: 'end' }}>
              <Button onClick={isNotEmpty(props?.handleBack) ? props?.handleBack : () => router.back()} variant='text'>
                Kembali
              </Button>
              <Button variant='contained' color='primary' type='submit'>
                Submit
              </Button>
            </Box>
          </CardActions>
        </form>
      </Card>

      <Dialog open={showAddCategoryDialog} onClose={() => setShowAddCategoryDialog(false)}>
        <DialogTitle>Tambah Kategori Produk</DialogTitle>
        <ProductCategoryForm afterSave={() => setShowAddCategoryDialog(false)} />
      </Dialog>
    </>
  )
}

export default ProductForm
