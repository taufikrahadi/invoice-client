import { Grid, Card, CardContent, Typography, Button } from '@mui/material'
import { DeleteOutline } from 'mdi-material-ui'

export class InvoiceFormSectionItemProps {
  children: any
  isShowDelete?: boolean
  handleDelete?: () => void
}

const InvoiceFormSectionItem = (props: InvoiceFormSectionItemProps) => {
  return (
    <>
      <Grid item xs={12}>
        <Card>
          <CardContent>
            <Grid container>
              <Grid item xs={9}>
                {props.children}
              </Grid>
              {props.isShowDelete ? (
                <Grid item xs={3}>
                  <Button variant='text' color='error' fullWidth onClick={props.handleDelete}>
                    <DeleteOutline />
                  </Button>
                </Grid>
              ) : (
                <></>
              )}
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </>
  )
}

export default InvoiceFormSectionItem
