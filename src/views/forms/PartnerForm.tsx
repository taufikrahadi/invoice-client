import { BaseFormProps } from './ProductForm'
import { PartnerFormSchema } from 'src/pages/partners/new'
import { useListRole } from 'src/modules/role'
import { useCallback, useState, useEffect } from 'react'
import { useFormValidation } from 'src/@core/hooks/useForm'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { findIsError, findErrorProperty } from 'src/@core/utils/validateSync'
import {
  Box,
  Card,
  Select,
  InputLabel,
  FormControl,
  FormHelperText,
  MenuItem,
  CardHeader,
  CardContent,
  CardActions,
  TextField,
  Button,
  Grid
} from '@mui/material'
import { useRouter } from 'next/router'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'

const PartnerForm = (props: BaseFormProps<PartnerFormSchema>) => {
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const { form: formProps, save, title } = props
  const router = useRouter()
  const { roles } = useListRole()
  const { runValidation, errors } = useFormValidation()
  const [form, setForm] = useState<PartnerFormSchema>(formProps)
  const handleChange = useHandleChange(setForm, form)

  const handleSave = useCallback(
    (e: any) => {
      e.preventDefault()
      startLoading()
      runValidation(new PartnerFormSchema(form))
      if (errors.length != 0) {
        stopLoading()
        
return
      } else {
        save(form).then(() => {
          stopLoading()
        })
      }
    },
    [form, runValidation, startLoading, stopLoading, save]
  )

  useEffect(() => {
    setForm({
      ...formProps,
      password: formProps.raw_password
    })
  }, [formProps])

  return (
    <>
      <Card sx={{ marginBottom: 10 }}>
        <CardHeader title={title} />

        <form onSubmit={handleSave}>
          <CardContent>
            <Grid container spacing={6}>
              <Grid item xs={12}>
                <TextField
                  label='Nomor Mitra *'
                  fullWidth
                  value={form.code}
                  disabled
                  variant='filled'
                  onChange={handleChange('code')}
                  error={findIsError('code', errors)}
                  helperText={findErrorProperty('code', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label='Nama Lengkap *'
                  fullWidth
                  value={form.fullname}
                  onChange={handleChange('fullname')}
                  variant='filled'
                  error={findIsError('fullname', errors)}
                  helperText={findErrorProperty('fullname', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={6}>
                <TextField
                  label='Username *'
                  variant='filled'
                  fullWidth
                  onChange={handleChange('username')}
                  value={form.username}
                  error={findIsError('username', errors)}
                  helperText={findErrorProperty('username', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={6}>
                <TextField
                  label='No HP *'
                  variant='filled'
                  value={form.phone}
                  onChange={handleChange('phone')}
                  fullWidth
                  error={findIsError('phone', errors)}
                  helperText={findErrorProperty('phone', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel>Tipe Mitra *</InputLabel>
                  <Select
                    labelId='demo-simple-select-label'
                    id='demo-simple-select'
                    fullWidth
                    label='Tipe Mitra *'
                    variant='filled'
                    value={form.role_id}
                    onChange={handleChange('role_id')}
                    error={findIsError('role_id', errors)}
                  >
                    {roles.map(role => (
                      <MenuItem key={role.id} value={role.id}>
                        {role.name}
                      </MenuItem>
                    ))}
                  </Select>
                  {findIsError('role_id', errors) ? (
                    <FormHelperText error>{findErrorProperty('role_id', errors)?.errors.join(', ')}</FormHelperText>
                  ) : (
                    <></>
                  )}
                </FormControl>
              </Grid>

              <Grid item xs={12}>
                <TextField
                  label='Password *'
                  fullWidth
                  variant='filled'
                  value={form.password}
                  onChange={handleChange('password')}
                  error={findIsError('password', errors)}
                  helperText={findErrorProperty('password', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Alamat'
                  variant='filled'
                  multiline
                  value={form.address}
                  onChange={handleChange('address')}
                  rows={4}
                  error={findIsError('address', errors)}
                  helperText={findErrorProperty('address', errors)?.errors.join(', ')}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  fullWidth
                  label='Deskripsi'
                  variant='filled'
                  multiline
                  value={form.description}
                  onChange={handleChange('description')}
                  rows={4}
                  error={findIsError('description', errors)}
                  helperText={findErrorProperty('description', errors)?.errors.join(', ')}
                />
              </Grid>
            </Grid>
          </CardContent>

          <CardActions>
            <Box sx={{ display: 'flex', justifyContent: 'end', width: '100%' }}>
              <Button variant={'text'} color='secondary' sx={{ marginRight: 4 }} onClick={() => router.back()}>
                Kembali
              </Button>
              <Button type='submit' variant='contained' color='primary'>
                Simpan
              </Button>
            </Box>
          </CardActions>
        </form>
      </Card>
    </>
  )
}

export default PartnerForm
