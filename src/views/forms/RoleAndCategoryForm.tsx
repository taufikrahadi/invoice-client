import { Grid, DialogContent, DialogActions, TextField, Box, Button } from '@mui/material'

interface Props {
  onSubmit: any
  form: any
  setForm: any
  onCancel: any
}

const RoleAndCategory = (props: Props) => {
  return (
    <>
      <form
        onSubmit={(e: any) => {
          e.preventDefault()
          props.onSubmit(props.form)
        }}
      >
        <DialogContent>
          <Grid container spacing={12}>
            <Grid item xs={12}>
              <TextField
                label='Nama'
                fullWidth
                required
                value={props.form.name}
                onChange={(e: any) => {
                  props.setForm({
                    ...props.form,
                    name: e.target.value
                  })
                }}
              />
            </Grid>
          </Grid>
        </DialogContent>

        <DialogActions>
          <Box
            sx={{
              display: 'flex',
              width: '100%',
              justifyContent: 'end',
              gap: 2
            }}
          >
            <Button variant='text' color='secondary' onClick={props.onCancel}>
              Batal
            </Button>
            <Button type='submit' variant='contained' color='primary'>
              Simpan
            </Button>
          </Box>
        </DialogActions>
      </form>
    </>
  )
}

export default RoleAndCategory
