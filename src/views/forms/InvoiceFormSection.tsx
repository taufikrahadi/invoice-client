import { Card, CardContent, CardActions, Grid, Button, Typography, Box } from '@mui/material'

export class InvoiceFormSectionProps {
  buttonTitle: string
  isShowSubtotal?: boolean
  subTotal?: number
  children: any
  handleAdd: () => void
  show?: boolean
}

const InvoiceFormSection = (props: InvoiceFormSectionProps) => {
  const { buttonTitle, isShowSubtotal, subTotal, children, handleAdd, show } = props

  if (!show) return <></>

  return (
    <>
      <Card sx={{ marginY: '10px' }}>
        <CardContent>
          <Grid container spacing={4}>
            {children}
          </Grid>
        </CardContent>

        <CardActions>
          <Grid container spacing={4}>
            {isShowSubtotal ? (
              <Grid item xs={12}>
                <Box sx={{ display: 'flex', justifyContent: 'end', flexDirection: 'column', width: '100%' }}>
                  <Typography variant='body1'>Subtotal</Typography>
                  <Typography variant='subtitle1'>
                    {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(subTotal ?? 0)}
                  </Typography>
                </Box>
              </Grid>
            ) : (
              <></>
            )}

            <Grid item xs={12}>
              <Button variant='contained' color='primary' fullWidth onClick={handleAdd}>
                {buttonTitle}
              </Button>
            </Grid>
          </Grid>
        </CardActions>
      </Card>
    </>
  )
}

export default InvoiceFormSection
