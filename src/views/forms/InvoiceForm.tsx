import { useCallback, useEffect, useState } from 'react'
import { findIsError, findErrorProperty } from 'src/@core/utils/validateSync'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { BaseFormProps } from './ProductForm'
import { InvoiceFormSchema, InvoiceDetail, InvoiceMerge, InvoicePayment, InvoiceReturn } from 'src/pages/invoices/new'
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Grid,
  TextField,
  Autocomplete,
  Typography,
  CardActions,
  Box,
  IconButton,
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions
} from '@mui/material'
import { DeleteOutline, Plus } from 'mdi-material-ui'
import { useFormValidation } from 'src/@core/hooks/useForm'
import { useRouter } from 'next/router'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import InvoiceFormSection from './InvoiceFormSection'
import InvoiceFormSectionItem from './InvoiceFormSectionItem'
import InvoiceDetailForm from './Invoice/InvoiceDetailForm'
import InvoiceMergeForm from './Invoice/InvoiceMergeForm'
import InvoiceReturnsForm from './Invoice/InvoiceReturnsForm'
import InvoicePaymentForm from './Invoice/InvoicePaymentForm'
import { isNotEmpty } from 'class-validator'
import dayjs from 'dayjs'
import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'
import { isEmpty } from 'class-validator'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'

const InvoiceForm = (
  props: BaseFormProps<InvoiceFormSchema> & { previousPayment?: any[]; disabledPartner?: boolean }
) => {
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const { form: formProps, save, title, previousPayment, disabledPartner } = props
  const [partners, setPartners] = useState<any[]>([])
  const [products, setProducts] = useState<any[]>([])
  const [invoices, setInvoices] = useState<any[]>([])
  const router = useRouter()
  const profile = useProfile()
  const { runValidation, errors } = useFormValidation()
  const [form, setForm] = useState<InvoiceFormSchema>(formProps)
  const handleChange = useHandleChange(setForm, form)
  const [triggerFetchDeps, setTriggerFetchDeps] = useState<boolean>(true)
  const [showInvoiceSectionModal, setShowInvoiceSectionModal] = useState<string | null>(null)
  const [invoiceDetailForm, setInvoiceDetailForm] = useState<InvoiceDetail>({
    product_id: null as any,
    qty: 1,
    total_price: 0,
    product: {}
  })
  const [invoiceMergeForm, setInvoiceMergeForm] = useState<InvoiceMerge>({
    from_invoice_id: null as any,
    description: '',
    from_invoice: {},
    remaining_amount: 0
  })
  const [invoiceReturnForm, setInvoiceReturnForm] = useState<InvoiceReturn>({
    product_id: null as any,
    qty: 1,
    total_price: 0,
    note: '',
    product: {}
  })
  const [invoicePaymentForm, setInvoicePaymentForm] = useState<InvoicePayment>({
    amount: null as any,
    note: '',
    paid_at: dayjs(),
    payment_method: '',
    bank_account_name: ''
  })

  const supabase = useSupabaseClient()

  const listPartners = useCallback(async () => {
    const { data: roles } = await supabase.from('roles').select('id').eq('type', 'Partner')
    const { data, error } = await supabase
      .from('profiles')
      .select('id, fullname, code')
      .is('deleted_at', null)
      .in(
        'role_id',
        (roles as any[]).map(role => role.id)
      )

    setPartners(data as any)
  }, [supabase, setPartners])
  const listProducts = useCallback(async () => {
    const { data, error } = await supabase
      .from('products')
      .select('id, name, sku, selling_price')
      .eq('is_exposed_to_list', true)
      .is('deleted_at', null)

    setProducts(data as any)
  }, [setProducts, supabase])
  const listInvoices = useCallback(async () => {
    if (!form.profile_id) return

    const query = supabase.from('invoices')
      .select()
      .eq('profile_id', form.profile_id)
      .lt('invoice_status_id', 2)
      .is('deleted_at', null)

    if (!isEmpty(form.id)) query.not('id', 'eq', form?.id)

    const { data } = await query
    setInvoices(data as any)
  }, [form, setInvoices])

  useEffect(() => {
    startLoading()

    Promise.all([listPartners(), listProducts()]).then(() => {
      setTriggerFetchDeps(false)
      stopLoading()
    })
  }, [])

  const findPreviousPaymentNominal = (previousPayment: any[]) => {
    let total = 0
    for (const invoice of previousPayment) {
      total += Math.abs(invoice.remaining_amount)
    }

    return total
  }

  useEffect(() => {
    if (previousPayment)
      setForm({
        ...formProps,
        payment_total: findPreviousPaymentNominal(previousPayment)
      })
    else setForm(formProps)
  }, [formProps])

  useEffect(() => {
    startLoading()

    Promise.all([listPartners(), listProducts()]).then(() => {
      setTriggerFetchDeps(false)
      stopLoading()
    })
  }, [triggerFetchDeps])

  useEffect(() => {
    if (form.profile_id) listInvoices()
  }, [form.profile_id])

  const handleSubmit = (e: any) => {
    e.preventDefault()
    startLoading()

    save(form).then(() => {
      stopLoading()
    })
  }

  return (
    <>
      <InvoicePaymentForm
        show={showInvoiceSectionModal === 'invoice-payment'}
        form={invoicePaymentForm}
        setForm={setInvoicePaymentForm}
        handleSubmit={(e: any) => {
          e.preventDefault()
          setShowInvoiceSectionModal(null)
          const payment_total = form.payment_total + invoicePaymentForm.amount
          setForm({
            ...form,
            payment_total,
            invoice_payments: [...form.invoice_payments, invoicePaymentForm]
          })
        }}
        onClose={() => {
          setInvoicePaymentForm({
            amount: null as any,
            note: '',
            paid_at: dayjs(),
            payment_method: '',
            bank_account_name: ''
          })
          setShowInvoiceSectionModal(null)
        }}
      />
      <InvoiceReturnsForm
        show={showInvoiceSectionModal === 'invoice-returns'}
        form={invoiceReturnForm}
        products={products}
        onClose={() => {
          setInvoiceReturnForm({
            product_id: null as any,
            qty: 1,
            total_price: 0,
            note: '',
            product: {}
          })
          setShowInvoiceSectionModal(null)
        }}
        setForm={setInvoiceReturnForm}
        handleSubmit={(e: any) => {
          e.preventDefault()
          setShowInvoiceSectionModal(null)
          const subtotal_return = form.subtotal_return + invoiceReturnForm.total_price
          setForm({
            ...form,
            subtotal_return,
            invoice_returns: [...form.invoice_returns, invoiceReturnForm]
          })
          setInvoiceReturnForm({
            product_id: null as any,
            qty: 1,
            total_price: 0,
            note: '',
            product: {}
          })
        }}
      />

      <InvoiceMergeForm
        show={showInvoiceSectionModal === 'invoice-merge'}
        form={invoiceMergeForm}
        invoices={invoices}
        onClose={() => {
          setInvoiceMergeForm({
            from_invoice_id: null as any,
            description: '',
            from_invoice: {},
            remaining_amount: 0
          })
          setShowInvoiceSectionModal(null)
        }}
        setForm={setInvoiceMergeForm}
        handleSubmit={(e: any) => {
          e.preventDefault()
          setShowInvoiceSectionModal(null)
          const subtotal_merges = form.subtotal_merges + invoiceMergeForm.remaining_amount

          setForm({
            ...form,
            subtotal_merges,
            invoice_merges: [...form.invoice_merges, invoiceMergeForm]
          })
          setInvoiceMergeForm({
            from_invoice_id: null as any,
            description: '',
            from_invoice: {},
            remaining_amount: 0
          })
        }}
      />

      <InvoiceDetailForm
        show={showInvoiceSectionModal === 'invoice-detail'}
        form={invoiceDetailForm}
        products={products}
        onClose={() => {
          setInvoiceDetailForm({
            product_id: null as any,
            qty: 1,
            total_price: 0,
            product: {}
          })
          setShowInvoiceSectionModal(null)
        }}
        setForm={setInvoiceDetailForm}
        handleSubmit={(e: any) => {
          e.preventDefault()
          setShowInvoiceSectionModal(null)
          const subtotal_product = form.subtotal_product + invoiceDetailForm.total_price
          setForm({
            ...form,
            subtotal_product,
            invoice_details: [...form.invoice_details, invoiceDetailForm]
          })
          setInvoiceDetailForm({
            product_id: null as any,
            qty: 1,
            total_price: 0,
            product: {}
          })
        }}
      />

      <form style={{ marginBottom: '20px' }} onSubmit={handleSubmit}>
        <Card>
          <CardHeader title={title} />

          <CardContent>
            <Grid container spacing={6}>
              <Grid item xs={6}>
                <TextField label='No Nota' fullWidth variant='filled' value={form.invoice_number} disabled />
              </Grid>
              <Grid item xs={6}>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DatePicker
                    label='Tanggal Invoice'
                    value={form.invoice_date}
                    onChange={newValue =>
                      setForm({
                        ...form,
                        invoice_date: dayjs(newValue)
                      })
                    }
                  />
                </LocalizationProvider>
              </Grid>

              <Grid item xs={12}>
                <Autocomplete
                  options={partners}
                  value={form.profile}
                  disabled={disabledPartner}
                  getOptionLabel={(option: any) => {
                    return option.fullname
                  }}
                  onChange={async (_, newValue: any) => {
                    startLoading()
                    if (newValue && newValue.id) {
                      const { count } = await supabase
                        .from('invoices')
                        .select('id', { count: 'exact' })
                        .eq('profile_id', newValue.id)

                      const invoice_number = new Intl.NumberFormat(`en-US`, { minimumIntegerDigits: 3 }).format(
                        Number(count) + 1
                      )

                      setForm({
                        ...form,
                        profile_id: newValue.id,
                        profile: newValue,
                        invoice_number
                      })
                    } else {
                      setForm({
                        ...form,
                        invoice_number: '',
                        profile_id: null as any,
                        profile: null
                      })
                    }

                    stopLoading()
                  }}
                  renderInput={params => (
                    <TextField
                      {...params}
                      label='Pilih Mitra'
                      variant='filled'

                    // error={findIsError('category_id', errors)}
                    // helperText={findErrorProperty('category_id', errors)?.errors.join(', ')}
                    />
                  )}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>

        <InvoiceFormSection
          show={Boolean(form.profile_id)}
          buttonTitle='Tambah Produk'
          subTotal={form.subtotal_product}
          isShowSubtotal={form.subtotal_product > 0}
          handleAdd={() => setShowInvoiceSectionModal('invoice-detail')}
        >
          {form.invoice_details
            .filter(d => !d?.isDeleted)
            .map((detail, index) => {
              return (
                <InvoiceFormSectionItem
                  isShowDelete={profile?.roles.name === 'Superadmin' || !isNotEmpty(detail?.id)}
                  handleDelete={() => {
                    const arr = form.invoice_details
                    arr.splice(index, 1)

                    const subtotal_product = form.subtotal_product - detail.total_price
                    if (isNotEmpty(detail?.id)) {
                      setForm({
                        ...form,
                        invoice_details: [...arr, { ...detail, isDeleted: true }],
                        subtotal_product
                      })

                      return
                    }

                    setForm({
                      ...form,
                      invoice_details: arr,
                      subtotal_product
                    })
                  }}
                >
                  <Typography variant='body1'>
                    {detail.product.name} | {detail.product.sku}
                  </Typography>
                  <Typography variant='subtitle1'>X {detail.qty}</Typography>
                  <Typography variant='body2'>
                    {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(detail.total_price)}
                  </Typography>
                </InvoiceFormSectionItem>
              )
            })}
        </InvoiceFormSection>

        <InvoiceFormSection
          show={Boolean(form.profile_id)}
          isShowSubtotal={form.subtotal_merges > 0}
          buttonTitle='Tambah Penggabungan Nota'
          subTotal={form.subtotal_merges}
          handleAdd={() => setShowInvoiceSectionModal('invoice-merge')}
        >
          {form.invoice_merges
            .filter(d => !d?.isDeleted)
            .map((merge, index) => (
              <InvoiceFormSectionItem
                isShowDelete={profile?.roles.name === 'Superadmin' || !isNotEmpty(merge?.id)}
                handleDelete={() => {
                  const arr = form.invoice_merges
                  arr.splice(index, 1)
                  const subtotal_merges = form.subtotal_merges - merge.remaining_amount
                  if (isNotEmpty(merge?.id)) {
                    setForm({
                      ...form,
                      invoice_merges: [...arr, { ...merge, isDeleted: true }],
                      subtotal_merges
                    })

                    return
                  }

                  setForm({
                    ...form,
                    invoice_merges: arr,
                    subtotal_merges
                  })
                }}
              >
                <Typography variant='body1'>Nota {merge.from_invoice.invoice_number}</Typography>
                <Typography variant='subtitle1'>{merge.from_invoice.invoice_date}</Typography>
                <Typography variant='body2'>
                  {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(
                    merge.remaining_amount
                  )}
                </Typography>
              </InvoiceFormSectionItem>
            ))}
        </InvoiceFormSection>

        <InvoiceFormSection
          show={Boolean(form.profile_id)}
          isShowSubtotal={form.subtotal_return > 0}
          buttonTitle='Tambah Retur Produk'
          subTotal={form.subtotal_return}
          handleAdd={() => setShowInvoiceSectionModal('invoice-returns')}
        >
          {form.invoice_returns
            .filter(d => !d?.isDeleted)
            .map((returns, index) => (
              <InvoiceFormSectionItem
                isShowDelete={profile?.roles.name === 'Superadmin' || !isNotEmpty(returns?.id)}
                handleDelete={() => {
                  const arr = form.invoice_returns
                  arr.splice(index, 1)
                  const subtotal_return = form.subtotal_return - returns.total_price
                  if (isNotEmpty(returns?.id)) {
                    setForm({
                      ...form,
                      invoice_returns: [...arr, { ...returns, isDeleted: true }],
                      subtotal_return
                    })

                    return
                  }

                  setForm({
                    ...form,
                    invoice_returns: arr,
                    subtotal_return
                  })
                }}
              >
                <Typography variant='body1'>
                  {returns.product.name} | {returns.product.sku}
                </Typography>
                <Typography variant='subtitle1'>X {returns.qty}</Typography>
                <Typography variant='body2'>
                  {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(returns.total_price)}
                </Typography>
              </InvoiceFormSectionItem>
            ))}
        </InvoiceFormSection>

        <InvoiceFormSection
          show={Boolean(form.profile_id)}
          buttonTitle='Tambah Pembayaran'
          handleAdd={() => setShowInvoiceSectionModal('invoice-payment')}
        >
          {form.invoice_payments
            .filter(d => !d?.isDeleted)
            .map((payment, index) => (
              <InvoiceFormSectionItem
                isShowDelete={profile?.roles.name === 'Superadmin' || !isNotEmpty(payment?.id)}
                handleDelete={() => {
                  const arr = form.invoice_payments
                  arr.splice(index, 1)
                  const payment_total = form.payment_total - payment.amount
                  if (isNotEmpty(payment?.id)) {
                    setForm({
                      ...form,
                      invoice_payments: [...arr, { ...payment, isDeleted: true }],
                      payment_total
                    })

                    return
                  }

                  setForm({
                    ...form,
                    payment_total,
                    invoice_payments: arr
                  })
                }}
              >
                <Typography variant='body1'>
                  {payment.payment_method} {payment.bank_account_name}
                </Typography>
                <Typography variant='subtitle1'>{dayjs(payment.paid_at).format('MM/DD/YYYY')}</Typography>
                <Typography variant='body2'>
                  {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(payment.amount)}
                </Typography>
              </InvoiceFormSectionItem>
            ))}
        </InvoiceFormSection>

        <Card sx={{ marginY: '10px' }}>
          <CardContent>
            <Grid container spacing={6}>
              <Grid item xs={12}>
                <Typography variant='body1'>Subtotal Produk:</Typography>
                <Typography variant='body2'>
                  {new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                  }).format(form.subtotal_product)}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant='body1'>Subtotal Penggabungan Nota:</Typography>
                <Typography variant='body2'>
                  {new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                  }).format(form.subtotal_merges)}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant='body1'>Subtotal Retur Produk:</Typography>
                <Typography variant='body2'>
                  {new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                  }).format(form.subtotal_return)}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant='body1'>Total Sudah Dibayar:</Typography>
                <Typography variant='body2'>
                  {new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                  }).format(form.payment_total)}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Typography variant='body1'>Total Keseluruhan:</Typography>
                <Typography variant='body2'>
                  {new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR'
                  }).format(form.subtotal_product + form.subtotal_merges - form.subtotal_return - form.payment_total)}
                </Typography>
              </Grid>
            </Grid>
          </CardContent>

          <CardActions>
            <Box sx={{ display: 'flex', justifyContent: 'end', width: '100%' }}>
              <Button variant='text' color='primary' sx={{ marginRight: '5px' }} onClick={() => router.back()}>
                Kembali
              </Button>

              <Button variant='contained' color='primary' type='submit'>
                Simpan
              </Button>
            </Box>
          </CardActions>
        </Card>
      </form>
    </>
  )
}

export default InvoiceForm
