import {
  Grid,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
  Box,
  Button,
  DialogContent,
  DialogActions
} from '@mui/material'
import { useState, useEffect, useCallback } from 'react'
import { useFormValidation } from 'src/@core/hooks/useForm'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { findIsError, findErrorProperty } from 'src/@core/utils/validateSync'
import { PartnerFormSchema } from 'src/pages/partners/new'

interface Props {
  form: any
  roles: any[]
  handleSubmit: (form: any) => Promise<void> | Promise<any> | void
  onClose: () => void | any
}

const AdminUserForm = (props: Props) => {
  const { form: formProps, roles, handleSubmit } = props
  const [form, setForm] = useState<PartnerFormSchema>(formProps)
  const handleChange = useHandleChange(setForm, form)
  const { runValidation, errors } = useFormValidation()

  const onSubmit = useCallback(
    async (e: any) => {
      e.preventDefault()

      console.log(form)
      handleSubmit(form)
    },
    [form]
  )

  useEffect(() => {
    setForm({
      ...props.form
    })
  }, [props])

  return (
    <>
      <form onSubmit={onSubmit}>
        <DialogContent>
          <Grid container spacing={6}>
            <Grid item xs={12}>
              <TextField
                label='Nama Lengkap *'
                fullWidth
                value={form.fullname}
                onChange={handleChange('fullname')}
                variant='filled'
                error={findIsError('fullname', errors)}
                helperText={findErrorProperty('fullname', errors)?.errors.join(', ')}
              />
            </Grid>

            <Grid item xs={6}>
              <TextField
                label='Username *'
                variant='filled'
                fullWidth
                onChange={handleChange('username')}
                value={form.username}
                error={findIsError('username', errors)}
                helperText={findErrorProperty('username', errors)?.errors.join(', ')}
              />
            </Grid>

            <Grid item xs={6}>
              <TextField
                label='No HP *'
                variant='filled'
                value={form.phone}
                onChange={handleChange('phone')}
                fullWidth
                error={findIsError('phone', errors)}
                helperText={findErrorProperty('phone', errors)?.errors.join(', ')}
              />
            </Grid>

            <Grid item xs={6}>
              <FormControl fullWidth>
                <InputLabel>Tipe Mitra *</InputLabel>
                <Select
                  labelId='demo-simple-select-label'
                  id='demo-simple-select'
                  fullWidth
                  label='Tipe Mitra *'
                  variant='filled'
                  value={form.role_id}
                  onChange={handleChange('role_id')}
                  error={findIsError('role_id', errors)}
                >
                  {roles.map(role => (
                    <MenuItem key={role.id} value={role.id}>
                      {role.name}
                    </MenuItem>
                  ))}
                </Select>
                {findIsError('role_id', errors) ? (
                  <FormHelperText error>{findErrorProperty('role_id', errors)?.errors.join(', ')}</FormHelperText>
                ) : (
                  <></>
                )}
              </FormControl>
            </Grid>

            <Grid item xs={6}>
              <TextField
                label='Password *'
                fullWidth
                variant='filled'
                type='password'
                value={form.password}
                onChange={handleChange('password')}
                error={findIsError('password', errors)}
                helperText={findErrorProperty('password', errors)?.errors.join(', ')}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                fullWidth
                label='Alamat'
                variant='filled'
                multiline
                value={form.address}
                onChange={handleChange('address')}
                rows={4}
                error={findIsError('address', errors)}
                helperText={findErrorProperty('address', errors)?.errors.join(', ')}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                fullWidth
                label='Deskripsi'
                variant='filled'
                multiline
                value={form.description}
                onChange={handleChange('description')}
                rows={4}
                error={findIsError('description', errors)}
                helperText={findErrorProperty('description', errors)?.errors.join(', ')}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Box
            sx={{
              display: 'flex'
            }}
          >
            <Button color='secondary' variant='text' onClick={props.onClose}>
              Batal
            </Button>
            <Button type='submit'>Simpan</Button>
          </Box>
        </DialogActions>
      </form>
    </>
  )
}

export default AdminUserForm
