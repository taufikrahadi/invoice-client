import { Table, TableRow, TableBody, TableHead, TableCell, TableContainer, IconButton } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useState, useCallback, useEffect } from 'react'
import { Pencil, TrashCan } from 'mdi-material-ui'
import ConfirmDialog from 'src/views/ConfirmDialog'
import FormModal from 'src/views/users/FormModal'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import RoleAndCategoryForm from 'src/views/forms/RoleAndCategoryForm'

const TipeMitraTable = (props: { handleEditButton: any; refetch?: boolean }) => {
  const { refetch } = props
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const supabase = useSupabaseClient()
  const [roles, setRoles] = useState([])
  const [showConfirmDelete, setShowConfirmDelete] = useState(false)
  const [selectedId, setSelectedId] = useState(null)
  const [showModalForm, setShowModalForm] = useState(false)
  const [form, setForm] = useState<any>({
    id: null,
    name: ''
  })

  const fetchRoles = useCallback(async () => {
    const { data } = await supabase.from('roles').select('*').eq('type', 'Partner').is('deleted_at', null)
    setRoles(data as any)

    return
  }, [setRoles, supabase])

  const deleteRole = useCallback(async () => {
    startLoading()
    await supabase.from('roles').update({ deleted_at: new Date() }).eq('id', selectedId)

    await fetchRoles()
    setSelectedId(null)
    setShowConfirmDelete(false)
    stopLoading()
  }, [supabase, startLoading, stopLoading, selectedId])

  useEffect(() => {
    fetchRoles().then(() => {
      stopLoading()
    })
  }, [])

  useEffect(() => {
    fetchRoles().then(() => {
      stopLoading()
    })
  }, [refetch])

  return (
    <TableContainer>
      <ConfirmDialog
        title='Yakin hapus tipe mitra?'
        message='Data yang sudah dihapus tidak bisa dikembalikan lagi'
        action={deleteRole as any}
        setShow={setShowConfirmDelete}
        show={showConfirmDelete}
      />

      <Table>
        <TableHead>
          <TableRow>
            <TableCell>NO</TableCell>
            <TableCell>Nama</TableCell>
            <TableCell>Aksi</TableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {roles.map((role: any, index: number) => (
            <TableRow key={role.id}>
              <TableCell>{index + 1}</TableCell>
              <TableCell>{role.name}</TableCell>
              <TableCell>
                <IconButton color='warning' onClick={() => props.handleEditButton(role)}>
                  <Pencil />
                </IconButton>
                <IconButton
                  color='error'
                  onClick={() => {
                    setShowConfirmDelete(true)
                    setSelectedId(role.id)
                  }}
                >
                  <TrashCan />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default TipeMitraTable
