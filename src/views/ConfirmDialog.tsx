import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material'

export interface ConfirmDialogProps {
  show: boolean
  setShow(value: boolean): void
  title: string
  message: string
  action(): void | any
}

const ConfirmDialog = (props: ConfirmDialogProps) => {
  return (
    <Dialog
      open={props.show}
      onClose={() => props.setShow(false)}
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
    >
      <DialogTitle id='alert-dialog-title'>{props.title}</DialogTitle>
      <DialogContent>
        <DialogContentText id='alert-dialog-description'>{props.message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => props.setShow(false)}>Batal</Button>
        <Button onClick={() => props.action()} autoFocus>
          Lanjutkan
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ConfirmDialog
