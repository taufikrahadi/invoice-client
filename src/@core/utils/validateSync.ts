import { validateSync as validate } from 'class-validator'
import { plainToInstance } from 'class-transformer'

export type FormValidationError = {
  property: string
  errors: string[]
}

export const findErrorProperty = (name: string, errors: FormValidationError[]) => {
  return errors.find(error => error.property === name)
}

export const findIsError = (name: string, errors: FormValidationError[]) => {
  return Boolean(findErrorProperty(name, errors))
}

export const toSchema = (something: any, schema: any) => {
  const result = plainToInstance(schema, {
    ...something
  })

  console.log(result)

  return result
}

export const validateSync = (form: any) => {
  const validating = validate(form)

  if (validating.length === 0) return null

  const errors: FormValidationError[] = validating.map(error => {
    const { property, constraints } = error

    const keys = Object.keys(constraints as any)
    const messages: string[] = []

    keys.forEach(key => {
      messages.push(`${constraints?.[key]}`)
    })

    return {
      property,
      errors: messages
    }
  })

  return errors
}
