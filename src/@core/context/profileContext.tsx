import { createContext, ReactNode } from 'react'
import { useProfile } from '../hooks/useProfile'

export const ProfileContext = createContext(null)

export const ProfileProvider = ({ children }: { children: ReactNode }) => {
  const profile = useProfile()

  return <ProfileContext.Provider value={profile}>{children}</ProfileContext.Provider>
}
