import { createContext, ReactNode, useState } from 'react'
import nProgress from 'nprogress'

interface LoadingContext {
  loading: boolean
  loadingText: string
  setLoadingText: (value: string) => void
  startLoading: () => void
  stopLoading: () => void
}

export const LoadingContext = createContext<LoadingContext | undefined>(undefined)

export const LoadingProvider = ({ children }: { children: ReactNode }) => {
  const [loading, setLoading] = useState(false)
  const [loadingText, setLoadingText] = useState('')
  const startLoading = () => {
    setLoading(true)
    nProgress.start()
  }

  const stopLoading = () => {
    setLoading(false)
    nProgress.done()
    setLoadingText('')
  }

  return (
    <LoadingContext.Provider value={{ loading, loadingText, startLoading, stopLoading, setLoadingText }}>
      {children}
    </LoadingContext.Provider>
  )
}
