import { useState } from 'react'
import { FormValidationError, validateSync } from '../utils/validateSync'

export const useFormValidation = () => {
  const [errors, setErrors] = useState<FormValidationError[]>([])

  const runValidation = (mappedForm: any) => {
    const e = validateSync(mappedForm)

    setErrors(e ?? [])
  }

  return {
    errors,
    runValidation
  }
}
