import { useMemo } from 'react'
import { SupabaseClient } from '@supabase/supabase-js'

export const useSupabaseAdmin = () =>
  useMemo(
    () =>
      new SupabaseClient(
        String(process.env.NEXT_PUBLIC_SUPABASE_URL),
        String(process.env.NEXT_PUBLIC_SUPABASE_ROLE_KEY)
      ),
    []
  )
