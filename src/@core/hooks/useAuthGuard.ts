import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useRouter } from 'next/router'
import { useEffect } from 'react'

export const useAuthGuard = () => {
  const supabase = useSupabaseClient()
  const router = useRouter()

  useEffect(() => {
    ;(async function () {
      const {
        data: { session }
      } = await supabase.auth.getSession()

      if (!session) router.push('/pages/login')

      return session
    })()
  }, [])
}
