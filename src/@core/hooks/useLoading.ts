import { useContext, useState } from 'react'
import nProgress from 'nprogress'
import { LoadingContext } from '../context/loadingContext'

export const useLoadingUsingContext = () => {
  const context = useContext(LoadingContext)
  if (context === undefined) {
    throw new Error('no provider registered')
  }

  return context
}

export const useLoading = (defaultValue = false) => {
  const [loading, setLoading] = useState(defaultValue)

  const startLoading = () => {
    setLoading(true)
    nProgress.start()
  }

  const stopLoading = () => {
    setLoading(false)
    nProgress.done()
  }

  return {
    loading,
    startLoading,
    stopLoading
  } as const
}
