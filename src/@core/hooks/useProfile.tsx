import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useAuthGuard } from './useAuthGuard'
import { useState, useCallback, useEffect, useContext } from 'react'
import { useLoadingUsingContext } from './useLoading'
import { ProfileContext } from '../context/profileContext'

export const useProfile = () => {
  useAuthGuard()
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const supabase = useSupabaseClient()

  const [profile, setProfiles] = useState<any>(null)

  const fetchprofile = useCallback(async () => {
    startLoading()
    const {
      data: { user }
    } = await supabase.auth.getUser()
    const { data } = await supabase
      .from('profiles')
      .select('*, roles(id, name), companies(*)')
      .eq('uid', user?.id)
      .single()

    setProfiles(data)
    stopLoading()
  }, [supabase, setProfiles])

  useEffect(() => {
    fetchprofile()
  }, [])

  return profile
}

export const useProfileUsingContext = (): any => {
  const profile = useContext(ProfileContext)

  return profile
}
