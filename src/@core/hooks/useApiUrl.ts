import { useSupabaseClient } from "@supabase/auth-helpers-react"
import { useCallback, useEffect, useState } from "react"

export const useApiUrl = () => {
    const supabase = useSupabaseClient()
    const [apiUrl, setApiUrl] = useState('')

    const generalConfigApiUrl = useCallback(async () => {
        const { data } = await supabase
            .from('general_configs')
            .select('value')
            .eq('key', 'api_url')
            .single() 

        setApiUrl(data?.value)
    }, [setApiUrl])

    useEffect(() => {
        generalConfigApiUrl()
    })

    return apiUrl
}