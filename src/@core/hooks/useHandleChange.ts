import { Dispatch } from 'react'

export const useHandleChange =
  <T>(setForm: Dispatch<any>, form: T) =>
  (prop: keyof T, value?: any) =>
  (e: any) => {
    setForm({
      ...form,
      [prop]: value ?? e.target.value
    })
  }
