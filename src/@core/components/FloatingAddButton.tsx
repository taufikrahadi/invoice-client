// ** React Imports
import { ReactNode } from 'react'

// ** MUI Imports
import Zoom from '@mui/material/Zoom'
import Fab from '@mui/material/Fab'
import { styled } from '@mui/material/styles'
import useScrollTrigger from '@mui/material/useScrollTrigger'
import { Plus } from 'mdi-material-ui'

interface ScrollToTopProps {
  className?: string
  handleClick?: () => void
}

const ScrollToTopStyled = styled('div')(({ theme }) => ({
  zIndex: 11,
  position: 'fixed',
  right: theme.spacing(6),
  bottom: theme.spacing(20)
}))

const FloatingAddButton = (props: ScrollToTopProps) => {
  // ** Props
  const { className, handleClick } = props

  return (
    <ScrollToTopStyled className={className} role='presentation'>
      <Fab onClick={handleClick} color='primary' size='small' aria-label='add data of this resource'>
        <Plus />
      </Fab>
    </ScrollToTopStyled>
  )
}

export default FloatingAddButton
