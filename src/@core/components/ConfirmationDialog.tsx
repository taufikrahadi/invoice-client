import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControlLabel, Radio, RadioGroup } from '@mui/material'
import { options } from 'pdfkit'
import React from 'react'

interface Props {
    title: string
    action: () => void | any
    children: (showDialog: () => void) => React.ReactNode
    description?: string
}

function ConfirmationDialog(props: Props) {
    const [open, setOpen] = React.useState(false)
    const showDialog = () => {
        setOpen(true)
    }

    const hideDialog = () => {
        setOpen(false)
    }

    const confirmed = () => {
        props.action()
        hideDialog()
    }

    return (
        <>
            {props.children(showDialog)}
            <Dialog
                sx={{ '& .MuiDialog-paper': { width: '80%', maxHeight: 435 } }}
                maxWidth="xs"
                open={open}
            >
                <DialogTitle>{props.title}</DialogTitle>
                <DialogContent dividers>
                    <DialogContentText>
                        {props.description}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={hideDialog}>
                        Cancel
                    </Button>
                    <Button color='primary' onClick={confirmed}>Ok</Button>
                </DialogActions>
            </Dialog>
        </>
    )
}

export default ConfirmationDialog