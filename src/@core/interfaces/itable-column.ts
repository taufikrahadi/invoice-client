export interface IListItem {
  key: string
  title: string
  formatter?(value: any): any
  align: 'left' | 'center' | 'right' | 'inherit' | 'justify'
}
