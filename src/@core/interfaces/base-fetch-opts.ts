export interface BaseFetchOptions {
  rangeStart: number
  rangeEnd: number
  search?: string
  searchInColumns?: string[]
  filters?: FetchOptionsFilters[]
  sort?: {
    [k: string]: 'ASC' | 'DESC'
  }
}

export interface FetchOptionsFilters {
  columnName: string
  value: any
  operator: 'eq' | 'in' | 'is' | 'gt' | 'gte' | 'neq' | 'rangeGte'
}
