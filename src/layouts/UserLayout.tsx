// ** React Imports
import { ReactNode, useRef, useState, useEffect, useCallback } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import { Theme } from '@mui/material/styles'
import useMediaQuery from '@mui/material/useMediaQuery'

// ** Layout Imports
// !Do not remove this Layout import
import VerticalLayout from 'src/@core/layouts/VerticalLayout'

// ** Navigation Imports
import VerticalNavItems from 'src/navigation/vertical'

// ** Component Import
import UpgradeToProButton from './components/UpgradeToProButton'
import VerticalAppBarContent from './components/vertical/AppBarContent'
import Head from 'next/head'

import ArchiveIcon from 'mdi-material-ui/Archive'
import HomeOutline from 'mdi-material-ui/HomeOutline'
import { CartArrowUp, AccountCogOutline, Package } from 'mdi-material-ui'

import themeConfig from 'src/configs/themeConfig'

// ** Hook Import
import { useSettings } from 'src/@core/hooks/useSettings'
import BottomNavigation from '@mui/material/BottomNavigation'
import BottomNavigationAction from '@mui/material/BottomNavigationAction'
import Paper from '@mui/material/Paper'

import { Router, useRouter } from 'next/router'
import Link from 'next/link'
import { handleURLQueries } from 'src/@core/layouts/utils'
import { useAuthGuard } from 'src/@core/hooks/useAuthGuard'
import NextComposedLink from 'src/@core/components/NextLinkComposed'
import { useProfileUsingContext as useProfile, useProfileUsingContext } from 'src/@core/hooks/useProfile'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'

interface Props {
  children: ReactNode
}

interface Menu {
  label: string
  icon: any
  roles: string[]
  to: string
}

const menus: Menu[] = [
  {
    label: 'Home',
    icon: <HomeOutline />,
    roles: ['Superadmin'],
    to: '/dashboard'
  },
  {
    label: 'Penjualan',
    icon: <CartArrowUp />,
    roles: [],
    to: '/invoices'
  },
  {
    label: 'Mitra',
    icon: <AccountCogOutline />,
    roles: ['Superadmin'],
    to: '/partners'
  },
  {
    label: 'Produk',
    icon: <Package />,
    roles: ['Superadmin'],
    to: '/products'
  }
]

const menuByRoleName = (currentRole: string) => {
  return menus.filter(menu => menu.roles.length === 0 || menu.roles.includes(currentRole))
}

const UserLayout = ({ children }: Props) => {
  // ** Hooks
  const { settings, saveSettings } = useSettings()
  useAuthGuard()

  const { startLoading, stopLoading } = useLoadingUsingContext()

  // ** Pace Loader
  if (themeConfig.routingLoader) {
    Router.events.on('routeChangeStart', () => {
      startLoading()
    })
    Router.events.on('routeChangeError', () => {
      stopLoading()
    })
    Router.events.on('routeChangeComplete', () => {
      stopLoading()
    })
  }

  /**
   *  The below variable will hide the current layout menu at given screen size.
   *  The menu will be accessible from the Hamburger icon only (Vertical Overlay Menu).
   *  You can change the screen size from which you want to hide the current layout menu.
   *  Please refer useMediaQuery() hook: https://mui.com/components/use-media-query/,
   *  to know more about what values can be passed to this hook.
   *  ! Do not change this value unless you know what you are doing. It can break the template.
   */
  const hidden = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'))
  const ref = useRef<HTMLDivElement>(null)

  const router = useRouter()
  const pathname = router.pathname
  const profile = useProfileUsingContext()
  const [value, setValue] = useState(pathname)

  const isNavLinkActive = (path: string) => {
    if (router.pathname === path || handleURLQueries(router, path)) {
      return true
    } else {
      return false
    }
  }

  const checkIsAllowedToShow = useCallback(
    (menuName: string) => {
      if (profile?.roles) {
        const { roles } = menus.find((m: Menu) => m.label === menuName) as any

        return roles.includes(profile?.roles.name)
      }
    },
    [profile]
  )

  useEffect(() => {
    ; (ref.current as HTMLDivElement).ownerDocument.body.scrollTop = 0
  }, [value])

  return (
    <>
      <Head>
        <title>Aplikasi Nota Pembayaran</title>
      </Head>
      <VerticalLayout
        hidden={hidden}
        settings={settings}
        saveSettings={saveSettings}
        verticalNavItems={VerticalNavItems(profile?.roles.name)} // Navigation Items
        verticalAppBarContent={(
          props // AppBar Content
        ) => (
          <VerticalAppBarContent
            hidden={hidden}
            settings={settings}
            saveSettings={saveSettings}
            toggleNavVisibility={props.toggleNavVisibility}
          />
        )}
      >
        {children}
        <Box sx={{ pb: 7 }} ref={ref}>
          <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0, zIndex: 99999 }} elevation={24}>
            {menuByRoleName(profile?.roles.name).length > 1 ? (
              <BottomNavigation
                showLabels
                value={router.pathname}
                onChange={(_, newValue) => {
                  setValue(newValue)
                }}
              >
                {menuByRoleName(profile?.roles.name).map(menu => {
                  if (menu.roles.length === 0 || checkIsAllowedToShow(menu.label)) {
                    return (
                      <BottomNavigationAction
                        key={menu.to}
                        label={menu.label}
                        icon={menu.icon}
                        sx={{ opacity: 1 }}
                        component={NextComposedLink}
                        value={menu.to}
                        href={menu.to}
                      />
                    )
                  }
                })}
              </BottomNavigation>
            ) : (
              <></>
            )}
          </Paper>
        </Box>
      </VerticalLayout>
    </>
  )
}

export default UserLayout
