import { Typography } from '@mui/material'

const TotalItemText = (props: { text: string }) => {
  return (
    <>
      <Typography variant='body2' color='secondary'>
        {props.text}
      </Typography>
    </>
  )
}

export default TotalItemText
