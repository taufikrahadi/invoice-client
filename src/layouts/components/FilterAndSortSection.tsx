import { InputAdornment, Box, TextField, Grid, Button } from '@mui/material'
import { TextSearch } from 'mdi-material-ui'

export type VoidFunction = (e?: any) => void

export interface FilterAndSortSectionProps {
  searchTerm?: string
  handleSearchChange?: VoidFunction
  toggleModalFilterOpen?: VoidFunction
  toggleModalSortOpen?: VoidFunction
}

const FilterAndSortSection = (props: FilterAndSortSectionProps) => {
  const { searchTerm, handleSearchChange, toggleModalFilterOpen, toggleModalSortOpen } = props

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: '10px'
        }}
      >
        <TextField
          variant='outlined'
          fullWidth
          placeholder='Cari...'
          value={searchTerm}
          onChange={handleSearchChange}
          InputProps={{
            startAdornment: (
              <InputAdornment position='start'>
                <TextSearch />
              </InputAdornment>
            )
          }}
        />
      </Box>

      <Grid item xs={12} container sx={{ marginY: '5px' }}>
        <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
          <Button variant='outlined' onClick={toggleModalFilterOpen}>
            Filter
          </Button>

          <Button variant='outlined' onClick={toggleModalSortOpen}>
            Urutkan
          </Button>
        </Box>
      </Grid>
    </>
  )
}

export default FilterAndSortSection
