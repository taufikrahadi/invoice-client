import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useState, useCallback, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useLoading } from 'src/@core/hooks/useLoading'
import Loading from 'src/views/Loading'

const InvoicePdf = () => {
  const supabase = useSupabaseClient()
  const [invoice, setInvoice] = useState<any>(null)
  const [invoiceMerges, setInvoiceMerges] = useState<any[]>([])
  const { loading, startLoading, stopLoading } = useLoading(true)
  const router = useRouter()
  const { invoiceId } = router.query
  const [showConfirmDelete, setShowConfirmDelete] = useState(false)

  const fetchInvoice = useCallback(async () => {
    const { data } = await supabase
      .from('invoices')
      .select(
        '*, profiles(id, fullname, code), invoice_statuses(id,name), invoice_payments(*), invoice_details(*, products(id,name,sku)), invoice_returns(*, products(id,name,sku))'
      )
      .eq('id', invoiceId)
      .single()

    setInvoice(data)
  }, [supabase, setInvoice, invoiceId])

  const fetchInvoiceMerges = useCallback(async () => {
    const { data } = await supabase
      .from('invoice_merges')
      .select('*, invoices:from_invoice_id(id, code, invoice_number)')
      .eq('to_invoice_id', invoiceId)

    setInvoiceMerges(data as any)
  }, [supabase, setInvoice, invoice, invoiceId])

  useEffect(() => {
    fetchInvoice()
      .then(() => {
        fetchInvoiceMerges()
      })
      .finally(() => {
        stopLoading()
      })
  }, [])

  return (
    <>
      <Loading show={loading} />
    </>
  )
}

export default InvoicePdf
