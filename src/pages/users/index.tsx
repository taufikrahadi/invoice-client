import { Grid, Card, CardHeader, CardContent, Button } from '@mui/material'
import ListUserTable from 'src/views/users/ListUserTable'
import { useCallback, useEffect, useState } from 'react'
import { useSupabaseClient } from '@supabase/auth-helpers-react'

import FormModal from 'src/views/users/FormModal'
import AdminUserForm from 'src/views/forms/AdminUserForm'
import { PartnerFormSchema } from 'src/pages/partners/new'
import { useSupabaseAdmin } from 'src/@core/hooks/useSupabaseAdmin'
import { isNotEmpty } from 'class-validator'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'

const ListAdminUser = () => {
  const [users, setUsers] = useState([])
  const supabase = useSupabaseClient()
  const supabaseAdmin = useSupabaseAdmin()
  const { startLoading, stopLoading } = useLoadingUsingContext()

  const [showFormModal, setShowFormModal] = useState(false)
  const [selectedId, setSelectedId] = useState<string | null>(null)
  const [formModalTitle, setFormModalTitle] = useState<'create' | 'edit' | null>(null)
  const [roles, setRoles] = useState<any>([])
  const [form, setForm] = useState<PartnerFormSchema>({
    fullname: '',
    phone: '',
    code: '',
    role_id: '',
    uid: '',
    address: '',
    description: '',
    username: '',
    password: '',
    raw_password: ''
  })

  const generateCode = useCallback(() => {
    const prefix = `MITRA-`

    const code = `${prefix}-${Date.now()}`

    setForm({
      ...form,
      code
    })

    return code
  }, [setForm])

  const closeModal = () => {
    setShowFormModal(false)
    setSelectedId(null)
    setFormModalTitle(null)
    setForm({
      fullname: '',
      phone: '',
      code: '',
      role_id: '',
      uid: '',
      address: '',
      description: '',
      username: '',
      password: '',
      raw_password: ''
    })
  }

  const fetchAdminUser = useCallback(async () => {
    const { data } = await supabase
      .from('profiles')
      .select('*,roles(*)')
      .in(
        'role_id',
        (roles as any[]).map(role => role.id)
      )
      .is('deleted_at', null)

    setUsers(data as any)
    Promise.resolve(users)
  }, [supabase, users, setUsers, roles])

  const fetchRoles = useCallback(async () => {
    const { data: roles } = await supabase.from('roles').select('*').eq('type', 'Organizer').is('deleted_at', null)

    setRoles(roles as any)
    Promise.resolve(roles)
  }, [supabase])

  const createUser = useCallback(
    async (formData: any) => {
      const { data, error } = await supabaseAdmin.auth.admin.createUser({
        email: `${formData.username}@invoice-app.com`,
        password: formData.password,
        email_confirm: true
      })

      if (error) {
        Promise.reject(error)

        return
      }

      Promise.resolve(data.user)

      return data.user
    },
    [supabaseAdmin]
  )

  const createAdminUser = useCallback(
    async (formData: any) => {
      startLoading()

      const userData = await createUser(formData)
      await supabase.from('profiles').insert({
        uid: (userData as any)?.id,
        fullname: formData.fullname,
        role_id: formData.role_id,
        phone: formData.phone,
        code: formData.code,
        address: formData.address,
        description: formData.description,
        raw_password: formData.password
      })

      await fetchAdminUser()
      stopLoading()
      closeModal()
    },
    [supabase, startLoading, stopLoading, createUser, closeModal, fetchAdminUser]
  )

  const deleteAdminUser = useCallback(
    async (id: string) => {
      startLoading()
      const { data } = await supabase.from('profiles').select('uid').eq('id', id).single()

      await supabase.from('profiles').delete().eq('id', id)
      await supabaseAdmin.auth.admin.deleteUser((data as any).uid)

      fetchAdminUser()
      stopLoading()

      return
    },
    [supabase, fetchAdminUser, startLoading, stopLoading]
  )

  const updateAdminUser = useCallback(
    async (sentForm: any) => {
      startLoading()

      if (form.username !== sentForm.username || isNotEmpty(sentForm.password)) {
        await supabaseAdmin.auth.admin.updateUserById(form.uid, {
          email: `${sentForm.username}@invoice-app.com`,
          password: sentForm.password,
          email_confirm: true
        })
      }

      await supabase
        .from('profiles')
        .update({
          fullname: sentForm.fullname,
          role_id: sentForm.role_id,
          phone: sentForm.phone,
          code: sentForm.code,
          address: sentForm.address,
          description: sentForm.description,
          raw_password: sentForm.raw_password
        })
        .eq('id', selectedId)

      await fetchAdminUser()
      closeModal()
      stopLoading()
    },
    [supabase, fetchAdminUser, startLoading, stopLoading, form]
  )

  useEffect(() => {
    startLoading()

    fetchRoles()
  }, [])

  useEffect(() => {
    generateCode()
    fetchAdminUser().finally(() => {
      stopLoading()
    })
  }, [roles])

  return (
    <>
      <Grid container spacing={6} sx={{ marginBottom: '20px' }}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              action={
                <Button
                  variant='contained'
                  color='primary'
                  onClick={() => {
                    setFormModalTitle('create')
                    setShowFormModal(true)
                  }}
                >
                  Tambah Data Baru
                </Button>
              }
            />

            <CardContent>
              <Grid container spacing={6}>
                <Grid item xs={12}>
                  <ListUserTable
                    users={users}
                    onDelete={deleteAdminUser}
                    onEdit={async (id: any | string | number, user: any) => {
                      const { data } = await supabaseAdmin.auth.admin.getUserById(user?.uid)
                      setSelectedId(id)
                      setShowFormModal(true)
                      setFormModalTitle('edit')
                      setForm({
                        fullname: user?.fullname,
                        phone: user?.phone,
                        code: user?.code,
                        role_id: user?.role_id,
                        uid: user?.uid,
                        address: user?.address,
                        description: user?.description,
                        username: (data as any)?.user?.email.split('@invoice-app.com')[0],
                        password: user?.raw_password,
                        raw_password: user?.raw_password
                      })
                    }}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>

      <FormModal
        show={showFormModal}
        title={formModalTitle === 'create' ? 'Daftarkan User Baru' : 'Edit User'}
        onClose={closeModal}
        formComponent={
          <AdminUserForm
            onClose={closeModal}
            form={form}
            roles={roles}
            handleSubmit={formModalTitle === 'create' ? createAdminUser : updateAdminUser}
          />
        }
      />
    </>
  )
}

export default ListAdminUser
