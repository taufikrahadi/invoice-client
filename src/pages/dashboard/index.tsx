// ** MUI Imports
import Grid from '@mui/material/Grid'

// ** Icons Imports
import Poll from 'mdi-material-ui/Poll'
import CurrencyUsd from 'mdi-material-ui/CurrencyUsd'

// ** Custom Components Imports
import CardStatisticsVerticalComponent from 'src/@core/components/card-statistics/card-stats-vertical'

// ** Styled Component Import
import ApexChartWrapper from 'src/@core/styles/libs/react-apexcharts'
import { useState, useEffect, useCallback } from 'react'

// ** Demo Components Imports
import StatisticsCard from 'src/views/dashboard/StatisticsCard'
import WeeklyOverview from 'src/views/dashboard/WeeklyOverview'
import LineChartMoney from 'src/views/dashboard/LineChart'
import { Typography } from '@mui/material'
import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import dayjs from 'dayjs'
import timezone from 'dayjs/plugin/timezone'
import utc from 'dayjs/plugin/utc'
import router from 'next/router'

dayjs.extend(timezone)
dayjs.extend(utc)

interface Props {
  profileId?: string
}

const Dashboard = (props?: Props) => {
  const profile = useProfile()
  const supabase = useSupabaseClient()
  const { startLoading, stopLoading } = useLoadingUsingContext()

  const [moneyIn, setMoneyIn] = useState(0)
  const [moneyOut, setMoneyOut] = useState(0)
  const [groupedMoneyIn, setGroupedMoneyIn] = useState<number[]>([])

  const fetchMoneyIn = useCallback(async () => {
    const { data } = await supabase.rpc('sum_money_in')

    setMoneyIn(data)

    return
  }, [])

  const fetchMoneyInGroupByMonthBatch = useCallback(async () => {
    const { data: rpc, error } = await supabase.rpc('sum_every_month')

    if (error) {
      console.error('Error fetching data:', error)

      return
    }

    // Initialize an array for 12 months with default values
    const monthlyTotals = Array(12).fill(0)


      ; (rpc as any[]).forEach((item: any) => {
        const monthIndex = item.month - 1// Month index (0-11)
        monthlyTotals[monthIndex] += item.total
      })

    setGroupedMoneyIn(monthlyTotals)
  }, [supabase, setGroupedMoneyIn])

  useEffect(() => {
    startLoading()
    Promise.all([fetchMoneyIn(), fetchMoneyInGroupByMonthBatch()]).then(() => {
      stopLoading()
    })
  }, [profile])

  return (
    <ApexChartWrapper>
      <Grid container spacing={6} sx={{ marginBottom: '20px' }}>
        <Grid item xs={12}>
          <Typography variant='h4'>Halo, {profile?.fullname}</Typography>
        </Grid>
        <Grid item xs={12} md={8}>
          <StatisticsCard />
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <Grid container spacing={6}>
            <Grid item xs={6}>
              <CardStatisticsVerticalComponent
                stats={new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(moneyIn)}
                icon={<Poll />}
                color='success'
                trendNumber='+42%'
                title='Uang Masuk'
                subtitle='Jumlah Uang Masuk'
              />
            </Grid>
            <Grid item xs={6}>
              <CardStatisticsVerticalComponent
                stats={new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(moneyOut)}
                title='Uang Keluar'
                trend='negative'
                color='warning'
                trendNumber='-15%'
                subtitle='Jumlah Uang Keluar'
                icon={<CurrencyUsd />}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <WeeklyOverview moneyIn={moneyIn} moneyOut={moneyOut} />
        </Grid>
        <Grid item xs={12} md={6}>
          <LineChartMoney
            categories={[
              'Januari',
              'Februari',
              'Maret',
              'April',
              'Mei',
              'Juni',
              'Juli',
              'Agustus',
              'September',
              'Oktober',
              'November',
              'Desember'
            ].map((s: string) => s.substring(0, 3))}
            dataSeries={[
              {
                name: 'Uang Masuk',
                data: groupedMoneyIn
              }
            ]}
            colors={['#9155FD']}
            yAxisOption={{
              axisTicks: {
                show: true
              },
              axisBorder: {
                show: true,
                color: '#FF1654'
              },
              labels: {
                style: {
                  colors: '#FF1654'
                }
              },
              title: {
                text: 'Uang Masuk',
                style: {
                  color: '#FF1654'
                }
              }
            }}
          />
        </Grid>
      </Grid>
    </ApexChartWrapper>
  )
}

export default Dashboard
