import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useSupabaseAdmin } from 'src/@core/hooks/useSupabaseAdmin'
import { useState, useEffect, useCallback } from 'react'
import { PartnerFormSchema } from '../new'
import { useRouter } from 'next/router'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { Grid } from '@mui/material'
import PartnerForm from 'src/views/forms/PartnerForm'
import { isNotEmpty, IsOptional, MinLength } from 'class-validator'

class UpdatePartnerFormSchema extends PartnerFormSchema {
  @IsOptional()
  @MinLength(6)
  password: string
}

const EditPartner = () => {
  const supabase = useSupabaseClient()
  const supabaseAdmin = useSupabaseAdmin()
  const [form, setForm] = useState<UpdatePartnerFormSchema>({
    fullname: '',
    phone: '',
    code: '',
    role_id: '',
    uid: '',
    address: '',
    description: '',
    username: '',
    password: '',
    raw_password: ''
  })
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const router = useRouter()
  const [isSuccess, setIsSuccess] = useState(false)
  const { id } = router.query

  const fetchPartner = useCallback(async () => {
    const { data, error } = await supabase.from('profiles').select('*, roles(id, name)').eq('id', id).single()
    const {
      data: { user }
    } = await supabaseAdmin.auth.admin.getUserById(data.uid)

    const [username] = (user?.email as any).split('@')
    setForm({
      fullname: data.fullname,
      phone: data.phone,
      code: data.code,
      role_id: data.role_id,
      uid: data.uid,
      address: data.address,
      description: data.description,
      username: username,
      password: data.raw_password,
      raw_password: data.raw_password
    })
  }, [supabase, setForm, id])

  const savePartner = useCallback(
    async (sentForm: UpdatePartnerFormSchema) => {
      if (form.username !== sentForm.username || isNotEmpty(sentForm.password)) {
        await supabaseAdmin.auth.admin.updateUserById(form.uid, {
          email: `${sentForm.username}@invoice-app.com`,
          password: sentForm.password,
          email_confirm: true
        })
      }

      await supabase
        .from('profiles')
        .update({
          fullname: sentForm.fullname,
          role_id: sentForm.role_id,
          phone: sentForm.phone,
          code: sentForm.code,
          address: sentForm.address,
          description: sentForm.description
        })
        .eq('id', id)

      setIsSuccess(true)
      router.push('/partners')
      stopLoading()
    },
    [supabase, id, router]
  )

  useEffect(() => {
    startLoading()
    fetchPartner().then(() => {
      stopLoading()
    })
  }, [])

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <PartnerForm title='Ubah Data Mitra' form={form} save={savePartner} />
        </Grid>
      </Grid>
    </>
  )
}

export default EditPartner
