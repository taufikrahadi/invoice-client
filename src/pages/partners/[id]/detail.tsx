import { useRouter } from 'next/router'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useCallback, useEffect, useState } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { ROLEPARTNER } from '..'
import { Grid, Card, Box, Button, Avatar, CardContent, Typography } from '@mui/material'
import { ArrowLeft, Delete, Pencil, Phone, Home, Note } from 'mdi-material-ui'
import ConfirmDialog from 'src/views/ConfirmDialog'

const DetailPartner = () => {
  const router = useRouter()
  const { id } = router.query
  const supabase = useSupabaseClient()
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const [partner, setPartner] = useState<any>({})
  const [invoices, setInvoices] = useState<any[]>([])
  const [showConfirmDelete, setShowConfirmDelete] = useState(false)

  const fetchPartner = useCallback(async () => {
    const { data } = await supabase.from('profiles').select('*').eq('id', id).in('role_id', ROLEPARTNER).single()

    setPartner(data as any)
  }, [supabase, setPartner, id])

  const deletePartner = useCallback(async () => {
    startLoading()
    await supabase
      .from('profiles')
      .update({
        deleted_at: new Date()
      })
      .eq('id', id)

    router.back()
    stopLoading()
  }, [id, supabase, router])

  const fetchInvoice = useCallback(async () => {
    const { data } = await supabase
      .from('invoices')
      .select('*, invoice_statuses(id, name)')
      .eq('profile_id', id)
      .in('invoice_status_id', [1, 2])

    setInvoices(data as any)
  }, [supabase, setInvoices])

  useEffect(() => {
    Promise.all([fetchPartner(), fetchInvoice()]).then(() => {
      stopLoading()
    })
  }, [])

  return (
    <>
      <Grid container spacing={8} sx={{ marginBottom: '20px' }}>
        <Grid item xs={12}>
          <Card>
            <Box
              sx={{
                padding: '10px',
                display: 'flex',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <Button variant='text' onClick={() => router.back()} color='secondary'>
                <ArrowLeft />
              </Button>
              <Box>
                <Button variant='text' color='info' onClick={() => router.push(`/partners/${id}/edit`)}>
                  <Pencil sx={{ color: 'info', marginRight: 1.5 }} />
                  Edit
                </Button>
                <Button variant='text' color='error' onClick={() => setShowConfirmDelete(true)}>
                  <Delete sx={{ color: 'error', marginRight: 1.5 }} />
                  Delete
                </Button>
              </Box>
            </Box>

            <CardContent sx={{ padding: theme => `${theme.spacing(3.25, 5.75, 6.25)} !important` }}>
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: 5,
                  flexDirection: 'column'
                }}
              >
                <Avatar
                  alt='John Doe'
                  sx={{ width: 100, height: 100, marginBottom: '20px' }}
                  src='/images/avatars/1.png'
                />

                <Typography variant='h5'>{partner?.fullname}</Typography>
              </Box>

              <Grid container spacing={6}>
                <Grid item xs={6} sx={{ alignItems: 'center' }}>
                  <Typography variant='h6' color='secondary'>
                    # No Mitra
                  </Typography>
                  <Typography variant='h5'>{partner?.code}</Typography>
                </Grid>

                <Grid item xs={6} sx={{ textAlign: 'right' }}>
                  <Box sx={{ display: 'flex', justifyContent: 'end', alignItems: 'center' }}>
                    <Typography variant='h6' color='secondary'>
                      <Phone />
                    </Typography>
                    <Typography variant='h6' color='secondary' sx={{ marginTop: '-5px', marginLeft: '3px' }}>
                      Nomor Telepon
                    </Typography>
                  </Box>
                  <Typography variant='h5'>{partner?.phone}</Typography>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>

        <Grid item xs={12} md={6}>
          <Card>
            <CardContent>
              <Typography variant='h6' color='secondary'>
                <Box sx={{ display: 'flex', justifyContent: 'start', alignItems: 'center' }}>
                  <Home /> Alamat
                </Box>
              </Typography>

              <Typography variant='h5'>{partner?.address}</Typography>
            </CardContent>
          </Card>
        </Grid>

        <Grid item xs={12} md={6}>
          <Card>
            <CardContent>
              <Typography variant='h6' color='secondary'>
                <Box sx={{ display: 'flex', justifyContent: 'start', alignItems: 'center' }}>
                  <Note /> Catatan
                </Box>
              </Typography>

              <Typography variant='h5'>{partner?.description}</Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>

      <ConfirmDialog
        show={showConfirmDelete}
        setShow={setShowConfirmDelete}
        title='Hapus Data?'
        message='Yakin ingin menghapus Mitra? Nota dari mitra ini akan tetap disimpan dan tidak dihapus.'
        action={deletePartner}
      />
    </>
  )
}

export default DetailPartner
