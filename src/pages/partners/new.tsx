import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useCallback, useState, useEffect } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { useListRole } from 'src/modules/role'
import { IsNotEmpty, IsPhoneNumber, IsOptional, MinLength } from 'class-validator'
import { useRouter } from 'next/router'
import { Grid } from '@mui/material'
import PartnerForm from 'src/views/forms/PartnerForm'
import { useSupabaseAdmin } from 'src/@core/hooks/useSupabaseAdmin'
import { ROLEPARTNER } from './index'

export class PartnerFormSchema {
  constructor(form: PartnerFormSchema) {
    Object.assign(this, form)
  }

  @IsNotEmpty()
  fullname: string

  @IsNotEmpty()
  username: string

  @IsOptional()
  address: string

  @IsOptional()
  description: string

  @IsNotEmpty()
  @IsPhoneNumber('ID')
  phone: string

  @IsNotEmpty()
  code: string

  @IsNotEmpty()
  role_id: string

  @IsOptional()
  uid: string

  @IsOptional()
  @MinLength(6)
  password: string

  @IsNotEmpty()
  @MinLength(6)
  raw_password: string
}

const NewPartner = () => {
  const supabase = useSupabaseClient()
  const supabaseAdmin = useSupabaseAdmin()
  const [form, setForm] = useState<PartnerFormSchema>({
    fullname: '',
    phone: '',
    code: '',
    role_id: '',
    uid: '',
    address: '',
    description: '',
    username: '',
    password: '',
    raw_password: ''
  })
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const router = useRouter()
  const { roles } = useListRole()
  const [isSuccess, setIsSuccess] = useState(false)

  const buildNoMitra = useCallback(async () => {
    const { data: roless } = await supabase.from('roles').select('id').eq('type', 'Partner')
    const { count } = await supabase
      .from('profiles')
      .select('id', { count: 'exact' })
      .in(
        'role_id',
        (roless as any[]).map(role => role.id)
      )

    const formattedCount = new Intl.NumberFormat(`en-US`, { minimumIntegerDigits: 3 }).format(Number(count) + 1)

    return formattedCount
  }, [supabase])

  const savePartner = useCallback(
    async (form: PartnerFormSchema) => {
      const { data, error } = await supabaseAdmin.auth.admin.createUser({
        email: `${form.username}@invoice-app.com`,
        password: form.password,
        email_confirm: true
      })

      await supabase.from('profiles').insert({
        uid: data.user?.id,
        fullname: form.fullname,
        role_id: form.role_id,
        phone: form.phone,
        code: form.code,
        address: form.address,
        description: form.description,
        raw_password: form.password
      })

      setIsSuccess(true)
      router.push('/partners')
      stopLoading()
    },
    [supabase]
  )

  useEffect(() => {
    buildNoMitra().then(count => {
      setForm({
        ...form,
        code: count
      })
    })
  }, [])

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <PartnerForm title='Buat Mitra Baru' form={form} save={savePartner} />
        </Grid>
      </Grid>
    </>
  )
}

export default NewPartner
