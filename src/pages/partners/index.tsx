import { useSupabaseClient } from '@supabase/auth-helpers-react'
import DataCard from 'src/views/DataCard'
import { useCallback, useEffect, useRef, useState } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import {
  Card,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  MenuItem,
  Select,
  Box,
  FormControl,
  InputLabel,
  SelectChangeEvent,
  Button,
  CardContent,
  Typography,
  Chip,
  CardActionArea
} from '@mui/material'
import FilterAndSortSection from 'src/layouts/components/FilterAndSortSection'
import TotalItemText from 'src/layouts/components/TotalItem'
import { useListRole } from 'src/modules/role'
import FloatingAddButton from 'src/@core/components/FloatingAddButton'
import { useRouter } from 'next/router'
import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'
import { useInView } from 'react-intersection-observer'

export const ROLEPARTNER = ['91891723-e858-4d8f-a358-7946359eb33b', 'af12d3b7-3619-49bb-adf2-65cdad012061']

const ListIndex = () => {
  const supabase = useSupabaseClient()
  const [partners, setPartners] = useState<any[]>([])
  const [partnerCount, setPartnerCount] = useState<number>(0)
  const [countText, setCountText] = useState<string>('Mitra')
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const [searchTerm, setSearchTerm] = useState('')
  const [sort, setSort] = useState<'ASC' | 'DESC'>('ASC')
  const [showModalPosition, setShowModalPosition] = useState<null | 'filter' | 'sort'>(null)
  const [offset, setOffset] = useState(0)
  const [isMaxOffset, setIsMaxOffset] = useState(false)
  const { roles } = useListRole()
  const [roleId, setRoleId] = useState<any[]>(ROLEPARTNER)
  const router = useRouter()
  const profile = useProfile()
  const { ref, inView } = useInView()

  const fetchPartners = useCallback(
    async (isNeedToPush = true) => {
      startLoading()

      if (isMaxOffset) {
        stopLoading()

        return
      }

      const query = supabase
        .from('profiles')
        .select(
          '*, roles(id, name), invoices(id, invoice_date, remaining_amount, invoice_number, code, invoice_status_id, deleted_at)',
          { count: 'exact' }
        )
        .is('deleted_at', null)
        .is('invoices.deleted_at', null)

      if (offset === 0) {
        query.range(0, 5)
      } else {
        query.range(offset + 1, offset + 5)
      }

      const { data, count, error } = await query
        .in('invoices.invoice_status_id', [1, 2])
        .ilike('fullname', `%${searchTerm}%`)
        .in('role_id', roleId)
        .order('fullname', { ascending: sort === 'ASC' })

      if (error?.code === 'PGRST103') {
        setIsMaxOffset(true)
        stopLoading()

        return
      }

      if (count) setPartnerCount(count as any)
      else setPartnerCount(0)

      if (offset <= (count as number)) {
        setOffset(offset + 5)
      }

      if (isNeedToPush) {
        let needToBePush = partners
        needToBePush = [...partners, ...(data as any)]
        setPartners(needToBePush)
      } else {
        setPartners(data as any)
      }

      stopLoading()
    },
    [supabase, setPartners, partners, roleId, searchTerm, sort, offset, isMaxOffset, setIsMaxOffset]
  )

  const fetchRoleById = useCallback(async () => {
    if (roleId) {
      startLoading()

      const { data } = await supabase.from('roles').select('id, name').eq('type', 'Partner').single()

      setCountText(data?.name as any)
      stopLoading()
    }
  }, [supabase, roleId, setCountText])

  const getAmountTotal = (invoices: any[]) => {
    let initialValue = 0
    for (const invoice of invoices) {
      initialValue += invoice?.remaining_amount
    }

    return initialValue
  }

  // mounted
  useEffect(() => {
    setIsMaxOffset(false)
    fetchPartners()
  }, [])

  useEffect(() => {
    setOffset(0)
    setIsMaxOffset(false)
    fetchPartners(false)
  }, [roleId, searchTerm, sort])

  useEffect(() => {
    fetchRoleById()
  }, [roleId])

  return (
    <div ref={ref}>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <FilterAndSortSection
            searchTerm={searchTerm}
            toggleModalFilterOpen={() => setShowModalPosition('filter')}
            toggleModalSortOpen={() => setShowModalPosition('sort')}
            handleSearchChange={(e: any) => setSearchTerm(e.target.value)}
          />
        </Grid>

        <Grid item xs={12} sx={{ textAlign: 'center', marginY: '20px' }}>
          <TotalItemText text={`${partnerCount ?? 0} ${roleId.length >= 2 ? 'Mitra' : countText}`} />
        </Grid>

        {partners.map((partner: any) => (
          <Grid item lg={4} md={6} xs={12} key={partner?.id}>
            <DataCard
              name={partner?.fullname}
              code={partner?.code}
              money={getAmountTotal(partner?.invoices)}
              categoryOrSomething={partner?.roles?.name}
              onClick={() => router.push(`/partners/${partner?.id}/detail`)}
            />
          </Grid>
        ))}

        <Grid item xs={12} container>
          <Button variant='contained' color='primary' onClick={() => fetchPartners()}>
            Lihat Lebih Banyak
          </Button>
        </Grid>
      </Grid>

      <Dialog open={showModalPosition === 'filter'} onClose={() => setShowModalPosition(null)}>
        <DialogTitle>Filter Mitra</DialogTitle>

        <DialogContent>
          <FormControl fullWidth>
            <InputLabel>Jenis Mitra</InputLabel>
            <Select
              labelId='demo-simple-select-label'
              id='demo-simple-select'
              value={roleId.length > 1 ? null : roleId[0]}
              label='Role'
              onChange={(e: SelectChangeEvent) => setRoleId([e.target.value as string])}
            >
              {roles.map(role => (
                <MenuItem value={role.id} key={role.id}>
                  {role.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </DialogContent>

        <DialogActions>
          <Button
            variant='contained'
            color='secondary'
            onClick={() => {
              setRoleId(ROLEPARTNER)
              setCountText('Mitra')
              setShowModalPosition(null)
            }}
          >
            Reset
          </Button>
          <Button variant='contained' color='primary' onClick={() => setShowModalPosition(null)}>
            Terapkan
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog open={showModalPosition === 'sort'} onClose={() => setShowModalPosition(null)}>
        <DialogTitle>Urutkan</DialogTitle>

        <DialogContent>
          <FormControl fullWidth>
            <InputLabel>Urut Dari</InputLabel>
            <Select
              labelId='demo-simple-select-label'
              id='demo-simple-select'
              value={sort}
              label='Age'
              onChange={(e: SelectChangeEvent) => setSort(e.target.value as any)}
            >
              <MenuItem value={'ASC'}>A - Z</MenuItem>
              <MenuItem value={'DESC'}>Z - A</MenuItem>
            </Select>
          </FormControl>
        </DialogContent>

        <DialogActions>
          <Button
            variant='contained'
            color='secondary'
            onClick={() => {
              setSort('ASC')
              setShowModalPosition(null)
            }}
          >
            Reset
          </Button>
          <Button variant='contained' color='primary' onClick={() => setShowModalPosition(null)}>
            Terapkan
          </Button>
        </DialogActions>
      </Dialog>

      {['Superadmin', 'Owner'].includes(profile?.roles.name) ? (
        <FloatingAddButton handleClick={() => router.push('/partners/new')} />
      ) : (
        <></>
      )}
    </div>
  )
}

export default ListIndex
