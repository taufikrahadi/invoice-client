import {
  Card,
  CardHeader,
  CardContent,
  Grid,
  Button,
  Table,
  TableRow,
  TableBody,
  TableHead,
  TableCell,
  TableContainer,
  IconButton,
  Tabs,
  Tab,
  Box
} from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useCallback, useEffect, useState } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import TipeMitraTable from 'src/views/settings/TipeMitraTable'
import TipeKategoriTable from 'src/views/settings/TipeKategoriTable'
import FormModal from 'src/views/users/FormModal'
import RoleAndCategoryForm from 'src/views/forms/RoleAndCategoryForm'

const TabSettingTipeMitra = () => {
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const [showModalForm, setShowModalForm] = useState(false)
  const [form, setForm] = useState<any>({})
  const [refetch, setRefetch] = useState(false)
  const supabase = useSupabaseClient()

  const handleEdit = (value: any) => {
    setShowModalForm(true)
    setForm({
      id: value?.id,
      name: value?.name
    })
  }

  const handleClose = () => {
    setShowModalForm(false)
    setForm({})
  }

  const handleSubmit = async (inputted: any) => {
    startLoading()

    await supabase.from('roles').upsert({
      id: inputted?.id,
      name: inputted?.name,
      type: 'Partner'
    })

    handleClose()
    stopLoading()
    setRefetch(!refetch)

    return
  }

  return (
    <>
      <FormModal
        show={showModalForm}
        onClose={() => setShowModalForm(false)}
        formComponent={
          <RoleAndCategoryForm onSubmit={handleSubmit} onCancel={handleClose} form={form} setForm={setForm} />
        }
        title={'Tipe Mitra'}
      />

      <Card>
        <CardHeader
          subheader='Setting Tipe Mitra'
          action={
            <Button
              color='primary'
              variant='contained'
              onClick={() => {
                setShowModalForm(true)
              }}
            >
              Tambah Mitra Baru
            </Button>
          }
        />
        <CardContent>
          <TipeMitraTable handleEditButton={handleEdit} refetch={refetch} />
        </CardContent>
      </Card>
    </>
  )
}

const TabSettingKategoriProduk = () => {
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const [showModalForm, setShowModalForm] = useState(false)
  const [form, setForm] = useState<any>({})
  const [refetch, setRefetch] = useState(false)
  const supabase = useSupabaseClient()

  const handleEdit = (value: any) => {
    setShowModalForm(true)
    setForm({
      id: value?.id,
      name: value?.name
    })
  }

  const handleClose = () => {
    setShowModalForm(false)
    setForm({})
  }

  const handleSubmit = async (inputted: any) => {
    startLoading()

    await supabase.from('product_categories').upsert({
      id: inputted?.id,
      name: inputted?.name
    })

    handleClose()
    stopLoading()
    setRefetch(!refetch)

    return
  }

  return (
    <>
      <FormModal
        show={showModalForm}
        onClose={() => setShowModalForm(false)}
        formComponent={
          <RoleAndCategoryForm onSubmit={handleSubmit} onCancel={handleClose} form={form} setForm={setForm} />
        }
        title={'Kategori Produk'}
      />

      <Card>
        <CardHeader
          subheader='Setting Tipe Kategori Produk'
          action={
            <Button
              color='primary'
              variant='contained'
              onClick={() => {
                setShowModalForm(true)
              }}
            >
              Tambah Kategori Baru
            </Button>
          }
        />
        <CardContent>
          <TipeKategoriTable handleEditButton={handleEdit} refetch={refetch} />
        </CardContent>
      </Card>
    </>
  )
}

const PartnerSetting = () => {
  const supabase = useSupabaseClient()
  const [tabValue, setTabValue] = useState(0)

  const components = [<TabSettingTipeMitra key={1} />, <TabSettingKategoriProduk key={2} />]

  return (
    <>
      <Grid container spacing={6} sx={{ marginBottom: '20px' }}>
        <Grid item xs={12}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider', marginBottom: '10px' }}>
            <Tabs value={tabValue} onChange={(e, newValue: number) => setTabValue(newValue)}>
              <Tab label='Tipe Mitra' />
              <Tab label='Kategori Produk' />
            </Tabs>
          </Box>
        </Grid>
        <Grid item xs={12}>
          {components[tabValue]}
        </Grid>
      </Grid>
    </>
  )
}

export default PartnerSetting
