import { decode } from 'base64-arraybuffer'
import {
  Box,
  Grid,
  Card,
  CardContent,
  CardHeader,
  CardActions,
  Button,
  TextField,
  Typography,
  IconButton
} from '@mui/material'
import { isURL } from 'class-validator'
import { TrashCan, Plus } from 'mdi-material-ui'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useCallback, useEffect, useState, ChangeEvent } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { useRouter } from 'next/router'
import { useFormValidation } from 'src/@core/hooks/useForm'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { findIsError, findErrorProperty } from 'src/@core/utils/validateSync'
import { useProfile } from 'src/@core/hooks/useProfile'

class Company {
  id: number
  name: string
  address: string
  phone: string
  email: string
  bank_accounts: { pic: string; bank: string; accountNo: string }[]
  logo: string
  created_at: string
  stores: { name: string; address: string }[]
}

const generateSlug = (companyName: string): string => {
  return companyName.toLowerCase().replace(' ', '-')
}

const CompanySetting = () => {
  const supabase = useSupabaseClient()
  const [company, setCompany] = useState<Company>({
    id: null as any,
    name: '',
    address: '',
    phone: '',
    email: '',
    bank_accounts: [],
    logo: '',
    created_at: '',
    stores: []
  })
  const [logo, setLogo] = useState('')
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const router = useRouter()
  const { runValidation, errors } = useFormValidation()
  const handleChange = useHandleChange(setCompany, company)
  const profile = useProfile()

  const fetchCompanySetting = useCallback(async () => {
    const { data } = await supabase.from('companies').select('*').eq('id', profile?.company_id).single()

    setCompany({
      ...(data as any)
    })
    setLogo(data.logo)

    return data
  }, [setCompany, supabase, profile])

  const uploadLogo = useCallback(async () => {
    if (!isURL(company.logo)) {
      const [type, _] = logo.split(';base64')
      const [__, mimetype] = type.split('data:')
      const [___, ext] = mimetype.split('/')
      const filename = `${generateSlug(company?.name)}.${ext}`
      await supabase.storage.from('logo').upload(filename, decode(logo.split('base64,')[1]), {
        upsert: true,
        contentType: mimetype
      })

      const url = supabase.storage.from('logo').getPublicUrl(filename)

      return url.data.publicUrl
    }

    return company.logo
  }, [supabase, logo, company])

  const handleSubmit = useCallback(
    async (e: any) => {
      e.preventDefault()
      startLoading()

      const logo = await uploadLogo()
      await supabase
        .from('companies')
        .update({
          ...company,
          logo
        })
        .eq('id', company.id)

      router.back()

      setTimeout(() => {
        stopLoading()
      }, 1500)
      Promise.resolve(true)
    },
    [supabase, uploadLogo, company, router, startLoading, stopLoading]
  )

  useEffect(() => {
    if (profile?.id) {
      fetchCompanySetting().then(() => {
        stopLoading()
      })
    }
  }, [profile])

  return (
    <>
      <Grid
        container
        spacing={6}
        sx={{
          marginBottom: '20px'
        }}
      >
        <Grid item xs={12}>
          <Card>
            <CardHeader title='Setting Data Perusahaan' />
            <form onSubmit={handleSubmit}>
              <CardContent>
                <Grid container spacing={6}>
                  <Grid item xs={12}>
                    <TextField
                      label='Nama Perusahaan'
                      variant='filled'
                      fullWidth
                      value={company?.name}
                      onChange={(e) => {
                        setCompany({
                          ...company,
                          name: e.target.value
                        })
                      }}
                    />
                  </Grid>

                  <Grid item xs={6}>
                    <TextField
                      label='Email Perusahaan'
                      variant='filled'
                      fullWidth
                      value={company?.email}
                      onChange={(e) => {
                        setCompany({
                          ...company,
                          email: e.target.value
                        })
                      }}
                    />
                  </Grid>

                  <Grid item xs={6}>
                    <TextField
                      label='No HP Perusahaan'
                      variant='filled'
                      fullWidth
                      value={company?.phone}
                      onChange={(e) => {
                        setCompany({
                          ...company,
                          phone: e.target.value
                        })
                      }}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <TextField
                      label='Alamat Perusahaan'
                      rows={4}
                      multiline
                      variant='filled'
                      fullWidth
                      value={company?.address}
                      onChange={(e) => {
                        setCompany({
                          ...company,
                          address: e.target.value
                        })
                      }}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Typography>Logo (.jpg, .png)</Typography>
                  </Grid>

                  <Grid item xs={12}>
                    <img
                      src={logo}
                      style={{
                        height: '10vh'
                      }}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        gap: 5
                      }}
                    >
                      <Button variant='contained' color='primary' component='label'>
                        Upload Logo
                        <input
                          type='file'
                          hidden
                          onChange={(e: ChangeEvent<HTMLInputElement>) => {
                            if ((e.target.files as any)[0]) {
                              setCompany({
                                ...company,
                                logo: (e.target.files as any)[0]
                              })
                              const reader = new FileReader()
                              const file = (e.target.files as any)[0]
                              reader.onloadend = () => {
                                setLogo(reader.result as any)
                              }
                              reader.readAsDataURL(file)
                            }
                          }}
                        />
                      </Button>
                      <Typography variant='subtitle2'>Format gambar jpg atau png, Maksimal 1MB.</Typography>
                    </Box>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography>Alamat Outlet</Typography>
                  </Grid>

                  {company?.stores.map((store, i) => (
                    <Grid item xs={12} key={i}>
                      <Card>
                        <CardContent>
                          <Box sx={{ width: '100%', marginY: '10px' }}>
                            <TextField
                              label='Nama Outlet'
                              value={store.name}
                              fullWidth
                              variant='filled'
                              onChange={(e) => {
                                const updatedAccounts = [...company.stores];
                                updatedAccounts[i] = { ...updatedAccounts[i], name: e.target.value };
                                setCompany({ ...company, stores: updatedAccounts });
                              }}
                            />
                          </Box>
                          <Box sx={{ width: '100%', marginY: '10px' }}>
                            <TextField label='Alamat Outlet' value={store.address} variant='filled' fullWidth
                              onChange={(e) => {
                                const updatedAccounts = [...company.stores];
                                updatedAccounts[i] = { ...updatedAccounts[i], address: e.target.value };
                                setCompany({ ...company, stores: updatedAccounts });
                              }}
                            />
                          </Box>
                        </CardContent>
                        <CardActions>
                          <Box
                            sx={{
                              display: 'flex',
                              justifyContent: 'end',
                              alignItems: 'center',
                              width: '100%'
                            }}
                          >
                            <IconButton
                              color='error'
                              onClick={() => {
                                const arr = company.stores
                                arr.splice(i, 1)

                                setCompany({
                                  ...company,
                                  stores: arr
                                })
                              }}
                            >
                              <TrashCan />
                            </IconButton>
                          </Box>
                        </CardActions>
                      </Card>
                    </Grid>
                  ))}

                  <Grid item xs={12}>
                    <Button
                      color='primary'
                      onClick={() => {
                        setCompany({
                          ...company,
                          stores: [...company.stores, { name: '', address: '' }]
                        })
                      }}
                    >
                      <Plus /> Tambah Outlet
                    </Button>
                  </Grid>

                  <Grid item xs={12}>
                    <Typography>Rekening Bank</Typography>
                  </Grid>

                  {company?.bank_accounts.map((bankAccount, index) => (
                    <Grid item xs={12} key={index}>
                      <Card>
                        <CardContent>
                          <Box sx={{ width: '100%', marginY: '10px' }}>
                            <TextField label='Nama Bank' value={bankAccount.bank} fullWidth variant='filled'
                              onChange={(e) => {
                                const updatedAccounts = [...company.bank_accounts];
                                updatedAccounts[index] = { ...updatedAccounts[index], bank: e.target.value };
                                setCompany({ ...company, bank_accounts: updatedAccounts });
                              }}
                            />
                          </Box>
                          <Box sx={{ width: '100%', marginY: '10px' }}>
                            <TextField
                              label='Nomor Rekening'
                              value={bankAccount.accountNo}
                              variant='filled'
                              fullWidth
                              onChange={(e) => {
                                const updatedAccounts = [...company.bank_accounts];
                                updatedAccounts[index] = { ...updatedAccounts[index], accountNo: e.target.value };
                                setCompany({ ...company, bank_accounts: updatedAccounts });
                              }}
                            />
                          </Box>
                          <Box sx={{ width: '100%', marginY: '10px' }}>
                            <TextField label='Atas Nama' value={bankAccount.pic} fullWidth variant='filled'
                              onChange={(e) => {
                                const updatedAccounts = [...company.bank_accounts];
                                updatedAccounts[index] = { ...updatedAccounts[index], pic: e.target.value };
                                setCompany({ ...company, bank_accounts: updatedAccounts });
                              }}
                            />
                          </Box>
                        </CardContent>
                        <CardActions>
                          <Box
                            sx={{
                              display: 'flex',
                              justifyContent: 'end',
                              alignItems: 'center',
                              width: '100%'
                            }}
                          >
                            <IconButton
                              color='error'
                              onClick={() => {
                                const arr = company.bank_accounts
                                arr.splice(index, 1)

                                setCompany({
                                  ...company,
                                  bank_accounts: arr
                                })
                              }}
                            >
                              <TrashCan />
                            </IconButton>
                          </Box>
                        </CardActions>
                      </Card>
                    </Grid>
                  ))}

                  <Grid item xs={12}>
                    <Button
                      color='primary'
                      onClick={() => {
                        setCompany({
                          ...company,
                          bank_accounts: [...company.bank_accounts, { pic: '', bank: '', accountNo: '' }]
                        })
                      }}
                    >
                      <Plus /> Tambah Rekening
                    </Button>
                  </Grid>
                </Grid>
              </CardContent>

              <CardActions>
                <Box sx={{ display: 'flex', width: '100%', justifyContent: 'end' }}>
                  <Button variant='text' color='secondary' type='submit' onClick={() => router.back()}>
                    Batalkan
                  </Button>
                  <Button variant='contained' color='primary' type='submit'>
                    Submit
                  </Button>
                </Box>
              </CardActions>
            </form>
          </Card>
        </Grid>
      </Grid>
    </>
  )
}

export default CompanySetting
