// ** React Imports
import { ChangeEvent, MouseEvent, ReactNode, useCallback, useState } from 'react'

// ** Next Imports
import Link from 'next/link'
import { useRouter } from 'next/router'

// ** MUI Components
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Divider from '@mui/material/Divider'
import Checkbox from '@mui/material/Checkbox'
import TextField from '@mui/material/TextField'
import InputLabel from '@mui/material/InputLabel'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import OutlinedInput from '@mui/material/OutlinedInput'
import { styled, useTheme } from '@mui/material/styles'
import MuiCard, { CardProps } from '@mui/material/Card'
import InputAdornment from '@mui/material/InputAdornment'
import MuiFormControlLabel, { FormControlLabelProps } from '@mui/material/FormControlLabel'

// ** Icons Imports
import Google from 'mdi-material-ui/Google'
import Github from 'mdi-material-ui/Github'
import Twitter from 'mdi-material-ui/Twitter'
import Facebook from 'mdi-material-ui/Facebook'
import EyeOutline from 'mdi-material-ui/EyeOutline'
import EyeOffOutline from 'mdi-material-ui/EyeOffOutline'

// ** Configs
import themeConfig from 'src/configs/themeConfig'

// ** Layout Import
import BlankLayout from 'src/@core/layouts/BlankLayout'

// ** Demo Imports
import FooterIllustrationsV1 from 'src/views/pages/auth/FooterIllustration'

import { useSession, useSupabaseClient } from '@supabase/auth-helpers-react'
import { useLoading } from 'src/@core/hooks/useLoading'
import Loading from 'src/views/Loading'

interface State {
  email: string
  password: string
  showPassword: boolean
}

// ** Styled Components
const Card = styled(MuiCard)<CardProps>(({ theme }) => ({
  [theme.breakpoints.up('sm')]: { width: '28rem' }
}))

const LinkStyled = styled('a')(({ theme }) => ({
  fontSize: '0.875rem',
  textDecoration: 'none',
  color: theme.palette.primary.main
}))

const FormControlLabel = styled(MuiFormControlLabel)<FormControlLabelProps>(({ theme }) => ({
  '& .MuiFormControlLabel-label': {
    fontSize: '0.875rem',
    color: theme.palette.text.secondary
  }
}))

const LoginPage = () => {
  const session = useSession()

  const router = useRouter()
  if (session) router.push('/')

  // ** State
  const [values, setValues] = useState<State>({
    email: '',
    password: '',
    showPassword: false
  })

  // ** Hook
  const theme = useTheme()

  const handleChange = (prop: keyof State) => (event: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [prop]: event.target.value })
  }

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword })
  }

  const handleMouseDownPassword = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
  }
  const [errorMessage, setErrorMessage] = useState('')
  const [isError, setIsError] = useState(false)

  const { loading, startLoading, stopLoading } = useLoading(false)
  const supabase = useSupabaseClient()

  const afterLogin = async (user: any) => {
    const directToInvoice = ['Reseller', 'Penjahit', 'Owner', 'Admin']
    const { data } = await supabase.from('profiles').select('*, roles(*)').eq('uid', user.id).single()

    const {
      id,
      roles: { name }
    } = data

    if (directToInvoice.includes(name)) {
      if (['Reseller', 'Penjahit'].includes(name)) {
        router.push(`/invoices/${id}/invoices`)

        return
      } else {
        router.push(`/invoices/`)

        return
      }
    } else {
      router.push('/')

      return
    }
  }

  const handleSubmit = useCallback(
    (e: any) => {
      e.preventDefault()
      startLoading()
      setErrorMessage('')
      setIsError(false)
      const suffixEmail = '@invoice-app.com'

      supabase.auth
        .signInWithPassword({
          email: values.email + suffixEmail,
          password: values.password
        })
        .then(async ({ data: { user, session }, error }) => {
          if (error) {
            if (error?.message === 'Invalid login credentials') {
              setErrorMessage('Username atau password anda salah, mohon cek kembali')
              setIsError(true)

              return
            }
          }

          const { data: profile } = await supabase
            .from('profiles')
            .select('*, companies(*)')
            .eq('uid', user?.id)
            .single()

          localStorage.setItem('access_token', String(session?.access_token))
          localStorage.setItem('user_profile', JSON.stringify(profile))
          afterLogin(user)

          return
        })
        .finally(() => {
          stopLoading()
        })
    },
    [values]
  )

  return (
    <Box className='content-center'>
      <Loading show={loading} />
      <Card sx={{ zIndex: 1 }}>
        <CardContent sx={{ padding: theme => `${theme.spacing(12, 9, 7)} !important` }}>
          <Box sx={{ mb: 6 }}>
            <Typography variant='h5' sx={{ fontWeight: 600, marginBottom: 1.5 }}>
              👋🏻
            </Typography>
            <Typography variant='body2'>Silahkan login menggunakan akun kamu yang sudah terdaftar.</Typography>
            {errorMessage.length > 0 || errorMessage !== '' ? (
              <Typography variant='body2' color='error'>
                {errorMessage}
              </Typography>
            ) : (
              <></>
            )}
          </Box>
          <form noValidate autoComplete='off' onSubmit={handleSubmit}>
            <TextField
              onChange={handleChange('email')}
              autoFocus
              fullWidth
              id='username'
              label='Username'
              sx={{ marginBottom: 4 }}
              error={isError}
            />
            <FormControl fullWidth>
              <InputLabel htmlFor='auth-login-password'>Password</InputLabel>
              <OutlinedInput
                label='Password'
                value={values.password}
                id='auth-login-password'
                onChange={handleChange('password')}
                type={values.showPassword ? 'text' : 'password'}
                error={isError}
                endAdornment={
                  <InputAdornment position='end'>
                    <IconButton
                      edge='end'
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      aria-label='toggle password visibility'
                    >
                      {values.showPassword ? <EyeOutline /> : <EyeOffOutline />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
            <Box
              sx={{ mb: 4, display: 'flex', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'space-between' }}
            >
              <FormControlLabel control={<Checkbox />} label='Remember Me' />
              <Link passHref href='/'>
                <LinkStyled onClick={e => e.preventDefault()}>Lupa Password?</LinkStyled>
              </Link>
            </Box>
            <Button fullWidth size='large' type='submit' variant='contained' sx={{ marginBottom: 7 }}>
              Masuk
            </Button>
            <Box sx={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'center' }}>
              <Typography variant='body2' sx={{ marginRight: 2 }}>
                Mitra Baru?
              </Typography>
              <Typography variant='body2'>
                <Link passHref href='/pages/register'>
                  <LinkStyled>Daftar Sekarang!</LinkStyled>
                </Link>
              </Typography>
            </Box>
          </form>
        </CardContent>
      </Card>
      <FooterIllustrationsV1 />
    </Box>
  )
}

LoginPage.getLayout = (page: ReactNode) => <BlankLayout>{page}</BlankLayout>

export default LoginPage
