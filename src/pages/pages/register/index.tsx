import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useState, ReactNode, useCallback } from 'react'

import BlankLayout from 'src/@core/layouts/BlankLayout'
import FooterIllustrationsV1 from 'src/views/pages/auth/FooterIllustration'
import Link from 'next/link'
import { useRouter } from 'next/router'

// ** MUI Components
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'
import { styled, useTheme } from '@mui/material/styles'
import MuiCard, { CardProps } from '@mui/material/Card'
import MuiFormControlLabel, { FormControlLabelProps } from '@mui/material/FormControlLabel'

// ** Styled Components
const Card = styled(MuiCard)<CardProps>(({ theme }) => ({
  [theme.breakpoints.up('sm')]: { width: '28rem' }
}))

const LinkStyled = styled('a')(({ theme }) => ({
  fontSize: '0.875rem',
  textDecoration: 'none',
  color: theme.palette.primary.main
}))

const FormControlLabel = styled(MuiFormControlLabel)<FormControlLabelProps>(({ theme }) => ({
  marginTop: theme.spacing(1.5),
  marginBottom: theme.spacing(4),
  '& .MuiFormControlLabel-label': {
    fontSize: '0.875rem',
    color: theme.palette.text.secondary
  }
}))

const Register = () => {
  const supabase = useSupabaseClient()
  const [form, setForm] = useState({
    companyCode: ''
  })
  const [disableButton, setDisableButton] = useState(true)
  const [errors, setErrors] = useState<string[]>([])
  const router = useRouter()

  const fetchCompanyByCode = useCallback(
    async (e: any) => {
      e.preventDefault()
      setDisableButton(true)
      const { count, error } = await supabase
        .from('companies')
        .select('*', { count: 'exact' })
        .eq('code', form.companyCode)

      if (error) {
        setErrors([...errors, error.message])
        setDisableButton(true)
      }

      if (Number(count) > 0) {
        router.push(`/pages/register/${form.companyCode}`)
      } else {
        setErrors([...errors, 'Kode perusahaan tidak ditemukan'])
        setDisableButton(true)
      }
    },
    [supabase, form, router]
  )

  return (
    <Box className='content-center'>
      <Card sx={{ zIndex: 1 }}>
        <CardContent sx={{ padding: theme => `${theme.spacing(12, 9, 7)} !important` }}>
          <Box sx={{ mb: 6 }}>
            <Typography variant='h5' sx={{ fontWeight: 600, marginBottom: 1.5 }}>
              Daftar Sebagai Mitra 🚀
            </Typography>
          </Box>
          <form noValidate autoComplete='off' onSubmit={fetchCompanyByCode}>
            <TextField
              error={errors.length >= 1}
              value={form.companyCode}
              onChange={(e: any) => {
                setDisableButton(false)
                setForm({
                  companyCode: e.target.value
                })
              }}
              helperText={errors.join(', ')}
              autoFocus
              fullWidth
              id='username'
              label='Kode Brand atau Perusahaan'
              sx={{ marginBottom: 4 }}
            />
            <Button
              disabled={disableButton}
              fullWidth
              size='large'
              type='submit'
              variant='contained'
              sx={{ marginBottom: 7 }}
            >
              Lanjut
            </Button>
            <Box sx={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'center' }}>
              <Typography variant='body2' sx={{ marginRight: 2 }}>
                Sudah punya akun sebelumnya?
              </Typography>
              <Typography variant='body2'>
                <Link passHref href='/pages/login'>
                  <LinkStyled>Login Akun</LinkStyled>
                </Link>
              </Typography>
            </Box>
          </form>
        </CardContent>
      </Card>
      <FooterIllustrationsV1 />
    </Box>
  )
}

Register.getLayout = (page: ReactNode) => <BlankLayout>{page}</BlankLayout>

export default Register
