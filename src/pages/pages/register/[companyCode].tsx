// ** React Imports
import { useCallback, useState, useEffect, Fragment, ChangeEvent, MouseEvent, ReactNode } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** MUI Components
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Divider from '@mui/material/Divider'
import Checkbox from '@mui/material/Checkbox'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import InputLabel from '@mui/material/InputLabel'
import IconButton from '@mui/material/IconButton'
import CardContent from '@mui/material/CardContent'
import FormControl from '@mui/material/FormControl'
import FormHelperText from '@mui/material/FormHelperText'
import OutlinedInput from '@mui/material/OutlinedInput'
import { styled, useTheme } from '@mui/material/styles'
import MuiCard, { CardProps } from '@mui/material/Card'
import InputAdornment from '@mui/material/InputAdornment'
import MuiFormControlLabel, { FormControlLabelProps } from '@mui/material/FormControlLabel'
import { Select, MenuItem } from '@mui/material'

// ** Icons Imports
import EyeOutline from 'mdi-material-ui/EyeOutline'
import EyeOffOutline from 'mdi-material-ui/EyeOffOutline'

// ** Configs
import themeConfig from 'src/configs/themeConfig'

// ** Layout Import
import BlankLayout from 'src/@core/layouts/BlankLayout'

// ** Demo Imports
import FooterIllustrationsV1 from 'src/views/pages/auth/FooterIllustration'
import { useSupabaseAdmin } from 'src/@core/hooks/useSupabaseAdmin'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useFormValidation } from 'src/@core/hooks/useForm'
import { useHandleChange } from 'src/@core/hooks/useHandleChange'
import { useRouter } from 'next/router'
import { findIsError, findErrorProperty } from 'src/@core/utils/validateSync'

interface State {
  email: string
  fullname: string
  password: string
  phone: string
  showPassword: boolean
  role_id: any
}

// ** Styled Components
const Card = styled(MuiCard)<CardProps>(({ theme }) => ({
  [theme.breakpoints.up('sm')]: { width: '28rem' }
}))

const LinkStyled = styled('a')(({ theme }) => ({
  fontSize: '0.875rem',
  textDecoration: 'none',
  color: theme.palette.primary.main
}))

const FormControlLabel = styled(MuiFormControlLabel)<FormControlLabelProps>(({ theme }) => ({
  marginTop: theme.spacing(1.5),
  marginBottom: theme.spacing(4),
  '& .MuiFormControlLabel-label': {
    fontSize: '0.875rem',
    color: theme.palette.text.secondary
  }
}))

const RegisterPage = () => {
  // ** States
  const [values, setValues] = useState<State>({
    fullname: '',
    phone: '',
    email: '',
    password: '',
    role_id: null as any,
    showPassword: false
  })
  const supabase = useSupabaseClient()
  const supabaseAdmin = useSupabaseAdmin()
  const [roles, setRoles] = useState<any[]>([])
  const { runValidation, errors } = useFormValidation()
  const handleChange = useHandleChange(setValues, values)
  const router = useRouter()
  const [company, setCompany] = useState<any>({})
  const [isError, setIsError] = useState(false)
  const [errorsValue, setErrorsValue] = useState({
    fullname: '',
    phone: '',
    email: '',
    password: '',
    role_id: null as any,
    showPassword: false
  })

  // ** Hook
  const theme = useTheme()

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword })
  }
  const handleMouseDownPassword = (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
  }

  const fetchRole = useCallback(async () => {
    const { data } = await supabase.from('roles').select('id, name').eq('type', 'Partner')

    setRoles(data as any)
  }, [supabase, setRoles])

  const fetchCompany = useCallback(async () => {
    const { data } = await supabase.from('companies').select('*').eq('code', router.query.companyCode).single()

    setCompany(data as any)
  }, [supabase, router])

  const buildNoMitra = useCallback(async () => {
    const { count } = await supabase
      .from('profiles')
      .select('id', { count: 'exact' })
      .in(
        'role_id',
        roles.map((role: any) => role.id)
      )

    const formattedCount = new Intl.NumberFormat(`en-US`, { minimumIntegerDigits: 3 }).format(Number(count) + 1)

    return formattedCount
  }, [supabase])

  const handleSubmit = useCallback(
    async (e: any) => {
      e.preventDefault()
      setIsError(false)
      setErrorsValue({
        fullname: '',
        phone: '',
        email: '',
        password: '',
        role_id: null as any,
        showPassword: false
      })

      if (values.password.length < 6) {
        setIsError(true)
        setErrorsValue({
          ...errorsValue,
          password: 'Password harus lebih dari 6 karakter'
        })

        return
      }

      const {
        data: { user },
        error: userError
      } = await supabaseAdmin.auth.admin.createUser({
        email: values.email + '@invoice-app.com',
        password: values.password,
        email_confirm: true
      })

      if (userError) {
        if (userError.message === 'A user with this email address has already been registered') {
          setIsError(true)
          setErrorsValue({
            ...errorsValue,
            email: 'Username sudah terdaftar'
          })

          return
        }

        return
      }

      const code = await buildNoMitra()
      const { data, error } = await supabase.from('profiles').insert({
        code,
        phone: values.phone,
        fullname: values.fullname,
        role_id: values.role_id,
        uid: user?.id,
        company_id: company?.id
      })

      router.push('/pages/login')
    },
    [supabase, supabaseAdmin, runValidation, values]
  )

  useEffect(() => {
    fetchRole()
  }, [])

  useEffect(() => {
    if (router.query?.companyCode) {
      fetchCompany()
    }
  }, [router, router.query])

  return (
    <Box className='content-center'>
      <Card sx={{ zIndex: 1 }}>
        <CardContent sx={{ padding: theme => `${theme.spacing(12, 9, 7)} !important` }}>
          <Box sx={{ mb: 6 }}>
            <Typography variant='h5' sx={{ fontWeight: 600, marginBottom: 1.5 }}>
              Daftar Sebagai Mitra 🚀
            </Typography>
          </Box>
          <form noValidate autoComplete='off' onSubmit={handleSubmit}>
            <TextField
              autoFocus
              fullWidth
              id='fullname'
              label='Nama Lengkap'
              onChange={handleChange('fullname')}
              helperText={errorsValue.fullname}
              sx={{ marginBottom: 4 }}
              error={isError}
            />
            <TextField
              autoFocus
              fullWidth
              id='username'
              label='Username'
              onChange={handleChange('email')}
              helperText={errorsValue.email}
              error={isError}
              sx={{ marginBottom: 4 }}
            />
            <TextField
              fullWidth
              label='No HP'
              sx={{ marginBottom: 4 }}
              onChange={handleChange('phone')}
              error={isError}
              helperText={errorsValue.phone}
            />

            <FormControl fullWidth sx={{ marginBottom: 4 }}>
              <InputLabel>Tipe Mitra *</InputLabel>
              <Select
                labelId='demo-simple-select-label'
                id='demo-simple-select'
                fullWidth
                label='Tipe Mitra *'
                value={values.role_id}
                error={isError}
                onChange={handleChange('role_id')}
              >
                {roles.map(role => (
                  <MenuItem key={role.id} value={role.id}>
                    {role.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl fullWidth>
              <InputLabel htmlFor='auth-register-password'>Password</InputLabel>
              <OutlinedInput
                label='Password'
                value={values.password}
                id='auth-register-password'
                onChange={handleChange('password')}
                error={isError}
                type={values.showPassword ? 'text' : 'password'}
                endAdornment={
                  <InputAdornment position='end'>
                    <IconButton
                      edge='end'
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      aria-label='toggle password visibility'
                    >
                      {values.showPassword ? <EyeOutline fontSize='small' /> : <EyeOffOutline fontSize='small' />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              <FormHelperText>{errorsValue.password}</FormHelperText>
            </FormControl>
            <Button fullWidth size='large' type='submit' variant='contained' sx={{ marginY: 7 }}>
              Sign up
            </Button>
          </form>
        </CardContent>
      </Card>
      <FooterIllustrationsV1 />
    </Box>
  )
}

RegisterPage.getLayout = (page: ReactNode) => <BlankLayout>{page}</BlankLayout>

export default RegisterPage
