import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useRouter } from 'next/router'
import React, { ReactNode, useCallback, useEffect } from 'react'
import { useAuthGuard } from 'src/@core/hooks/useAuthGuard'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { useProfileUsingContext } from 'src/@core/hooks/useProfile'
import BlankLayout from 'src/@core/layouts/BlankLayout'

function Hub() {
    const { startLoading, stopLoading } = useLoadingUsingContext()
    const router = useRouter()
    const supabase = useSupabaseClient()

    const handleRedirect = useCallback(async () => {
        const {
            data: { session }
        } = await supabase.auth.getSession()

        if (!session) {
            router.push('/pages/login')

            return
        }

        const { data: profile } = await supabase
            .from('profiles')
            .select('*, roles(id, name, type), companies(*)')
            .eq('uid', session?.user.id)
            .single()

        if (profile.roles.type === 'Partner') {
            window.location.href = `/invoices/${profile?.id}/invoices`
        } else if (['Owner', 'Admin'].includes(profile?.roles.name)) {
            window.location.href = '/invoices'
        } else {
            window.location.href = '/dashboard'
        }

    }, [router, useAuthGuard, supabase])

    useEffect(() => {
        startLoading()
        handleRedirect().then(() => {
            stopLoading()
        })
    }, [])

    return (
        <></>
    )
}

Hub.getLayout = (page: ReactNode) => {

    return (
        <>
            <h3>Please wait...</h3>
            {page}
        </>
    )
}

export default Hub