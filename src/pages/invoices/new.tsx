import { Grid } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { IsNotEmpty, IsOptional, isNotEmpty } from 'class-validator'
import { useCallback, useState, useEffect } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import InvoiceForm from 'src/views/forms/InvoiceForm'
import dayjs from 'dayjs'
import { useRouter } from 'next/router'

export class InvoiceDetail {
  id?: string
  product_id: number
  total_price: number
  qty: number
  product?: any
  isDeleted?: boolean
}

export class InvoiceMerge {
  id?: string
  from_invoice_id: number
  remaining_amount: number
  description?: string
  isDeleted?: boolean
  from_invoice?: any
}

export class InvoicePayment {
  id?: string
  amount: number
  bank_account_name?: string
  payment_method?: string
  paid_at?: any
  note?: string
  isDeleted?: boolean
}

export class InvoiceReturn {
  id?: string
  product_id: number
  qty: number
  total_price: number
  note?: string
  product?: any
  isDeleted?: boolean
}

export class InvoiceFormSchema {
  id?: string

  @IsNotEmpty()
  profile_id: string

  profile?: any

  @IsNotEmpty()
  invoice_status_id: number

  @IsOptional()
  total_amount: number

  @IsOptional()
  remaining_amount: number

  @IsOptional()
  code: string

  @IsOptional()
  invoice_number: string

  @IsOptional()
  subtotal_product: number

  @IsOptional()
  subtotal_merges: number

  @IsOptional()
  subtotal_return: number

  @IsOptional()
  payment_total: number

  @IsOptional()
  invoice_date: any

  invoice_details: InvoiceDetail[]
  invoice_merges: InvoiceMerge[]
  invoice_payments: InvoicePayment[]
  invoice_returns: InvoiceReturn[]
}

const CreateInvoice = () => {
  const router = useRouter()
  const supabase = useSupabaseClient()
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const [previousInvoicePayment, setPreviousInvoicePayment] = useState<any[]>([])
  const [form, setForm] = useState<InvoiceFormSchema>({
    profile_id: null as any,
    profile: null,
    code: '',
    invoice_status_id: 0,
    invoice_number: '',
    total_amount: 0,
    remaining_amount: 0,
    invoice_merges: [],
    invoice_returns: [],
    invoice_details: [],
    invoice_payments: [],
    subtotal_merges: 0,
    subtotal_product: 0,
    payment_total: 0,
    subtotal_return: 0,
    invoice_date: dayjs()
  })
  const query = router.query

  const saveNotification = useCallback(
    async (invoice: any) => {
      const title = 'Kamu Memiliki Nota Baru'
      const body = `Nota ${invoice.invoice_number} sudah diterbitkan untukmu!`

      await supabase.from('inboxes').insert({
        profile_id: invoice.profile_id,
        title,
        body,
        action: {
          type: 'selling',
          id: invoice.id
        }
      })
    },
    [supabase]
  )

  const findPreviousInvoice = useCallback(
    async profile_id => {
      const { data } = await supabase
        .from('invoices')
        .select('id, remaining_amount')
        .eq('invoice_status_id', 3)
        .eq('profile_id', profile_id)
        .lt('remaining_amount', 0)

      setPreviousInvoicePayment(data as any)
    },
    [supabase, form]
  )

  const handleSave = useCallback(
    async (form: InvoiceFormSchema) => {
      const total_amount = form.subtotal_product + form.subtotal_merges - form.subtotal_return - form.payment_total

      let invoice_status_id = 1
      console.log(form.payment_total, total_amount)
      if (form.payment_total >= form.subtotal_product + form.subtotal_merges - form.subtotal_return)
        invoice_status_id = 3

      const { data: invoice } = await supabase
        .from('invoices')
        .insert({
          profile_id: form.profile_id,
          invoice_status_id,
          total_amount,
          remaining_amount: total_amount,
          code: `INV-${Date.now()}-${form.invoice_number}`,
          invoice_number: form.invoice_number,
          invoice_date: dayjs(form.invoice_date).toDate(),
          subtotal_product: form.subtotal_product,
          subtotal_return: form.subtotal_return,
          subtotal_merge: form.subtotal_merges,
          paid_amount: form.payment_total
        })
        .select()

      const invoiceId = (invoice as any)[0].id

      const ids = previousInvoicePayment.map(p => p?.id)

      await supabase
        .from('invoices')
        .update({
          remaining_amount: 0
        })
        .in('id', ids)

      await Promise.all([
        supabase.from('invoice_details').insert(
          form.invoice_details.map((detail: any) => ({
            invoice_id: invoiceId,
            product_id: detail.product_id,
            qty: detail.qty,
            total_price: detail.total_price
          }))
        ),
        supabase
          .from('invoice_merges')
          .insert(
            form.invoice_merges.map((merge: any) => ({
              from_invoice_id: merge.from_invoice_id,
              to_invoice_id: invoiceId,
              remaining_amount: merge.remaining_amount,
              description: merge.description
            }))
          )
          .select()
          .then(async ({ data }) => {
            data = data ?? []
            const ids = data.map(d => d.from_invoice_id)
            await supabase
              .from('invoices')
              .update({
                invoice_status_id: 3
              })
              .in('id', ids)
          }),
        supabase.from('invoice_returns').insert(
          form.invoice_returns.map((returns: any) => ({
            product_id: returns.product_id,
            qty: returns.qty,
            total_price: returns.total_price,
            note: returns.note,
            invoice_id: invoiceId
          }))
        ),
        supabase.from('invoice_payments').insert(
          form.invoice_payments.map((payment: any) => ({
            invoice_id: invoiceId,
            amount: payment.amount,
            note: payment.note,
            bank_account_name: payment.bank_account_name,
            payment_method: payment.payment_method
          }))
        )
      ])

      saveNotification((invoice as any)[0])

      // await generatePdf(invoiceId)

      router.back()

      return
    },
    [supabase, previousInvoicePayment]
  )

  const onInitProfileId = async (profile_id: string) => {
    const { data: newValue } = await supabase.from('profiles').select('*').eq('id', profile_id).single()

    const { count } = await supabase.from('invoices').select('id', { count: 'exact' }).eq('profile_id', profile_id)

    const invoice_number = new Intl.NumberFormat(`en-US`, { minimumIntegerDigits: 3 }).format(Number(count) + 1)

    await findPreviousInvoice(profile_id)

    setForm({
      ...form,
      profile_id,
      profile: newValue,
      invoice_number,
      payment_total: previousInvoicePayment as any
    })

    return
  }

  useEffect(() => {
    const profile_id = (router.query?.profileId as string) ?? (null as any)

    if (profile_id) {
      onInitProfileId(profile_id)
    }
  }, [router.query])

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <InvoiceForm
            title='Form Nota Penjualan'
            save={handleSave}
            form={form}
            previousPayment={previousInvoicePayment}
            disabledPartner={isNotEmpty(form.profile_id)}
          />
        </Grid>
      </Grid>
    </>
  )
}

export default CreateInvoice
