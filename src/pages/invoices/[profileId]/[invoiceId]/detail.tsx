import { isNotEmpty } from 'class-validator'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useState, useEffect, useCallback } from 'react'
import { LockClock, TrashCanOutline } from 'mdi-material-ui'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'
import { useRouter } from 'next/router'
import { Button, Chip, Grid, Card, CardContent, CardHeader, CardActions, Typography, Box } from '@mui/material'
import DetailCardHeader from 'src/views/DetailCardHeader'
import FloatingShareButton from 'src/@core/components/FloatingShareButton'
import InvoiceTemplate from 'src/views/pdfs/InvoiceTemplate'
import { isEmpty } from 'class-validator'
import { useApiUrl } from 'src/@core/hooks/useApiUrl'
import ConfirmationDialog from 'src/@core/components/ConfirmationDialog'
import { supabase as supabasee } from 'src/@core/utils/supabase'

export async function getServerSideProps(ctx: any) {
  const { invoiceId } = ctx.params
  const fetchInvoice = async () => {
    const { data: invoice } = await supabasee
      .from('invoices')
      .select(
        '*, profiles(id, fullname, code, phone, address), invoice_statuses(id,name)'
      )
      .eq('id', invoiceId)
      .single()

    return invoice
  }

  const fetchInvoiceDetails = async () => {
    const { data: invoiceDetails } = await supabasee
      .from('invoice_details')
      .select('*, products(id,name,sku)')
      .eq('invoice_id', invoiceId)
      .order('id', { ascending: true })

    return invoiceDetails
  }

  const fetchInvoicePayments = async () => {
    const { data: invoicePayments } = await supabasee
      .from('invoice_payments')
      .select('*')
      .eq('invoice_id', invoiceId)
      .order('id', { ascending: true })

    return invoicePayments
  }

  const fetchInvoiceMerges = async () => {
    const { data: invoiceMerges } = await supabasee
      .from('invoice_merges')
      .select('*, invoices:from_invoice_id(id, code, invoice_number, invoice_date)')
      .eq('to_invoice_id', invoiceId)
      .order('id', { ascending: true })

    return invoiceMerges
  }

  const fetchInvoiceReturns = async () => {
    const { data: invoiceReturns } = await supabasee
      .from('invoice_returns')
      .select('*, products(id,name,sku)')
      .eq('invoice_id', invoiceId)
      .order('id', { ascending: true })

    return invoiceReturns
  }

  const [invoice, invoice_details, invoice_payments, invoice_merges, invoice_returns] = await Promise.all([fetchInvoice(), fetchInvoiceDetails(), fetchInvoicePayments(), fetchInvoiceMerges(), fetchInvoiceReturns()])

  const invoiceProps = {
    ...invoice,
    invoice_details,
    invoice_payments,
    invoice_merges,
    invoice_returns,
  }

  return {
    props: {
      invoiceId: ctx.params.invoiceId as string,
      invoice: invoiceProps
    }
  }
}

const DetailInvoice = ({ invoiceId, invoice }: { invoiceId: string, invoice: any }) => {
  const supabase = useSupabaseClient()
  const { startLoading, stopLoading, setLoadingText } = useLoadingUsingContext()
  const router = useRouter()
  const profile = useProfile()
  const apiurl = useApiUrl()

  const isOwnedInvoice = useCallback(() => {
    if (profile?.id === invoice?.profile_id) {
      if (isEmpty(invoice?.read_at)) return true
    }

    return false
  }, [profile, invoice])

  const readInvoice = useCallback(async () => {
    if (isOwnedInvoice()) {
      await supabase
        .from('invoices')
        .update({
          read_at: new Date()
        })
        .eq('id', router.query.invoiceId)
    }
  }, [invoice, profile, router.query])


  useEffect(() => {
    readInvoice()
  }, [profile, invoice])

  const mapStatusColor = (status: string) => {
    switch (status) {
      case 'Nota Jalan':
        return 'success'
      case 'Nota Mati':
        return 'secondary'
      default:
        return 'error'
    }
  }

  const updateToNotaMati = useCallback(async () => {
    startLoading()
    const { data } = await supabase
      .from('invoices')
      .select('*, invoice_statuses(*)')
      .eq('id', router.query.invoiceId)
      .single()

    let updateToId = 2
    let status = data?.invoice_statuses.name
    if (data?.invoice_status_id === 2) {
      updateToId = 1
      status = 'Nota Jalan'
    }

    await Promise.all([
      supabase
        .from('invoices')
        .update({
          invoice_status_id: updateToId
        })
        .eq('id', router.query.invoiceId),
      supabase.from('inboxes').insert({
        profile_id: data?.profile_id,
        title: `Ada Perubahan Di Nota ${data?.invoice_number} Kamu!`,
        body: `Status Nota ${data?.invoice_number} Sudah Dirubah Menjadi ${status}.`,
        action: {
          id: router.query.invoiceId,
          type: 'selling'
        }
      })
    ])

    router.back()

    return
  }, [supabase])

  const handleDeleteInvoice = useCallback(async () => {
    try {
      startLoading()
      const { error } = await supabase.from('invoices').update({ deleted_at: new Date() }).eq('id', invoiceId)
      if (error) {
        throw new Error('error deleting data: ' + error.message)
      }

      router.back()
    } catch (error) {
      console.error(error)
    } finally {
      stopLoading()
    }

    stopLoading()
  }, [supabase, startLoading, stopLoading, invoiceId])

  return (
    <>
      <Grid container spacing={6} sx={{ marginBottom: '20px' }}>
        <Grid item xs={12}>
          <Card>
            <DetailCardHeader
              dontShowEdit={
                ['Penjahit', 'Reseller', 'Owner'].includes(profile?.roles.name)
              }
              editOnClick={() => router.push(`/invoices/${invoice?.profile_id}/${invoiceId}/edit`)}
            >
              {profile?.roles.name === 'Superadmin' ?
                <ConfirmationDialog
                  title='Yakin Hapus Nota?'
                  description='Nota yang sudah dihapus tidak bisa dikembalikan lagi, yakin hapus nota?'
                  action={handleDeleteInvoice}
                >
                  {(showDialog) => <Button color='error' onClick={showDialog}>
                    <TrashCanOutline />  Hapus
                  </Button>}
                </ConfirmationDialog> : <></>
              }
              {profile?.roles.name === 'Superadmin' &&
                (invoice?.invoice_status_id === 1 || invoice?.invoice_status_id === 2) ? (
                <Button variant='text' color='info' onClick={updateToNotaMati}>
                  <LockClock sx={{ color: 'info', marginRight: 1.5 }} />
                  Jadikan Nota {invoice?.invoice_status_id === 1 ? 'Mati' : 'Jalan'}
                </Button>
              ) : (
                <></>
              )}
            </DetailCardHeader>

            <CardContent>
              <Grid container spacing={6}>
                <Grid item xs={6}>
                  <Typography variant='subtitle2'>Tertagih Ke</Typography>
                  <Typography variant='h6'>{invoice?.profiles.fullname}</Typography>
                </Grid>

                <Grid
                  item
                  xs={6}
                  sx={{
                    textAlign: 'right'
                  }}
                >
                  <Chip
                    label={invoice?.invoice_statuses.name}
                    color={mapStatusColor(invoice?.invoice_statuses.name as any)}
                  />
                </Grid>

                <Grid item xs={6}>
                  <Typography variant='subtitle2'>Tgl Nota</Typography>
                  <Typography variant='h6'>{invoice?.invoice_date}</Typography>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>

        <Grid
          item
          xs={12}
          sx={{
            overflowX: 'hidden'
          }}
        >
          <InvoiceTemplate
            invoice={invoice}
            invoice_merges={invoice.invoice_merges}
            company={profile?.companies}
            margin='-30px 0px 0px'
            padding='0px 0px 25px'
            stampTop='80vh'
          />
        </Grid>
      </Grid>

      <FloatingShareButton
        handleClick={async () => {
          setLoadingText('Mohon ditunggu yaaa 🤗, kami sedang membuat file invoice kamu supaya bisa di download...')
          startLoading()
          const url = `${apiurl}/api/v1/invoice/${router.query?.invoiceId}/pdf`
          window.open(url, '_blank')
          stopLoading()
        }}
      />
    </>
  )
}

export default DetailInvoice
