import { ReactNode, useCallback, useEffect, useState } from 'react'
import BlankLayout from 'src/@core/layouts/BlankLayout'
import { supabase } from 'src/@core/utils/supabase'
import InvoiceTemplate from 'src/views/pdfs/InvoiceTemplate'

export const getServerSideProps = async (searchParams: any) => {
  const { invoiceId, print, status } = searchParams.query
  const fetchInvoice = async () => {
    const { data } = await supabase
      .from('invoices')
      .select(
        '*, profiles(id, fullname, code), invoice_statuses(id,name), invoice_payments(*), invoice_details(*, products(id,name,sku)), invoice_returns(*, products(id,name,sku))'
      )
      .eq('id', invoiceId)
      .single()

    return data
  }

  const fetchInvoiceMerges = async () => {
    const { data } = await supabase
      .from('invoice_merges')
      .select('*, invoices:from_invoice_id(id, code, invoice_number, invoice_date)')
      .eq('to_invoice_id', invoiceId)

    return data
  }

  const fetchCompany = async () => {
    const { data } = await supabase.from('companies').select('*').eq('id', 1).single()

    return data
  }

  const [invoice, invoiceMerges, company] = await Promise.all([fetchInvoice(), fetchInvoiceMerges(), fetchCompany()])

  return {
    props: {
      invoice,
      invoiceMerges,
      company,
      status: String(status),
      print: String(print ?? '') === 'true'
    }
  }
}

const InvoicePdf = (props: any) => {
  const { invoice, invoiceMerges, company, print: isPrintPage, status } = props

  useEffect(() => {
    ;(async () => {
      const res = await fetch(`/api/invoices/${invoice.id}/url`)
      const { publicUrl } = await res.json()

      window.open(publicUrl, '_blank')
    })()
  }, [])

  return (
    <>
      <InvoiceTemplate
        invoice={invoice}
        invoice_merges={invoiceMerges}
        company={company}
        stampLeft={'70vh'}
        stampTop={'15vw'}
      />
    </>
  )
}

InvoicePdf.getLayout = (page: ReactNode) => <BlankLayout>{page} </BlankLayout>

export default InvoicePdf
