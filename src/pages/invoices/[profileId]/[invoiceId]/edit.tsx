import { Grid } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { IsNotEmpty, IsOptional } from 'class-validator'
import { useCallback, useEffect, useState } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import InvoiceForm from 'src/views/forms/InvoiceForm'
import dayjs from 'dayjs'
import { useRouter } from 'next/router'
import { InvoiceFormSchema, InvoiceDetail, InvoiceMerge, InvoicePayment, InvoiceReturn } from 'src/pages/invoices/new'
import { isNotEmpty } from 'class-validator'

const EditInvoice = () => {
  const supabase = useSupabaseClient()
  const router = useRouter()
  const { invoiceId } = router.query
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const [form, setForm] = useState<InvoiceFormSchema>({
    profile_id: null as any,
    profile: null,
    code: '',
    invoice_status_id: 0,
    invoice_number: '',
    total_amount: 0,
    remaining_amount: 0,
    invoice_merges: [],
    invoice_returns: [],
    invoice_details: [],
    invoice_payments: [],
    subtotal_merges: 0,
    subtotal_product: 0,
    payment_total: 0,
    subtotal_return: 0,
    invoice_date: dayjs()
  })

  const fetchInvoice = useCallback(async () => {
    let { data } = await supabase
      .from('invoices')
      .select(
        '*, profiles(*), invoice_statuses(id,name), invoice_payments(*), invoice_details(*, products(*)), invoice_returns(*, products(*))'
      )
      .eq('id', invoiceId)
      .single()

    data = data as any

    const { data: merges } = await supabase
      .from('invoice_merges')
      .select('*, invoices:from_invoice_id(id, code, invoice_number)')
      .eq('to_invoice_id', invoiceId)

    const invoiceMerges = (merges as any[]).map(invoiceMerge => ({
      ...invoiceMerge,
      from_invoice: invoiceMerge.invoices
    }))

    const newForm = {
      id: invoiceId,
      profile_id: data?.profile_id,
      profile: data?.profiles,
      code: data?.code,
      invoice_date: dayjs(data.invoice_date),
      invoice_number: data?.invoice_number,
      invoice_status_id: data?.invoice_status_id,
      total_amount: data?.total_amount,
      remaining_amount: data?.remaining_amount,
      invoice_details: data?.invoice_details.map((invoiceDetail: any) => ({
        ...invoiceDetail,
        product: invoiceDetail.products
      })),
      invoice_payments: data?.invoice_payments,
      invoice_returns: data?.invoice_returns.map((invoiceReturn: any) => ({
        ...invoiceReturn,
        product: invoiceReturn.products
      })),
      subtotal_merges: data?.subtotal_merge,
      subtotal_product: data?.subtotal_product,
      payment_total: data?.paid_amount,
      subtotal_return: data?.subtotal_return,
      invoice_merges: invoiceMerges
    } as any

    setForm(newForm)
  }, [supabase, setForm, invoiceId])

  const saveInvoiceDetails = async (invoiceId: any, invoiceDetails: InvoiceDetail[]) => {
    await handleDeletedData('invoice_details', invoiceDetails)
    invoiceDetails = invoiceDetails.filter(d => !d.isDeleted)

    return Promise.all([
      invoiceDetails.map(
        async invoiceDetail =>
          await supabase.from('invoice_details').upsert({
            id: invoiceDetail?.id,
            invoice_id: invoiceId,
            product_id: invoiceDetail.product_id,
            qty: invoiceDetail.qty,
            total_price: invoiceDetail.total_price
          })
      )
    ])
  }

  const saveInvoicePayments = async (invoiceId: any, invoicePayments: InvoicePayment[]) => {
    await handleDeletedData('invoice_payments', invoicePayments)
    invoicePayments = invoicePayments.filter(d => !d.isDeleted)


    return Promise.all([
      invoicePayments.map(
        async invoicePayment =>
          await supabase.from('invoice_payments').upsert({
            id: invoicePayment?.id,
            invoice_id: invoiceId,
            amount: invoicePayment.amount,
            note: invoicePayment.note,
            paid_at: invoicePayment.paid_at,
            payment_method: invoicePayment.payment_method,
            bank_account_name: invoicePayment?.bank_account_name
          } as InvoicePayment)
      )
    ])
  }

  const saveInvoiceReturns = async (invoiceId: any, invoiceReturns: InvoiceReturn[]) => {
    await handleDeletedData('invoice_returns', invoiceReturns)
    invoiceReturns = invoiceReturns.filter(d => !d.isDeleted)

    return Promise.all([
      invoiceReturns.map(
        async invoiceReturn =>
          await supabase.from('invoice_returns').upsert({
            id: invoiceReturn?.id,
            invoice_id: invoiceId,
            note: invoiceReturn.note,
            product_id: invoiceReturn.product_id,
            total_price: invoiceReturn.total_price,
            qty: invoiceReturn.qty
          } as InvoiceReturn)
      )
    ])
  }

  const handleCancelInvoiceMerges = async (invoiceId: any, invoiceMerges: InvoiceMerge[]) => {
    return await Promise.all([
      invoiceMerges.map(async invoiceMerge => {
        // update status from nota menjadi nota jalan
        await supabase
          .from('invoices')
          .update({
            invoice_status_id: 1
          })
          .eq('id', invoiceMerge.from_invoice_id)

        return
      })
    ])
  }

  const saveInvoiceMerges = async (invoiceId: any, invoiceMerges: InvoiceMerge[]) => {
    await handleDeletedData('invoice_merges', invoiceMerges)
    invoiceMerges = invoiceMerges.filter(d => !d?.isDeleted)

    const ids = invoiceMerges.map((d: any) => d.from_invoice_id)

    await supabase
      .from('invoices')
      .update({
        invoice_status_id: 3
      })
      .in('id', ids)

    return await Promise.all([
      invoiceMerges.map(async invoiceMerge => {
        let { data }: any = await supabase.from('invoice_merges').upsert({
          id: invoiceMerge?.id,
          to_invoice_id: Number(invoiceId),
          from_invoice_id: invoiceMerge.from_invoice_id,
          remaining_amount: invoiceMerge.remaining_amount,
          description: invoiceMerge.description
        })

        data = data ?? []
      })
    ])
  }

  const updateAfterMerged = async (payload: any) => {
    const { new: invoiceMerge } = payload
    const { from_invoice_id } = invoiceMerge

    await supabase
      .from('invoices')
      .update({
        invoice_status_id: 3
      })
      .eq('id', from_invoice_id)
  }

  const findIfItsMerged = async (payload: any) => {
    const { new: invoicePayment } = payload
    const { invoice_id, amount } = invoicePayment

    const { data } = await supabase
      .from('invoice_merges')
      .select('id, to_invoice_id, remaining_amount')
      .eq('from_invoice_id', invoice_id)
      .single()

    if (data) {
      const { data: invoice } = await supabase.from('invoices').select('*').eq('id', data?.to_invoice_id).single()

      const remaining_amount = invoice.remaining_amount - amount

      let invoice_status_id = invoice.invoice_status_id

      if (remaining_amount <= 0) invoice_status_id = 3

      await supabase
        .from('invoices')
        .update({
          invoice_status_id,
          remaining_amount
        })
        .eq('id', invoice.id)
    }

    supabase.channel('invoice_payments').unsubscribe()
  }

  const handleDeletedData = useCallback(
    async (tableName: string, data: InvoiceDetail[] | InvoiceMerge[] | InvoiceReturn[] | InvoicePayment[]) => {
      const deleted = (data as any[]).filter((d: any) => d.isDeleted && isNotEmpty(d?.id))

      if (deleted.length !== 0) {
        return await Promise.all(deleted.map((d: any) => supabase.from(tableName).delete().eq('id', d.id)))
      }

      return
    },
    [supabase]
  )

  const handleSave = useCallback(
    async (form: InvoiceFormSchema) => {
      const total_amount =
        Number(form.subtotal_product) +
        Number(form.subtotal_merges) -
        Number(form.subtotal_return) -
        Number(form.payment_total)

      let invoice_status_id = form.invoice_status_id
      if (
        form.payment_total >=
        Number(form.subtotal_product) + Number(form.subtotal_merges) - Number(form.subtotal_return)
      )
        invoice_status_id = 3

      await supabase
        .from('invoices')
        .update({
          profile_id: form.profile_id,
          invoice_status_id,
          total_amount,
          remaining_amount: total_amount,
          code: `INV-${Date.now()}-${form.invoice_number}`,
          invoice_number: form.invoice_number,
          invoice_date: dayjs(form.invoice_date).format('YYYY-MM-DD'),
          subtotal_product: form.subtotal_product,
          subtotal_return: form.subtotal_return,
          subtotal_merge: form.subtotal_merges,
          paid_amount: form.payment_total
        })
        .eq('id', invoiceId)

      await saveInvoiceMerges(invoiceId, form.invoice_merges),
        await Promise.all([
          saveInvoiceDetails(invoiceId, form.invoice_details),
          saveInvoicePayments(invoiceId, form.invoice_payments),

          saveInvoiceReturns(invoiceId, form.invoice_returns),
          handleCancelInvoiceMerges(
            invoiceId,
            form.invoice_merges.filter(d => d?.isDeleted)
          )
        ])

      supabase.channel('invoices').unsubscribe()
      supabase.channel('invoice_merges').unsubscribe()
      await generatePdfAfterEdit(invoiceId as any)

      router.back()

      return
    },
    [supabase]
  )

  const generatePdfAfterEdit = async (id: number) => {
    const { data } = await supabase.from('invoices').select('*, invoice_statuses(*)').eq('id', id).single()

    return fetch(`/api/invoices/${id}/pdf?status=${data?.name}`).then(response => response.status)
  }

  useEffect(() => {
    startLoading()
    fetchInvoice().then(() => {
      stopLoading()
    })

    supabase
      .channel('invoice_payments')
      .on(
        'postgres_changes',
        {
          event: 'INSERT',
          schema: 'public',
          table: 'invoice_payments'
        },
        findIfItsMerged
      )
      .subscribe()

    supabase.channel('invoice_merges').on(
      'postgres_changes',
      {
        event: 'INSERT',
        schema: 'public',
        table: 'invoice_merges'
      },
      updateAfterMerged
    )
  }, [])

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <InvoiceForm title='Form Nota Penjualan' save={handleSave} form={form} disabledPartner />
        </Grid>
      </Grid>
    </>
  )
}

export default EditInvoice
