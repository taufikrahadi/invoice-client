import { Grid, Box, Tabs, Tab, Typography, TextField, InputAdornment } from '@mui/material'
import { TextSearch } from 'mdi-material-ui'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useRouter } from 'next/router'
import { useCallback, useEffect, useState } from 'react'
import FloatingAddButton from 'src/@core/components/FloatingAddButton'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import Loading from 'src/views/Loading'
import { useInvoiceStatusList } from 'src/modules/invoice-status'
import FilterAndSortSection from 'src/layouts/components/FilterAndSortSection'
import { isNotEmpty } from 'class-validator'
import InvoiceItem from 'src/views/invoice/InvoiceItem'
import { useProfileUsingContext as useProfile } from 'src/@core/hooks/useProfile'
import StatisticsCard from 'src/views/dashboard/StatisticsCard'

export function getServerSideProps(context: any) {
  return {
    props: {
      profileId: context.params.profileId
    }
  }
}

const ListInvoice = ({ profileId }: { profileId: string }) => {
  const router = useRouter()
  const supabase = useSupabaseClient()
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const [tabValue, setTabValue] = useState(0)
  const invoiceStatuses = useInvoiceStatusList()
  const [searchTerm, setSearchTerm] = useState<any>(null)
  const [invoices, setInvoices] = useState<any[]>([])
  const [invoiceCount, setInvoiceCount] = useState<number>(0)
  const [sortOrder, setSortOrder] = useState<'ASC' | 'DESC'>('DESC')
  const [selectedStatus, setSelectedStatus] = useState<string>('Nota')
  const profile = useProfile()
  const [isShowStatisticCard, setIsShowStatisticCard] = useState(true)

  const fetchInvoices = useCallback(async () => {
    startLoading()
    const query = supabase
      .from('invoices')
      .select('*, profiles(id, fullname, code), invoice_statuses(id, name)', {
        count: 'exact'
      })
      .order('invoice_number', {
        ascending: false
      })
      .is('deleted_at', null)
      .eq('profile_id', profileId)

    if (tabValue > 0) {
      query.eq('invoice_status_id', tabValue)
      setSelectedStatus(invoiceStatuses.find((s: any) => s.id === tabValue).name as any)
    } else {
      setSelectedStatus('Nota')
    }

    if (['Reseller', 'Penjahit'].includes(profile?.roles.name as any)) {
      query.eq('profile_id', profile?.id)
    }

    if (isNotEmpty(searchTerm)) query.ilike('invoice_number', searchTerm)

    const { data, count } = await query.order('invoice_date', { ascending: sortOrder === 'ASC' })
    setInvoices(data as any)
    setInvoiceCount(count ?? 0)
    stopLoading()
  }, [supabase, setInvoices, tabValue, searchTerm, setSelectedStatus, profileId])

  useEffect(() => {
    // if (profile?.roles.name !== 'Superadmin') setIsShowStatisticCard(true)

    fetchInvoices()
  }, [router.query.profileId, profile, setIsShowStatisticCard])

  useEffect(() => {
    fetchInvoices()
  }, [tabValue, searchTerm, sortOrder, profile])

  useEffect(() => {
    const { refresh } = router.query

    if (refresh) fetchInvoices()
  }, [router.query])

  return (
    <>
      <Grid container spacing={6} sx={{ marginBottom: '20px' }}>
        {isShowStatisticCard ? (
          <Grid item xs={12}>
            <StatisticsCard profileId={router.query.profileId as string} />
          </Grid>
        ) : (
          <></>
        )}

        <Grid item xs={12}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: '10px'
            }}
          >
            <TextField
              variant='outlined'
              fullWidth
              placeholder='Cari...'
              value={searchTerm}
              onChange={(e: any) => setSearchTerm(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position='start'>
                    <TextSearch />
                  </InputAdornment>
                )
              }}
            />
          </Box>
        </Grid>

        <Grid item xs={12}>
          <Box
            sx={{
              borderBottom: 1,
              borderColor: 'primary',
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Tabs
              value={tabValue}
              onChange={(_: any, newValue: number) => {
                setTabValue(newValue)
              }}
            >
              <Tab label='Semua' />
              {invoiceStatuses.map((invoiceStatus: any) => {
                return <Tab key={invoiceStatus?.seq} label={invoiceStatus?.name} />
              })}
            </Tabs>
          </Box>
        </Grid>

        <Grid item xs={12} sx={{ textAlign: 'center', marginY: '20px' }}>
          <Typography variant='body2' color='secondary'>
            {invoiceCount} {selectedStatus}
          </Typography>
        </Grid>

        {invoices.map((invoice: any) => (
          <Grid key={invoice?.id} item xs={12}>
            <InvoiceItem
              id={invoice?.id}
              partner_id={invoice?.profile_id}
              partner_name={invoice?.profiles?.fullname}
              status={invoice?.invoice_statuses?.name}
              remaining_amount={invoice?.remaining_amount}
              invoice_date={invoice?.invoice_date}
              invoice_number={invoice?.invoice_number}
              code={invoice?.code}
              read_at={invoice?.read_at}
            />
          </Grid>
        ))}
      </Grid>

      {['Superadmin', 'Admin'].includes(profile?.roles.name) ? (
        <FloatingAddButton handleClick={() => router.push(`/invoices/new?profileId=${router.query.profileId}`)} />
      ) : (
        <></>
      )}
    </>
  )
}

export default ListInvoice
