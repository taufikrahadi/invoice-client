import {
  Grid,
  Box,
  FormControl,
  TextField,
  InputAdornment,
  Dialog,
  MenuItem,
  Select,
  Button,
  Card,
  InputLabel,
  DialogTitle,
  DialogContent,
  SelectChangeEvent,
  DialogActions,
  CardContent,
  Typography,
  GridProps,
  Chip,
  CardActionArea
} from '@mui/material'
import { styled } from '@mui/material/styles'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { TextSearch } from 'mdi-material-ui'
import { useRouter } from 'next/router'
import { ChangeEvent, useCallback, useEffect, useState } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import { useListProductCategory } from 'src/modules/product-category'
import Loading from 'src/views/Loading'
import FloatingAddButton from 'src/@core/components/FloatingAddButton'
import { isNotEmpty } from 'class-validator'
import { useProfile } from 'src/@core/hooks/useProfile'
import DataCard from 'src/views/DataCard'

export const StyledGrid = styled(Grid)<GridProps>(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  [theme.breakpoints.down('md')]: {
    borderBottom: `1px solid ${theme.palette.divider}`
  },
  [theme.breakpoints.up('md')]: {
    borderRight: `1px solid ${theme.palette.divider}`
  }
}))

const Product = () => {
  const supabase = useSupabaseClient()
  const [categoryId, setCategoryId] = useState<any>(null)
  const [searchTerm, setSearchTerm] = useState('')
  const [currentRangePosition, setCurrentRangePosition] = useState<number>(0)
  const { stopLoading, startLoading } = useLoadingUsingContext()
  const [products, setProducts] = useState<any[]>([])
  const [sort, setSort] = useState<string>('ASC')
  const [totalProduct, setTotalProduct] = useState(0)
  const [offset, setOffset] = useState(0)
  const [isMaxOffset, setIsMaxOffset] = useState(false)
  const router = useRouter()
  const [showModalPosition, setShowModalPosition] = useState<'filter' | 'sort' | null>(null)
  const productCategories = useListProductCategory()
  const profile = useProfile()

  const fetchProducts = useCallback(
    async (search: string, categoryId: any, sortRule: string, isNeedToPush = false) => {
      startLoading()
      if (isMaxOffset) {
        stopLoading()

        return
      }

      const query = supabase
        .from('products')
        .select('*, product_categories (id, name)', { count: 'exact' })
        .is('deleted_at', null)
        .ilike('name', `%${search}%`)
        .order('name', { ascending: sortRule === 'ASC' })

      if (offset === 0) {
        query.range(0, 5)
      } else {
        query.range(offset + 1, offset + 5)
      }

      if (categoryId) {
        query.eq('category_id', categoryId)
      }

      const { data, count, error } = await query
      if (error?.code === 'PGRST103') {
        setIsMaxOffset(true)
        stopLoading()

        return
      }

      setTotalProduct(count ?? 0)
      if (isNeedToPush) {
        setProducts([...products, ...(data as any[])])
      } else {
        setProducts(data as any[])
      }

      if (offset <= (count as number)) {
        setOffset(offset + 5)
      }

      stopLoading()
    },
    [supabase, offset, setOffset, products]
  )

  useEffect(() => {
    fetchProducts(searchTerm, null, sort, true)
  }, [])

  useEffect(() => {
    setOffset(0)
    setIsMaxOffset(false)
    fetchProducts(searchTerm, categoryId, sort)
  }, [categoryId, searchTerm, sort])

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: '10px'
            }}
          >
            <TextField
              variant='outlined'
              fullWidth
              placeholder='Cari...'
              value={searchTerm}
              onChange={(e: ChangeEvent<HTMLInputElement>) => setSearchTerm(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position='start'>
                    <TextSearch />
                  </InputAdornment>
                )
              }}
            />
          </Box>

          <Grid item xs={12} container sx={{ marginY: '5px' }}>
            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
              <Button
                variant='outlined'
                onClick={() => {
                  setShowModalPosition('filter')
                }}
              >
                Filter
              </Button>

              <Button variant='outlined' onClick={() => setShowModalPosition('sort')}>
                Urutkan
              </Button>
            </Box>
          </Grid>

          <Grid item xs={12} sx={{ textAlign: 'center', marginY: '20px' }}>
            <Typography variant='body2' color='secondary'>
              {totalProduct} Produk
            </Typography>
          </Grid>

          <Grid item xs={12} container spacing={6} sx={{ marginY: '20px' }}>
            {products.map((product: any) => (
              <Grid key={product.id} item xs={12} md={6} lg={4}>
                <DataCard
                  name={product?.name}
                  code={product?.sku}
                  money={product?.selling_price}
                  categoryOrSomething={product?.product_categories?.name}
                  onClick={() => router.push(`/products/${product?.id}/view`)}
                />
              </Grid>
            ))}
          </Grid>

          <Grid item xs={12} container>
            <Button
              variant='contained'
              color='primary'
              onClick={() => fetchProducts(searchTerm, categoryId, sort, true)}
            >
              Lihat Lebih Banyak
            </Button>
          </Grid>
        </Grid>

        <Dialog open={showModalPosition === 'filter'} onClose={() => setShowModalPosition(null)}>
          <DialogTitle>Filter Produk</DialogTitle>

          <DialogContent>
            <FormControl fullWidth>
              <InputLabel>Kategori</InputLabel>
              <Select
                labelId='demo-simple-select-label'
                id='demo-simple-select'
                value={categoryId}
                label='Age'
                onChange={(e: SelectChangeEvent) => setCategoryId(e.target.value as string)}
              >
                {productCategories.map(productCategory => (
                  <MenuItem value={productCategory.id} key={productCategory.id}>
                    {productCategory.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </DialogContent>

          <DialogActions>
            <Button
              variant='contained'
              color='secondary'
              onClick={() => {
                setCategoryId(null)
                setShowModalPosition(null)
              }}
            >
              Reset
            </Button>
            <Button variant='contained' color='primary' onClick={() => setShowModalPosition(null)}>
              Terapkan
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog open={showModalPosition === 'sort'} onClose={() => setShowModalPosition(null)}>
          <DialogTitle>Urutkan</DialogTitle>

          <DialogContent>
            <FormControl fullWidth>
              <InputLabel>Urut Dari</InputLabel>
              <Select
                labelId='demo-simple-select-label'
                id='demo-simple-select'
                value={sort}
                label='Age'
                onChange={(e: SelectChangeEvent) => setSort(e.target.value as any)}
              >
                <MenuItem value={'ASC'}>A - Z</MenuItem>
                <MenuItem value={'DESC'}>Z - A</MenuItem>
              </Select>
            </FormControl>
          </DialogContent>

          <DialogActions>
            <Button
              variant='contained'
              color='secondary'
              onClick={() => {
                setSort('ASC')
                setShowModalPosition(null)
              }}
            >
              Reset
            </Button>
            <Button variant='contained' color='primary' onClick={() => setShowModalPosition(null)}>
              Terapkan
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>

      {['Superadmin', 'Owner'].includes(profile?.roles.name) ? (
        <FloatingAddButton handleClick={() => router.push('/products/new')} />
      ) : (
        <></>
      )}
    </>
  )
}

export default Product
