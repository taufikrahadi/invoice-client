import { Grid } from '@mui/material'
import { useState, useCallback, useEffect } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import ProductForm from 'src/views/forms/ProductForm'
import { IsOptional, IsString, IsNotEmpty, MinLength, IsNumber } from 'class-validator'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useRouter } from 'next/router'
import { Transform } from 'class-transformer'

export class ProductFormSchema {
  constructor(form: ProductFormSchema) {
    Object.assign(this, form)
  }

  id?: string

  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  name: string

  @IsNumber()
  @IsNotEmpty()
  category_id: number | string | null | any

  @IsString()
  @IsOptional()
  description: string

  @IsString()
  @IsNotEmpty()
  sku: string

  @Transform(({ value }) => Number(value))
  @IsNotEmpty()
  selling_price: number | null

  product_categories?: any
}

const NewProduct = (props?: { handleBack?: any }) => {
  const [form, setForm] = useState<ProductFormSchema>({
    name: '',
    category_id: undefined,
    description: '',
    sku: '',
    selling_price: 0
  })
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const supabase = useSupabaseClient()
  const router = useRouter()
  const [isSucces, setIsSuccess] = useState(false)
  const [totalProduct, setTotalProduct] = useState<number>(0)

  const buildSku = useCallback(async () => {
    let { count } = await supabase.from('products').select('id', { count: 'estimated' }).is('deleted_at', null)

    let formattedCount = new Intl.NumberFormat(`en-US`, { minimumIntegerDigits: 3 }).format(Number(count) + 1)

    const { count: anotherCount } = await supabase
      .from('products')
      .select('id', { count: 'estimated' })
      .is('deleted_at', null)
      .eq('sku', `SKU-${formattedCount}`)

    if (anotherCount) count = (count ?? 0) + 1

    formattedCount = new Intl.NumberFormat(`en-US`, { minimumIntegerDigits: 3 }).format(Number(count) + 1)
    const sku = `SKU-${formattedCount}`

    return sku
  }, [])

  const handleSaveData = useCallback(async (form: ProductFormSchema) => {
    await supabase.from('products').insert({
      ...form
    })

    setIsSuccess(true)
    stopLoading()
    if (props?.handleBack) {
      props?.handleBack()

      return
    }

    router.push('/products')
  }, [])

  useEffect(() => {
    startLoading()
    buildSku()
      .then((sku: string) => {
        setForm({
          ...form,
          sku
        })
      })
      .finally(() => {
        stopLoading()
      })
  }, [])

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <ProductForm save={handleSaveData} form={form} title='Buat Produk Baru' handleBack={props?.handleBack} />
        </Grid>
      </Grid>
    </>
  )
}

export default NewProduct
