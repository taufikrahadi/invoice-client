import { Grid, Box, Button, Typography, Card, CardContent, styled, BoxProps, Divider, CardHeader } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { isNotEmpty } from 'class-validator'
import { ArrowLeft, CurrencyUsd, DatabasePlus, Delete, Hanger, Pencil } from 'mdi-material-ui'
import { useRouter } from 'next/router'
import { useCallback, useEffect, useState } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import ConfirmDialog from 'src/views/ConfirmDialog'

const StyledBox = styled(Box)<BoxProps>(({ theme }) => ({
  [theme.breakpoints.up('sm')]: {
    borderRight: `1px solid ${theme.palette.divider}`
  }
}))

const DetailProduct = () => {
  const supabase = useSupabaseClient()
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const router = useRouter()
  const { id } = router.query
  const [product, setProduct] = useState<any>(null)
  const [showConfirmDelete, setShowConfirmDelete] = useState(false)

  const deleteProduct = useCallback(async () => {
    startLoading()
    await supabase
      .from('products')
      .update({
        deleted_at: new Date()
      })
      .eq('id', id)
    router.back()
    stopLoading()
  }, [])

  const fetchProduct = useCallback(async () => {
    startLoading()
    const { data, error } = await supabase
      .from('products')
      .select('*, product_categories(id, name)')
      .eq('id', id)
      .single()

    if (error) router.push('/500')

    stopLoading()
    setProduct({
      ...data
    })
  }, [])

  useEffect(() => {
    fetchProduct()
  }, [])

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <Card>
            <Box
              sx={{
                padding: '10px',
                display: 'flex',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <Button variant='text' onClick={() => router.back()} color='secondary'>
                <ArrowLeft />
              </Button>
              <Box>
                <Button variant='text' color='info' onClick={() => router.push(`/products/${id}/edit`)}>
                  <Pencil sx={{ color: 'info', marginRight: 1.5 }} />
                  Edit
                </Button>
                <Button variant='text' color='error' onClick={() => setShowConfirmDelete(true)}>
                  <Delete sx={{ color: 'error', marginRight: 1.5 }} />
                  Delete
                </Button>
              </Box>
            </Box>
            <Grid item xs={12}>
              <CardContent sx={{ padding: theme => `${theme.spacing(3.25, 5.75, 6.25)} !important` }}>
                <Typography variant='h6' sx={{ marginBottom: 3.5 }}>
                  {product?.name}
                </Typography>
                <Typography variant='body2'>{product?.sku}</Typography>
                <Divider sx={{ marginTop: 6.5, marginBottom: 6.75 }} />
                <Grid container spacing={4}>
                  <Grid item xs={12} sm={5}>
                    <StyledBox>
                      <Box sx={{ mb: 6.75, display: 'flex', alignItems: 'center' }}>
                        <CurrencyUsd sx={{ color: 'primary.main', marginRight: 2.75 }} fontSize='small' />
                        <Typography variant='body2'>
                          {new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(
                            Number(product?.selling_price ?? 0)
                          )}
                        </Typography>
                      </Box>
                      <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        <Hanger sx={{ color: 'primary.main', marginRight: 2.75 }} fontSize='small' />
                        <Typography variant='body2'>{product?.product_categories?.name ?? '-'}</Typography>
                      </Box>
                    </StyledBox>
                  </Grid>
                </Grid>
              </CardContent>
            </Grid>
          </Card>
        </Grid>

        {isNotEmpty(product?.description) ? (
          <Grid item xs={12}>
            <Card>
              <CardContent>
                <Typography variant='body2'>{product.description}</Typography>
              </CardContent>
            </Card>
          </Grid>
        ) : (
          <></>
        )}
      </Grid>

      <ConfirmDialog
        show={showConfirmDelete}
        setShow={setShowConfirmDelete}
        title='Hapus Data?'
        message='Yakin ingin menghapus produk? produk yang sudah dihapus tidak bisa dikembalikan lagi.'
        action={deleteProduct}
      />
    </>
  )
}

export default DetailProduct
