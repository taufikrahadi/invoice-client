import { Grid } from '@mui/material'
import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useRouter } from 'next/router'
import { useCallback, useEffect, useState } from 'react'
import { useLoadingUsingContext } from 'src/@core/hooks/useLoading'
import ProductForm from 'src/views/forms/ProductForm'

const EditProduct = () => {
  const router = useRouter()
  const { id } = router.query
  const [product, setProduct] = useState<any>({
    name: '',
    sku: '',
    selling_price: 0,
    category_id: 0,
    description: '',
    product_categories: null
  })
  const { startLoading, stopLoading } = useLoadingUsingContext()
  const supabase = useSupabaseClient()

  const fetchProduct = useCallback(async () => {
    startLoading()
    const { data } = await supabase.from('products').select('*, product_categories(id,name)').eq('id', id).single()
    setProduct({
      name: data.name,
      sku: data.sku,
      selling_price: data.selling_price,
      category_id: data?.category_id,
      description: data?.description,
      product_categories: data?.product_categories
    })
    stopLoading()
  }, [])

  const handleSave = useCallback(async (form: any) => {
    startLoading()
    delete form.product_categories
    await supabase
      .from('products')
      .update({
        ...form,
        updated_at: new Date()
      })
      .eq('id', id)

    stopLoading()
    router.push('/products')
  }, [])

  useEffect(() => {
    fetchProduct()
  }, [])

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <ProductForm
            form={{
              name: product?.name,
              sku: product?.sku,
              selling_price: product?.selling_price,
              category_id: product?.category_id,
              description: product?.description,
              product_categories: product?.product_categories
            }}
            title='Edit Produk'
            save={handleSave}
          />
        </Grid>
      </Grid>
    </>
  )
}

export default EditProduct
