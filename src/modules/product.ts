import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useCallback, useEffect, useState } from 'react'
import { BaseFetchOptions } from 'src/@core/interfaces/base-fetch-opts'

export const useProductList = (options: BaseFetchOptions, startLoading?: () => void, stopLoading?: () => void) => {
  const [products, setProducts] = useState<any>([])
  const [totalProduct, setTotalProduct] = useState<number | null>(0)
  const supabase = useSupabaseClient()

  const fetchProducts = useCallback(async () => {
    if (startLoading) startLoading()

    const query = supabase
      .from('products')
      .select('*, product_categories (id, name)', { count: 'exact' })
      .range(options.rangeStart, options.rangeEnd)

    if (options.search) query.ilike('name', `%${options.search}%`)

    // if (options.filters) {
    //   options.filters?.forEach(filter => {
    //     query[filter.operator](filter.columnName, filter.value)
    //   })
    // }
    //
    const { data, count } = await query
    setProducts(data)
    setTotalProduct(count)
    if (stopLoading) stopLoading()
  }, [supabase])

  useEffect(() => {
    fetchProducts()
  }, [])

  useEffect(() => {}, [options.search, options.filters, options.sort])

  return {
    products,
    totalProduct
  }
}
