import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useState, useEffect, useCallback } from 'react'
import { ROLEPARTNER } from 'src/pages/partners'

export const useListRole = (trigger?: boolean) => {
  const [roles, setRoles] = useState<any[]>([])
  const [roleCount, setRoleCount] = useState(0)
  const supabase = useSupabaseClient()
  const fetchRoles = useCallback(async () => {
    const { data, count } = await supabase
      .from('roles')
      .select('*', { count: 'exact' })
      .is('deleted_at', null)
      .eq('type', 'Partner')

    setRoles(data as any)
    setRoleCount(count as any)
  }, [supabase, setRoles])

  useEffect(() => {
    fetchRoles()
  }, [])

  useEffect(() => {
    fetchRoles()
  }, [trigger])

  return { roles, roleCount }
}
