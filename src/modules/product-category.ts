import { useCallback, useEffect, useState } from 'react'
import { useSupabaseClient } from '@supabase/auth-helpers-react'

export const useListProductCategory = (trigger?: boolean) => {
  const [productCategories, setProductCategories] = useState([])

  const supabase = useSupabaseClient()

  const fetchList = useCallback(async () => {
    const { data } = await supabase.from('product_categories').select().is('deleted_at', null)

    setProductCategories(data as any)
  }, [supabase])

  useEffect(() => {
    fetchList()
  }, [])

  useEffect(() => {
    fetchList()
  }, [trigger])

  return productCategories as any[]
}
