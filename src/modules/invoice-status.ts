import { useSupabaseClient } from '@supabase/auth-helpers-react'
import { useState, useCallback, useEffect } from 'react'

export const useInvoiceStatusList = () => {
  const supabase = useSupabaseClient()
  const [statuses, setStatuses] = useState<any>([])
  const fetchData = useCallback(async () => {
    const { data } = await supabase.from('invoice_statuses').select('*').order('seq', { ascending: true })

    setStatuses(data as any)
  }, [supabase])

  useEffect(() => {
    fetchData()
  }, [])

  return statuses
}
