export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export type Database = {
  public: {
    Tables: {
      inboxes: {
        Row: {
          action: Json | null
          body: string | null
          created_at: string
          id: number
          is_read: boolean
          profile_id: string
          title: string
          updated_at: string
        }
        Insert: {
          action?: Json | null
          body?: string | null
          created_at?: string
          id?: number
          is_read?: boolean
          profile_id: string
          title: string
          updated_at?: string
        }
        Update: {
          action?: Json | null
          body?: string | null
          created_at?: string
          id?: number
          is_read?: boolean
          profile_id?: string
          title?: string
          updated_at?: string
        }
        Relationships: [
          {
            foreignKeyName: "inboxes_profile_id_fkey"
            columns: ["profile_id"]
            isOneToOne: false
            referencedRelation: "profiles"
            referencedColumns: ["id"]
          }
        ]
      }
      invoice_details: {
        Row: {
          created_at: string
          id: number
          invoice_id: number
          product_id: number
          qty: number
          total_price: number
          updated_at: string
        }
        Insert: {
          created_at?: string
          id?: number
          invoice_id: number
          product_id: number
          qty: number
          total_price: number
          updated_at?: string
        }
        Update: {
          created_at?: string
          id?: number
          invoice_id?: number
          product_id?: number
          qty?: number
          total_price?: number
          updated_at?: string
        }
        Relationships: [
          {
            foreignKeyName: "invoice_details_invoice_id_fkey"
            columns: ["invoice_id"]
            isOneToOne: false
            referencedRelation: "invoices"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "invoice_details_product_id_fkey"
            columns: ["product_id"]
            isOneToOne: false
            referencedRelation: "products"
            referencedColumns: ["id"]
          }
        ]
      }
      invoice_merges: {
        Row: {
          created_at: string
          description: string | null
          from_invoice_id: number
          id: number
          remaining_amount: number
          to_invoice_id: number
          updated_at: string
        }
        Insert: {
          created_at?: string
          description?: string | null
          from_invoice_id: number
          id?: number
          remaining_amount: number
          to_invoice_id: number
          updated_at?: string
        }
        Update: {
          created_at?: string
          description?: string | null
          from_invoice_id?: number
          id?: number
          remaining_amount?: number
          to_invoice_id?: number
          updated_at?: string
        }
        Relationships: [
          {
            foreignKeyName: "invoice_merges_from_invoice_id_fkey"
            columns: ["from_invoice_id"]
            isOneToOne: false
            referencedRelation: "invoices"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "invoice_merges_to_invoice_id_fkey"
            columns: ["to_invoice_id"]
            isOneToOne: false
            referencedRelation: "invoices"
            referencedColumns: ["id"]
          }
        ]
      }
      invoice_payments: {
        Row: {
          amount: number
          created_at: string
          id: number
          invoice_id: number
          note: string | null
          updated_at: string
        }
        Insert: {
          amount?: number
          created_at?: string
          id?: number
          invoice_id: number
          note?: string | null
          updated_at?: string
        }
        Update: {
          amount?: number
          created_at?: string
          id?: number
          invoice_id?: number
          note?: string | null
          updated_at?: string
        }
        Relationships: [
          {
            foreignKeyName: "invoice_payments_invoice_id_fkey"
            columns: ["invoice_id"]
            isOneToOne: false
            referencedRelation: "invoices"
            referencedColumns: ["id"]
          }
        ]
      }
      invoice_returns: {
        Row: {
          created_at: string
          id: number
          invoice_id: number
          product_id: number
          qty: number
          updated_at: string
        }
        Insert: {
          created_at?: string
          id?: number
          invoice_id: number
          product_id: number
          qty?: number
          updated_at?: string
        }
        Update: {
          created_at?: string
          id?: number
          invoice_id?: number
          product_id?: number
          qty?: number
          updated_at?: string
        }
        Relationships: [
          {
            foreignKeyName: "invoice_returns_invoice_id_fkey"
            columns: ["invoice_id"]
            isOneToOne: false
            referencedRelation: "invoices"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "invoice_returns_product_id_fkey"
            columns: ["product_id"]
            isOneToOne: false
            referencedRelation: "products"
            referencedColumns: ["id"]
          }
        ]
      }
      invoice_statuses: {
        Row: {
          created_at: string
          id: number
          name: string
          seq: number
          updated_at: string
        }
        Insert: {
          created_at?: string
          id?: number
          name: string
          seq: number
          updated_at?: string
        }
        Update: {
          created_at?: string
          id?: number
          name?: string
          seq?: number
          updated_at?: string
        }
        Relationships: []
      }
      invoices: {
        Row: {
          created_at: string
          id: number
          invoice_status_id: number
          profile_id: string
          remaining_amount: number
          total_amount: number
          updated_at: string
        }
        Insert: {
          created_at?: string
          id?: number
          invoice_status_id: number
          profile_id: string
          remaining_amount?: number
          total_amount?: number
          updated_at?: string
        }
        Update: {
          created_at?: string
          id?: number
          invoice_status_id?: number
          profile_id?: string
          remaining_amount?: number
          total_amount?: number
          updated_at?: string
        }
        Relationships: [
          {
            foreignKeyName: "invoices_invoice_status_id_fkey"
            columns: ["invoice_status_id"]
            isOneToOne: false
            referencedRelation: "invoice_statuses"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "invoices_profile_id_fkey"
            columns: ["profile_id"]
            isOneToOne: false
            referencedRelation: "profiles"
            referencedColumns: ["id"]
          }
        ]
      }
      product_categories: {
        Row: {
          created_at: string
          id: number
          name: string
          updated_at: string
        }
        Insert: {
          created_at?: string
          id?: number
          name: string
          updated_at?: string
        }
        Update: {
          created_at?: string
          id?: number
          name?: string
          updated_at?: string
        }
        Relationships: []
      }
      products: {
        Row: {
          category_id: number | null
          created_at: string
          deleted_at: string | null
          id: number
          is_exposed_to_list: boolean
          name: string
          selling_price: number
          sku: string
          updated_at: string
        }
        Insert: {
          category_id?: number | null
          created_at?: string
          deleted_at?: string | null
          id?: number
          is_exposed_to_list?: boolean
          name: string
          selling_price: number
          sku: string
          updated_at?: string
        }
        Update: {
          category_id?: number | null
          created_at?: string
          deleted_at?: string | null
          id?: number
          is_exposed_to_list?: boolean
          name?: string
          selling_price?: number
          sku?: string
          updated_at?: string
        }
        Relationships: [
          {
            foreignKeyName: "products_category_id_fkey"
            columns: ["category_id"]
            isOneToOne: false
            referencedRelation: "product_categories"
            referencedColumns: ["id"]
          }
        ]
      }
      profiles: {
        Row: {
          code: string
          created_at: string
          deleted_at: string | null
          fullname: string
          id: string
          phone: string
          role_id: string
          uid: string
          updated_at: string
        }
        Insert: {
          code: string
          created_at?: string
          deleted_at?: string | null
          fullname: string
          id?: string
          phone: string
          role_id: string
          uid: string
          updated_at?: string
        }
        Update: {
          code?: string
          created_at?: string
          deleted_at?: string | null
          fullname?: string
          id?: string
          phone?: string
          role_id?: string
          uid?: string
          updated_at?: string
        }
        Relationships: [
          {
            foreignKeyName: "profiles_role_id_fkey"
            columns: ["role_id"]
            isOneToOne: false
            referencedRelation: "roles"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "profiles_uid_fkey"
            columns: ["uid"]
            isOneToOne: false
            referencedRelation: "users"
            referencedColumns: ["id"]
          }
        ]
      }
      roles: {
        Row: {
          created_at: string
          deleted_at: string | null
          id: string
          name: string
          updated_at: string
        }
        Insert: {
          created_at?: string
          deleted_at?: string | null
          id?: string
          name: string
          updated_at?: string
        }
        Update: {
          created_at?: string
          deleted_at?: string | null
          id?: string
          name?: string
          updated_at?: string
        }
        Relationships: []
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}

export type Tables<
  PublicTableNameOrOptions extends
    | keyof (Database["public"]["Tables"] & Database["public"]["Views"])
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof (Database[PublicTableNameOrOptions["schema"]]["Tables"] &
        Database[PublicTableNameOrOptions["schema"]]["Views"])
    : never = never
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? (Database[PublicTableNameOrOptions["schema"]]["Tables"] &
      Database[PublicTableNameOrOptions["schema"]]["Views"])[TableName] extends {
      Row: infer R
    }
    ? R
    : never
  : PublicTableNameOrOptions extends keyof (Database["public"]["Tables"] &
      Database["public"]["Views"])
  ? (Database["public"]["Tables"] &
      Database["public"]["Views"])[PublicTableNameOrOptions] extends {
      Row: infer R
    }
    ? R
    : never
  : never

export type TablesInsert<
  PublicTableNameOrOptions extends
    | keyof Database["public"]["Tables"]
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicTableNameOrOptions["schema"]]["Tables"]
    : never = never
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? Database[PublicTableNameOrOptions["schema"]]["Tables"][TableName] extends {
      Insert: infer I
    }
    ? I
    : never
  : PublicTableNameOrOptions extends keyof Database["public"]["Tables"]
  ? Database["public"]["Tables"][PublicTableNameOrOptions] extends {
      Insert: infer I
    }
    ? I
    : never
  : never

export type TablesUpdate<
  PublicTableNameOrOptions extends
    | keyof Database["public"]["Tables"]
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicTableNameOrOptions["schema"]]["Tables"]
    : never = never
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? Database[PublicTableNameOrOptions["schema"]]["Tables"][TableName] extends {
      Update: infer U
    }
    ? U
    : never
  : PublicTableNameOrOptions extends keyof Database["public"]["Tables"]
  ? Database["public"]["Tables"][PublicTableNameOrOptions] extends {
      Update: infer U
    }
    ? U
    : never
  : never

export type Enums<
  PublicEnumNameOrOptions extends
    | keyof Database["public"]["Enums"]
    | { schema: keyof Database },
  EnumName extends PublicEnumNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicEnumNameOrOptions["schema"]]["Enums"]
    : never = never
> = PublicEnumNameOrOptions extends { schema: keyof Database }
  ? Database[PublicEnumNameOrOptions["schema"]]["Enums"][EnumName]
  : PublicEnumNameOrOptions extends keyof Database["public"]["Enums"]
  ? Database["public"]["Enums"][PublicEnumNameOrOptions]
  : never
